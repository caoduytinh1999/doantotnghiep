package com.example.myapplication.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.HealthService;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class HealthServiceAdapter extends RecyclerView.Adapter<HealthServiceAdapter.ViewHolder> {
    List<HealthService> healthServices;;
    IOnClickItemListener listener;

    public HealthServiceAdapter(List<HealthService> list, IOnClickItemListener listener) {
        this.healthServices = list;
        this.listener = listener;
    }

    public HealthServiceAdapter(){}

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_service,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthServiceAdapter.ViewHolder holder, int position) {
        HealthService service = healthServices.get(position);
        holder.textViewName.setText(service.getName());
        DecimalFormat format = new DecimalFormat("###,###,###");
        holder.textViewPrice.setText(format.format(service.getPrice()) + "đ");
        holder.textViewDescription.setText(service.getDescription());
    }

    @Override
    public int getItemCount() {
        return healthServices.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textViewName,textViewDescription,textViewPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageViewCheckRowHS);
            textViewName = itemView.findViewById(R.id.textViewNameServiceRowHS);
            textViewDescription = itemView.findViewById(R.id.textViewDescriptionService);
            textViewPrice = itemView.findViewById(R.id.textViewPriceRowHS);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HealthService service = healthServices.get(getAdapterPosition());
                    service.setChecked(!service.isChecked());
                    imageView.setVisibility(service.isChecked() ? View.VISIBLE : View.INVISIBLE);
                    listener.onClick(healthServices.get(getAdapterPosition()));
                }
            });
        }
    }

    public List<HealthService> getAll(){
        return  healthServices;
    }

    public List<HealthService> getSelectedItem(){
        List<HealthService> list = new ArrayList<>();
        for (HealthService item : healthServices){
            if (item.isChecked()){
                list.add(item);
            }
        }
        return list;
    }
}
