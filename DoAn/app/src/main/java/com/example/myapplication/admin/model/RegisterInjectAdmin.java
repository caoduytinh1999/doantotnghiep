package com.example.myapplication.admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterInjectAdmin extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("patients")
    @Expose
    private Patients patients;
    @SerializedName("objectSyringe")
    @Expose
    private String objectSyringe;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("status")
    @Expose
    private String status;

    public RegisterInjectAdmin(String id, Patients patients, String objectSyringe, String date, String address, String status) {
        this.id = id;
        this.patients = patients;
        this.objectSyringe = objectSyringe;
        this.date = date;
        this.address = address;
        this.status = status;
    }

    protected RegisterInjectAdmin(Parcel in) {
        id = in.readString();
        patients = in.readParcelable(Patients.class.getClassLoader());
        objectSyringe = in.readString();
        date = in.readString();
        address = in.readString();
        status = in.readString();
    }

    public static final Creator<RegisterInjectAdmin> CREATOR = new Creator<RegisterInjectAdmin>() {
        @Override
        public RegisterInjectAdmin createFromParcel(Parcel in) {
            return new RegisterInjectAdmin(in);
        }

        @Override
        public RegisterInjectAdmin[] newArray(int size) {
            return new RegisterInjectAdmin[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getObjectSyringe() {
        return objectSyringe;
    }

    public void setObjectSyringe(String objectSyringe) {
        this.objectSyringe = objectSyringe;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(patients, flags);
        dest.writeString(objectSyringe);
        dest.writeString(date);
        dest.writeString(address);
        dest.writeString(status);
    }
}
