package com.example.myapplication.model.administrative_units;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.lang.annotation.Annotation;

public class Ward implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("division_type")
    @Expose
    private String division_type;
    @SerializedName("codename")
    @Expose
    private String codename;
    @SerializedName("district_code")
    @Expose
    private String district_code;

    public Ward(String code, String name, String division_type, String codename, String district_code) {
        this.code = code;
        this.name = name;
        this.division_type = division_type;
        this.codename = codename;
        this.district_code = district_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision_type() {
        return division_type;
    }

    public void setDivision_type(String division_type) {
        this.division_type = division_type;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getDistrict_code() {
        return district_code;
    }

    public void setDistrict_code(String district_code) {
        this.district_code = district_code;
    }
}
