package com.example.myapplication.model.administrative_units;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class District implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("division_type")
    @Expose
    private String division_type;

    @SerializedName("codename")
    @Expose
    private String codename;

    @SerializedName("province_code")
    @Expose
    private String province_code;

    @SerializedName("wards")
    @Expose
    private List<Ward> wards;

    public District(String code, String name, String division_type, String codename, String province_code, List<Ward> wards) {
        this.code = code;
        this.name = name;
        this.division_type = division_type;
        this.codename = codename;
        this.province_code = province_code;
        this.wards = wards;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision_type() {
        return division_type;
    }

    public void setDivision_type(String division_type) {
        this.division_type = division_type;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getProvince_code() {
        return province_code;
    }

    public void setProvince_code(String province_code) {
        this.province_code = province_code;
    }

    public List<Ward> getWards() {
        return wards;
    }

    public void setWards(List<Ward> wards) {
        this.wards = wards;
    }
}
