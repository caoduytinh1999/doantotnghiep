package com.example.myapplication.model;

import com.example.myapplication.my_interface.OnClick;

public class ObjectAU extends OnClick {
    private String code;
    private String name;

    public ObjectAU(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
