package com.example.myapplication.model.vaccine_record;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VaccineRecord extends OnClick {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idPatients")
    @Expose
    private String idPatients;

    @SerializedName("listContent")
    @Expose
    private List<VaccineRecordDetail> list;

    public VaccineRecord(String id, String idPatients) {
        this.id = id;
        this.idPatients = idPatients;
    }

    public List<VaccineRecordDetail> getList() {
        return list;
    }

    public void setList(List<VaccineRecordDetail> list) {
        this.list = list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPatients() {
        return idPatients;
    }

    public void setIdPatients(String idPatients) {
        this.idPatients = idPatients;
    }
}
