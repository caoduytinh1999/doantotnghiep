package com.example.myapplication.admin.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.TopProvince;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InjectAnalysisFragment extends Fragment {
    PieChart pieChart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inject_analysis, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pieChart = view.findViewById(R.id.pieChart);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<TopProvince>> callback = data.getTopProvinceInject();
        callback.enqueue(new Callback<List<TopProvince>>() {
            @Override
            public void onResponse(Call<List<TopProvince>> call, Response<List<TopProvince>> response) {
                dialog.dismissDialog();
                List<TopProvince> list = new ArrayList<>();
                list = response.body();

                ArrayList<PieEntry> entries = new ArrayList<>();
                for (TopProvince item : list){
                    entries.add(new PieEntry(Integer.parseInt(item.getCount()),item.getProvince()));
                }

                ArrayList<Integer> colors = new ArrayList<>();
                for (int color : ColorTemplate.MATERIAL_COLORS){
                    colors.add(color);
                }

                for (int color : ColorTemplate.VORDIPLOM_COLORS){
                    colors.add(color);
                }

                PieDataSet pieDataSet = new PieDataSet(entries,"");
                pieDataSet.setColors(colors);

                PieData pieData = new PieData(pieDataSet);
                pieData.setDrawValues(true);
                pieData.setValueFormatter(new PercentFormatter(pieChart));
                pieData.setValueTextSize(12f);
                pieData.setValueTextColor(Color.WHITE);

                pieChart.setData(pieData);
                pieChart.invalidate();
                pieChart.animateY(1500, Easing.EaseInOutQuad);
                pieChart.setDrawHoleEnabled(true);
                pieChart.setEntryLabelTextSize(0);
                pieChart.getDescription().setEnabled(false);
                pieChart.setCenterText("THỐNG KÊ");
                pieChart.setCenterTextSize(13);

                Legend l = pieChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                l.setTextSize(13);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                l.setOrientation(Legend.LegendOrientation.VERTICAL);
                l.setDrawInside(false);
                l.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<TopProvince>> call, Throwable t) {

            }
        });
    }
}