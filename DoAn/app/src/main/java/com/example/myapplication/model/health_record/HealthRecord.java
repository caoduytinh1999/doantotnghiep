package com.example.myapplication.model.health_record;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthRecord {

    @SerializedName("bloodSugarValue")
    @Expose
    private String bloodSugarValue;

    @SerializedName("bloodSugarTime")
    @Expose
    private String bloodSugarTime;

    @SerializedName("bloodPressureValue")
    @Expose
    private String bloodPressureValue;
    
    @SerializedName("bloodPressureTime")
    @Expose
    private String bloodPressureTime;
    @SerializedName("bmiValue")
    @Expose
    private String bmiValue;
    @SerializedName("bmiTime")
    @Expose
    private String bmiTime;
    @SerializedName("temValue")
    @Expose
    private String temperatureValue;
    @SerializedName("temTime")
    @Expose
    private String temperatureTime;
    @SerializedName("healthBeatValue")
    @Expose
    private String healthBeatValue;
    @SerializedName("healthBeatTime")
    @Expose
    private String healthBeatTime;
    @SerializedName("heightValue")
    @Expose
    private String heightValue;
    @SerializedName("heightTime")
    @Expose
    private String heightTime;
    @SerializedName("spo2Value")
    @Expose
    private String spo2Value;
    @SerializedName("spo2Time")
    @Expose
    private String spo2Time;
    @SerializedName("weightValue")
    @Expose
    private String weightValue;
    @SerializedName("weightTime")
    @Expose
    private String weightTime;

    public HealthRecord(){}

    public HealthRecord(String bloodSugarValue, String bloodSugarTime, String bloodPressureValue, String bloodPressureTime, String bmiValue, String bmiTime, String temperatureValue, String temperatureTime, String healthBeatValue, String healthBeatTime, String heightValue, String heightTime, String spo2Value, String spo2Time, String weightValue, String weightTime) {
        this.bloodSugarValue = bloodSugarValue;
        this.bloodSugarTime = bloodSugarTime;
        this.bloodPressureValue = bloodPressureValue;
        this.bloodPressureTime = bloodPressureTime;
        this.bmiValue = bmiValue;
        this.bmiTime = bmiTime;
        this.temperatureValue = temperatureValue;
        this.temperatureTime = temperatureTime;
        this.healthBeatValue = healthBeatValue;
        this.healthBeatTime = healthBeatTime;
        this.heightValue = heightValue;
        this.heightTime = heightTime;
        this.spo2Value = spo2Value;
        this.spo2Time = spo2Time;
        this.weightValue = weightValue;
        this.weightTime = weightTime;
    }

    public String getBloodSugarValue() {
        return bloodSugarValue;
    }

    public void setBloodSugarValue(String bloodSugarValue) {
        this.bloodSugarValue = bloodSugarValue;
    }

    public String getBloodSugarTime() {
        return bloodSugarTime;
    }

    public void setBloodSugarTime(String bloodSugarTime) {
        this.bloodSugarTime = bloodSugarTime;
    }

    public String getBloodPressureValue() {
        return bloodPressureValue;
    }

    public void setBloodPressureValue(String bloodPressureValue) {
        this.bloodPressureValue = bloodPressureValue;
    }

    public String getBloodPressureTime() {
        return bloodPressureTime;
    }

    public void setBloodPressureTime(String bloodPressureTime) {
        this.bloodPressureTime = bloodPressureTime;
    }

    public String getBmiValue() {
        return bmiValue;
    }

    public void setBmiValue(String bmiValue) {
        this.bmiValue = bmiValue;
    }

    public String getBmiTime() {
        return bmiTime;
    }

    public void setBmiTime(String bmiTime) {
        this.bmiTime = bmiTime;
    }

    public String getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(String temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public String getTemperatureTime() {
        return temperatureTime;
    }

    public void setTemperatureTime(String temperatureTime) {
        this.temperatureTime = temperatureTime;
    }

    public String getHealthBeatValue() {
        return healthBeatValue;
    }

    public void setHealthBeatValue(String healthBeatValue) {
        this.healthBeatValue = healthBeatValue;
    }

    public String getHealthBeatTime() {
        return healthBeatTime;
    }

    public void setHealthBeatTime(String healthBeatTime) {
        this.healthBeatTime = healthBeatTime;
    }

    public String getHeightValue() {
        return heightValue;
    }

    public void setHeightValue(String heightValue) {
        this.heightValue = heightValue;
    }

    public String getHeightTime() {
        return heightTime;
    }

    public void setHeightTime(String heightTime) {
        this.heightTime = heightTime;
    }

    public String getSpo2Value() {
        return spo2Value;
    }

    public void setSpo2Value(String spo2Value) {
        this.spo2Value = spo2Value;
    }

    public String getSpo2Time() {
        return spo2Time;
    }

    public void setSpo2Time(String spo2Time) {
        this.spo2Time = spo2Time;
    }

    public String getWeightValue() {
        return weightValue;
    }

    public void setWeightValue(String weightValue) {
        this.weightValue = weightValue;
    }

    public String getWeightTime() {
        return weightTime;
    }

    public void setWeightTime(String weightTime) {
        this.weightTime = weightTime;
    }
}
