package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthCheckAdapter;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HealthCheckAdminAdapter extends RecyclerView.Adapter<HealthCheckAdminAdapter.ViewHolder>{
    List<HealthCheckAdmin> list;
    IOnClickItemListener listener;

    public HealthCheckAdminAdapter(List<HealthCheckAdmin> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_health_check,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        HealthCheckAdmin check = list.get(position);
        String time = check.getTime().split(" ")[0];
        String day = check.getTime().split(" ")[1].split("/")[0];
        String monthyear = check.getTime().split(" ")[1].substring(3);
        String site = check.getNameHealthFacility();
        String name = check.getNamePatients();
        String status = check.getStatus();
        if (status.compareTo("0") ==0){
            status = "Chưa Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.theme_main));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status));
        }
        if (status.compareTo("1") == 0){
            status = "Đã Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.confirmed));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_confirmed));
        }
        if (status.compareTo("2") == 0){
            status = "Đã Hủy";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.red));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_cancelled));
        }

        holder.textViewStatus.setText(status);
        holder.textViewSite.setText(site);
        holder.textViewName.setText(name);
        holder.textViewTime.setText(time);
        holder.textViewMY.setText(monthyear);
        holder.textViewDay.setText(day);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewTime,textViewName,textViewSite,textViewStatus;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewDayHealthCheck);
            textViewMY  = itemView.findViewById(R.id.textViewMonthYearHealthCheck);
            textViewTime = itemView.findViewById(R.id.textViewTimeHealthCheck);
            textViewName = itemView.findViewById(R.id.textViewNameHealthCheck);
            textViewSite = itemView.findViewById(R.id.textViewVaccineSite);
            textViewStatus = itemView.findViewById(R.id.textViewStatusHealthCheck);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
