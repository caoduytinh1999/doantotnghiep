package com.example.myapplication.model.vaccine_record;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VaccineRecordDetail {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idVaccineRecord")
    @Expose
    private String idVaccineRecord;

    @SerializedName("idStaff")
    @Expose
    private String idStaff;

    @SerializedName("vaccineName")
    @Expose
    private String vaccineName;

        @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("vaccineNumber")
    @Expose
    private String vaccineNumber;

    public VaccineRecordDetail(String id, String idVaccineRecord, String idStaff, String vaccineName, String date, String address, String vaccineNumber) {
        this.id = id;
        this.idVaccineRecord = idVaccineRecord;
        this.idStaff = idStaff;
        this.vaccineName = vaccineName;
        this.date = date;
        this.address = address;
        this.vaccineNumber = vaccineNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVaccineRecord() {
        return idVaccineRecord;
    }

    public void setIdVaccineRecord(String idVaccineRecord) {
        this.idVaccineRecord = idVaccineRecord;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVaccineNumber() {
        return vaccineNumber;
    }

    public void setVaccineNumber(String vaccineNumber) {
        this.vaccineNumber = vaccineNumber;
    }
}
