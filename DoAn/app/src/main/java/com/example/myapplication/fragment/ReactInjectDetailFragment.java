package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.myapplication.R;
import com.example.myapplication.model.ReactInject;

import org.jetbrains.annotations.NotNull;

public class ReactInjectDetailFragment extends Fragment {
    EditText editTextName,editTextVaccine,editTextDataVaccine,editTextSymptom,editTextDateSymptom;
    RadioButton radioButtonYes,radioButtonNo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_react_inject_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        Bundle bundle = getArguments();
        ReactInject inject = bundle.getParcelable("object");
        editTextName.setText(inject.getInjectRecord().getPatients().getName());
        editTextDataVaccine.setText(inject.getInjectRecord().getDate());
        editTextVaccine.setText(inject.getInjectRecord().getVaccine());
        editTextDateSymptom.setText(inject.getTime());
        editTextSymptom.setText(inject.getSymptom());
        if (inject.getSymptom().isEmpty()){
            radioButtonNo.setChecked(true);
        }
        else{
            radioButtonYes.setChecked(true);
        }
    }

    private void assign(View view){
        editTextName = view.findViewById(R.id.editTextName);
        editTextVaccine = view.findViewById(R.id.editTextVaccine);
        editTextDataVaccine = view.findViewById(R.id.editTextDateInject);
        editTextDateSymptom = view.findViewById(R.id.editTextDateSymptom);
        editTextSymptom = view.findViewById(R.id.editTextSymptom);
        radioButtonNo = view.findViewById(R.id.radioNo);
        radioButtonYes = view.findViewById(R.id.radioYes);
    }
}