package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthCheckAdapter;
import com.example.myapplication.admin.activity.AppointmentActivity;
import com.example.myapplication.admin.activity.AppointmentDetailActivity;
import com.example.myapplication.admin.adapter.HealthCheckAdminAdapter;
import com.example.myapplication.fragment.HealthCheckHistoryDetailFragment;
import com.example.myapplication.fragment.SelectionBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentUnconfirmedFragment extends Fragment {
    private static String STATUS_UNCONFIRMED = "0";
    RecyclerView recyclerView;
    List<HealthCheckAdmin> list = new ArrayList<>();
    HealthCheckAdminAdapter adapter;
    HealthCheckAdmin healthCheck;;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointment_unconfirmed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    public void assign(View view){
        recyclerView  = view.findViewById(R.id.recyclerViewListAUnonfirmed);
    }

    public void event(View view){
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new HealthCheckAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                 healthCheck = (HealthCheckAdmin) onClick;
                createBottomSheet(healthCheck);
            }
        });

    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<HealthCheckAdmin>> callback = data.getListHealthCheckAdmin(new Session(getContext()).getID(),STATUS_UNCONFIRMED);
        callback.enqueue(new Callback<List<HealthCheckAdmin>>() {
            @Override
            public void onResponse(Call<List<HealthCheckAdmin>> call, Response<List<HealthCheckAdmin>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<HealthCheckAdmin>> call, Throwable t) {
                dialog.dismissDialog();
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createBottomSheet(HealthCheckAdmin healthCheck){
        SelectionBottomSheetFragment sheetFragment = new SelectionBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String status = objectAU.getCode();
                String mes = "";
                String id = healthCheck.getId();
                switch (status){
                    case "0":
                        changeActivity();
                        break;
                    case "1" :
                        mes = "Bạn chắc chắn có muốn Xác Nhận lịch đặt khám của " + healthCheck.getNamePatients() + " vào lúc " + healthCheck.getTime() + " không ? ";
                        createPopup(mes,id,status);
                        break;
                    case "2" :
                        mes = "Bạn chắc chắn có muốn Xóa lịch đặt khám của " + healthCheck.getNamePatients() + " vào lúc " + healthCheck.getTime() + " không ? ";
                        createPopup(mes,id,status);
                        break;
                }
            }
        });
        sheetFragment.show(getActivity().getSupportFragmentManager(),sheetFragment.getTag());
    }

    public void updateHealthCheck(String id,String status){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateHealthCheckAdmin(new Session(getContext()).getID(),id,status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                list.remove(healthCheck);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
                if (status.compareTo("1") == 0) AppointmentActivity.needUpdateConfirmed = true;
                if (status.compareTo("2") == 0) AppointmentActivity.needUpdateCancelled = true;
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismissDialog();
            }
        });
    }

    public void createPopup(String message,String id,String status){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                updateHealthCheck(id,status);
            }
        });

        dialog.show();
    }

    public void changeActivity(){
        Intent intent = new Intent(getContext(), AppointmentDetailActivity.class);
        intent.putExtra("object",healthCheck);
        startActivity(intent);
    }
}