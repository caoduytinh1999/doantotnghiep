package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.adapter.ViewPagerAdapter;
import com.example.myapplication.adapter.ViewPagerAdminAdapter;
import com.example.myapplication.admin.fragment.AdminAccountFragment;
import com.example.myapplication.admin.fragment.AdminHomeFragment;
import com.example.myapplication.admin.fragment.AdminNotifyFragment;
import com.example.myapplication.model.Session;
import com.simform.custombottomnavigation.SSCustomBottomNavigation;
import com.simform.custombottomnavigation.SSCustomBottomNavigationKt;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class AdminActivity extends AppCompatActivity {

    SSCustomBottomNavigation bottomNavigation;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        assign();
        event();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void event() {
        bottomNavigation.add(new SSCustomBottomNavigation.Model(0,R.drawable.icon_home,"Trang Chủ"));
        bottomNavigation.add(new SSCustomBottomNavigation.Model(1,R.drawable.icon_calendar_date,"Lịch Hẹn"));
        bottomNavigation.add(new SSCustomBottomNavigation.Model(2,R.drawable.icon_notifications,"Thông Báo"));
        bottomNavigation.add(new SSCustomBottomNavigation.Model(3,R.drawable.icon_account,"Cá Nhân"));
        bottomNavigation.setCount(2,"11");
        bottomNavigation.show(0,true);

        setUpViewPager();

        //replaceFragment(new AdminHomeFragment());

        bottomNavigation.setOnClickMenuListener(new Function1<SSCustomBottomNavigation.Model, Unit>() {
            @Override
            public Unit invoke(SSCustomBottomNavigation.Model model) {
                switch (model.getId()){
                    case 0 :
                        viewPager.setCurrentItem(0);
                        break;
                    case 1 :
                        viewPager.setCurrentItem(1);
                        break;
                    case 2 :
                        viewPager.setCurrentItem(2);
                        break;
                    case 3 :
                        viewPager.setCurrentItem(3);
                        break;
                }
                return null;
            }
        });
    }

    private void setUpViewPager() {
        ViewPagerAdminAdapter adapter = new ViewPagerAdminAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0 :
                        bottomNavigation.show(0,true);
                        break;
                    case 1 :
                        bottomNavigation.show(1,true);
                        break;
                    case 2 :
                        bottomNavigation.show(2,true);
                        break;
                    case 3 :
                        bottomNavigation.show(3,true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void assign() {
        bottomNavigation = findViewById(R.id.bottomNavigationAdmin);
        viewPager        = findViewById(R.id.viewPagerAdmin);
    }

    public void replaceFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.layoutAdmin,fragment);
        fragmentTransaction.commit();
    }
}