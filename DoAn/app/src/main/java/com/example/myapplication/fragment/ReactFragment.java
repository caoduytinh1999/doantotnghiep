package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.fragment.VaccineBottomSheetFragment;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReactFragment extends Fragment {
    EditText editTextName,editTextVaccine,editTextDataVaccine,editTextSymptom,editTextDateSymptom;
    RadioButton radioButtonYes,radioButtonNo;
    Button button;
    String id = "";
    boolean choose = false;
    List<InjectRecord> list = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_react, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void assign(View view){
        editTextName = view.findViewById(R.id.editTextName);
        editTextVaccine = view.findViewById(R.id.editTextVaccine);
        editTextDataVaccine = view.findViewById(R.id.editTextDateInject);
        editTextDateSymptom = view.findViewById(R.id.editTextDateSymptom);
        editTextSymptom = view.findViewById(R.id.editTextSymptom);
        radioButtonNo = view.findViewById(R.id.radioNo);
        radioButtonYes = view.findViewById(R.id.radioYes);
        button = view.findViewById(R.id.button);
    }

    private void event(View view){
        radioButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSymptom.setEnabled(true);
                choose = true;
            }
        });

        radioButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSymptom.setEnabled(false);
                choose = false;
                editTextSymptom.setText("");
            }
        });

        editTextVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.size() > 0){
                    createBottomSheetVaccine();
                }
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidate()){
                    react();
                }
            }
        });

        editTextName.setText(new Session(getContext()).getName());
        editTextDateSymptom.setText(getCurrentTime());
    }

    private void react(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        String time = editTextDateSymptom.getText().toString();
        String symptom = editTextSymptom.getText().toString();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.reactInject(id,time,symptom);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().compareTo("success") == 0){
                    showToastSuccess("Cập nhật thông tin thành công");
                    getActivity().finish();
                }
                else{
                    Toast.makeText(getContext(), "Cập nhật thông tin không thành công", Toast.LENGTH_SHORT).show();
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    private boolean checkValidate(){
        if (choose && editTextSymptom.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập triệu chứng");
            return false;
        }

        return true;
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,getActivity().findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,getActivity().findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public void createBottomSheetVaccine(){
        List<String> vaccineList = new ArrayList<>();
        for (InjectRecord item : list){
            String content = "Mũi " + item.getNumberOfInject() + " - " + item.getVaccine();
            vaccineList.add(content);
        }
        VaccineBottomSheetFragment fragment = new VaccineBottomSheetFragment(vaccineList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU object = (ObjectAU) onClick;
                editTextVaccine.setText(object.getName());
                int pos = Integer.parseInt(object.getCode());
                editTextDataVaccine.setText(list.get(pos).getDate());
                id = list.get(pos).getIdRecord();
            }
        });

        fragment.show(getActivity().getSupportFragmentManager(),fragment.getTag());
    }

    private void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<InjectRecord>> callback = data.getListAppointmentInject(new Session(getContext()).getID());
        callback.enqueue(new Callback<List<InjectRecord>>() {
            @Override
            public void onResponse(Call<List<InjectRecord>> call, Response<List<InjectRecord>> response) {
                for (int  i = 0 ; i < response.body().size() ; i++){
                    InjectRecord record = response.body().get(i);
                    if (record.getStatus().compareTo("1") ==0){
                        list.add(record);
                    }
                }
                Log.d("AAA","hihi" + list.size());
                if (list.size() > 0){
                    InjectRecord record = list.get(0);
                    editTextVaccine.setText("Mũi" + record.getNumberOfInject() + " - " + record.getVaccine());
                    editTextDataVaccine.setText(record.getDate());
                    id = record.getIdRecord();
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<InjectRecord>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    private String getCurrentTime(){
        String time = "";
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        time = (day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" +year + " " + (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);

        return time;
    }
}