package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.HealthFacility;

public class HealthFacilitiesInfoFragment extends Fragment {

    Button button;
    TextView textViewName,textViewAddress,textViewPhone,textViewEmail,textViewWebsite,textViewFanpage,textViewName1,textViewAddress1,textViewNumber;
    HealthFacility facility;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_health_facilities_info, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        setData();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment();
            }
        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",facility);
        InfoBA1Fragment fragment = new InfoBA1Fragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthFacilities,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void assign(View view) {
        button          = view.findViewById(R.id.buttonBookAppointment);
        textViewName    = view.findViewById(R.id.textViewHospitalNameInfo);
        textViewAddress = view.findViewById(R.id.textViewHospitalAddressInfo);
        textViewPhone   = view.findViewById(R.id.textViewHospitalPhoneInfo);
        textViewEmail         = view.findViewById(R.id.textViewHospitalEmailInfo);
        textViewWebsite       = view.findViewById(R.id.textViewHospitalWebsiteInfo);
        textViewFanpage       = view.findViewById(R.id.textViewHospitalFanpageInfo);
        textViewName1          = view.findViewById(R.id.textViewHospitalNameInfo1);
        textViewAddress1   = view.findViewById(R.id.textViewHospitalAddressInfo1);
        textViewNumber = view.findViewById(R.id.textViewNumber);
    }

    public void setData(){
        Bundle bundle = this.getArguments();
        facility = bundle.getParcelable("object");
        textViewName.setText(facility.getName());
        textViewAddress.setText(facility.getAddress());
        textViewPhone.setText("Số điện thoại : " + facility.getPhone());
        textViewEmail.setText("Email : " + facility.getEmail());
        textViewWebsite.setText("Website : " + facility.getWebsite());
        textViewFanpage.setText("Fanpage : " + facility.getFanpage());
        textViewName1.setText("Tên cơ sở y tế : " + facility.getName());
        textViewAddress1.setText(facility.getAddress());
        textViewNumber.setText("Số Lượt Khám \n" + facility.getnOAB());
    }

}