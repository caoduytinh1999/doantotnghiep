package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.activity.ConfirmedRSActivity;
import com.example.myapplication.admin.adapter.HealthCheckAdminAdapter;
import com.example.myapplication.admin.adapter.RIRAdminAdapter;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.fragment.ButtonBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RIRUnconfirmedFragment extends Fragment {
    private static String UNCONFIRMED_STATUS = "0";
    RecyclerView recyclerView;
    List<InjectRecord> list = new ArrayList<>();
    RIRAdminAdapter adapter;
    InjectRecord record;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_r_i_r_unconfirmed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RIRAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                record = (InjectRecord) onClick;
                createBottomSheet();
            }
        });
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListRIRUnconfirmed);
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<InjectRecord>> callback = data.getListRegisterInjectRecord(new Session(getContext()).getID(),UNCONFIRMED_STATUS);
        callback.enqueue(new Callback<List<InjectRecord>>() {
            @Override
            public void onResponse(Call<List<InjectRecord>> call, Response<List<InjectRecord>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<InjectRecord>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void createBottomSheet(){
        ButtonBottomSheetFragment fragment = new ButtonBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String code = objectAU.getCode();
                if (code.compareTo("1") ==0){
                    Intent intent = new Intent(getContext(), ConfirmedRSActivity.class);
                    intent.putExtra("status","0");
                    intent.putExtra("record",record);
                    startActivity(intent);
                }
                else{
                    String mes = "Bạn chắc chắn có muốn Xác Nhận lịch tiêm chủng của " + record.getPatients().getName() + " vào lúc " + record.getDate() + " không ? ";
                    String id = record.getId();
                    createPopup(mes,id);
                }
            }
        });
        fragment.show(getActivity().getSupportFragmentManager(),fragment.getTag());
    }

    public void createPopup(String message,String id){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                updateRIR();
            }
        });

        dialog.show();
    }

    public void updateRIR(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        int minute = calendar.get(Calendar.MINUTE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        String time = (hour < 10 ? "0" + hour : hour) + ":" +(minute < 10 ? "0" + minute : minute) + " " + (day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateRegisterInjectRecord(record.getId(),record.getNumberOfInject(),time);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().compareTo("success") ==0){
                    list.remove(record);
                    adapter.notifyDataSetChanged();
                }
                else{
                    Toast.makeText(getContext(), "Xác Nhận Không Thành Công", Toast.LENGTH_SHORT).show();
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

}