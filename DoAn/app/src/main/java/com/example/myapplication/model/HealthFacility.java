package com.example.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthFacility extends OnClick implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("fanpage")
    @Expose
    private String fanpage;

    @SerializedName("nOAB")
    @Expose
    private String nOAB;

    public HealthFacility() {
    }

    public HealthFacility(String id, String name, String address, String phone, String email, String website, String fanpage, String nOAB) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.fanpage = fanpage;
        this.nOAB = nOAB;
    }

    protected HealthFacility(Parcel in) {
        id = in.readString();
        name = in.readString();
        address = in.readString();
        phone = in.readString();
        email = in.readString();
        website = in.readString();
        fanpage = in.readString();
        nOAB = in.readString();
    }

    public static final Creator<HealthFacility> CREATOR = new Creator<HealthFacility>() {
        @Override
        public HealthFacility createFromParcel(Parcel in) {
            return new HealthFacility(in);
        }

        @Override
        public HealthFacility[] newArray(int size) {
            return new HealthFacility[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFanpage() {
        return fanpage;
    }

    public void setFanpage(String fanpage) {
        this.fanpage = fanpage;
    }

    public String getnOAB() {
        return nOAB;
    }

    public void setnOAB(String nOAB) {
        this.nOAB = nOAB;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(website);
        dest.writeString(fanpage);
        dest.writeString(nOAB);
    }
}
