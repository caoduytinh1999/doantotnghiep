package com.example.myapplication.model.Account;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Patients implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("birthday")
    @Expose
    private String birthday;

    @SerializedName("sex")
    @Expose
    private String sex;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("job")
    @Expose
    private String job;

    @SerializedName("idcode")
    @Expose
    private String idcode;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("healthInsurance")
    @Expose
    private String healthInsurance;

    public Patients(String id, String phone, String name, String birthday, String sex, String address, String job, String idcode, String email, String healthInsurance) {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
        this.job = job;
        this.idcode = idcode;
        this.email = email;
        this.healthInsurance = healthInsurance;
    }

    public Patients() {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
        this.job = job;
        this.idcode = idcode;
        this.email = email;
        this.healthInsurance = healthInsurance;
    }

    protected Patients(Parcel in) {
        id = in.readString();
        phone = in.readString();
        name = in.readString();
        birthday = in.readString();
        sex = in.readString();
        address = in.readString();
        job = in.readString();
        idcode = in.readString();
        email = in.readString();
        healthInsurance = in.readString();
    }

    public static final Creator<Patients> CREATOR = new Creator<Patients>() {
        @Override
        public Patients createFromParcel(Parcel in) {
            return new Patients(in);
        }

        @Override
        public Patients[] newArray(int size) {
            return new Patients[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getIdcode() {
        return idcode;
    }

    public void setIdcode(String idcode) {
        this.idcode = idcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHealthInsurance() {
        return healthInsurance;
    }

    public void setHealthInsurance(String healthInsurance) {
        this.healthInsurance = healthInsurance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(phone);
        dest.writeString(name);
        dest.writeString(birthday);
        dest.writeString(sex);
        dest.writeString(address);
        dest.writeString(job);
        dest.writeString(idcode);
        dest.writeString(email);
        dest.writeString(healthInsurance);
    }
}
