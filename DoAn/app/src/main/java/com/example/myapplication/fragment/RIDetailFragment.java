package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.register_syringe.RegisterSyringe;

public class RIDetailFragment extends Fragment {

    ConstraintLayout layoutName,layoutBirthday,layoutSex,layoutID,layoutHI,layoutProvince,layoutDistrict,layoutWard,layoutPlace;
    TextView textViewTName,textViewName,textViewTBirthday,textViewBirthday,textViewTSex,textViewSex,textViewTID,textViewID,textViewTHI,
            textViewHI,textViewTProvince,textViewProvince,textViewTDistrict,textViewDistrict,textViewTWard,textViewWard,textViewTPlace,textViewPlace;
    ImageView imageViewName,imageViewBirthday,imageViewSex,imageViewID,imageViewHI,imageViewProvince,imageViewDistrict,imageViewWard,imageViewPlace;
    Patients patients;
    RegisterSyringe syringe;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_r_i_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        Bundle bundle = this.getArguments();
        patients = bundle.getParcelable("patients");
        syringe = bundle.getParcelable("syringe");

        textViewName.setText(patients.getName());
        textViewTName.setText("Người tới tiêm");
        imageViewName.setImageResource(R.drawable.icon_user_small);

        textViewBirthday.setText(patients.getBirthday());
        textViewTBirthday.setText("Ngày tháng năm sinh");
        imageViewBirthday.setImageResource(R.drawable.icon_calendar_date);

        textViewSex.setText((patients.getSex().compareTo("1") ==0) ? "Name" : "Nữ");
        textViewTSex.setText("Giới tính");
        imageViewSex.setImageResource(R.drawable.icon_user_small);

        textViewID.setText(patients.getIdcode());
        textViewTID.setText("Số hộ chiếu/CMND/CCCD");
        imageViewID.setImageResource(R.drawable.icon_id);

        textViewHI.setText(patients.getHealthInsurance());
        textViewTHI.setText("Số thẻ BHYT");
        imageViewHI.setImageResource(R.drawable.icon_id);
        String province = "";
        String district = "";
        String ward = "";
        String place = "";
        if (!patients.getAddress().isEmpty()){
            province = patients.getAddress().split(",")[3];
            district = patients.getAddress().split(",")[2];
            ward = patients.getAddress().split(",")[1];
            place = patients.getAddress().split(",")[0];
        }
        textViewProvince.setText(province);
        textViewTProvince.setText("Tỉnh/Thành phố");
        imageViewProvince.setImageResource(R.drawable.icon_place);

        textViewDistrict.setText(district);
        textViewTDistrict.setText("Quận/Huyện");
        imageViewDistrict.setImageResource(R.drawable.icon_place);

        textViewWard.setText(ward);
        textViewTWard.setText("Phường/Xã");
        imageViewWard.setImageResource(R.drawable.icon_place);

        textViewPlace.setText(place);
        textViewTPlace.setText("Địa chỉ nơi ở");
        imageViewPlace.setImageResource(R.drawable.icon_place);
    }

    private void assign(View view) {
        layoutName = view.findViewById(R.id.layoutNameDetailRI);
        layoutBirthday = view.findViewById(R.id.layoutBirthdayDetailRI);
        layoutSex = view.findViewById(R.id.layoutSexDetailRI);
        layoutID = view.findViewById(R.id.layoutIDDetailRI);
        layoutHI = view.findViewById(R.id.layoutHIDetailRI);
        layoutProvince = view.findViewById(R.id.layoutProvinceDetailRI);
        layoutDistrict = view.findViewById(R.id.layoutDistrictDetailRI);
        layoutWard = view.findViewById(R.id.layoutWardDetailRI);
        layoutPlace = view.findViewById(R.id.layoutPlaceDetailRI);

        textViewTName = layoutName.findViewById(R.id.textViewTitleRowRI);
        textViewName = layoutName.findViewById(R.id.textViewContentRowRI);
        imageViewName = layoutName.findViewById(R.id.imageViewRowRI);

        textViewTBirthday = layoutBirthday.findViewById(R.id.textViewTitleRowRI);
        textViewBirthday = layoutBirthday.findViewById(R.id.textViewContentRowRI);
        imageViewBirthday = layoutBirthday.findViewById(R.id.imageViewRowRI);

        textViewTSex = layoutSex.findViewById(R.id.textViewTitleRowRI);
        textViewSex = layoutSex.findViewById(R.id.textViewContentRowRI);
        imageViewSex = layoutSex.findViewById(R.id.imageViewRowRI);

        textViewTID = layoutID.findViewById(R.id.textViewTitleRowRI);
        textViewID = layoutID.findViewById(R.id.textViewContentRowRI);
        imageViewID = layoutID.findViewById(R.id.imageViewRowRI);

        textViewTHI = layoutHI.findViewById(R.id.textViewTitleRowRI);
        textViewHI = layoutHI.findViewById(R.id.textViewContentRowRI);
        imageViewHI = layoutHI.findViewById(R.id.imageViewRowRI);

        textViewTProvince = layoutProvince.findViewById(R.id.textViewTitleRowRI);
        textViewProvince = layoutProvince.findViewById(R.id.textViewContentRowRI);
        imageViewProvince = layoutProvince.findViewById(R.id.imageViewRowRI);

        textViewTDistrict = layoutDistrict.findViewById(R.id.textViewTitleRowRI);
        textViewDistrict = layoutDistrict.findViewById(R.id.textViewContentRowRI);
        imageViewDistrict = layoutDistrict.findViewById(R.id.imageViewRowRI);

        textViewTWard = layoutWard.findViewById(R.id.textViewTitleRowRI);
        textViewWard = layoutWard.findViewById(R.id.textViewContentRowRI);
        imageViewWard = layoutWard.findViewById(R.id.imageViewRowRI);

        textViewTPlace = layoutPlace.findViewById(R.id.textViewTitleRowRI);
        textViewPlace = layoutPlace.findViewById(R.id.textViewContentRowRI);
        imageViewPlace = layoutPlace.findViewById(R.id.imageViewRowRI);
    }
}