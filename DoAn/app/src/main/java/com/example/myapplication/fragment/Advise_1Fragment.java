package com.example.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.VideoActivity;
import com.example.myapplication.adapter.AdviseAdapter;
import com.example.myapplication.model.Advise;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class Advise_1Fragment extends Fragment {
    RecyclerView recyclerView;
    List<Advise> list = new ArrayList<>();
    AdviseAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_advise_1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        setData();
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new AdviseAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                Advise advise = (Advise) onClick;
                Intent intent = new Intent(getContext(),VideoActivity.class);
                intent.putExtra("object",advise);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListAD1);
    }

    public void setData(){
        list.add(new Advise("Bài tập thở số 1","D2UVMRSmc5g",129,180,getString(R.string.tho_1)));
        list.add(new Advise("Bài tập thở số 2","D2UVMRSmc5g",181,232,getString(R.string.tho_2)));
        list.add(new Advise("Bài tập thở số 3","D2UVMRSmc5g",234,317,getString(R.string.tho_3)));
        list.add(new Advise("Bài tập thở số 4","D2UVMRSmc5g",319,636,getString(R.string.tho_4)));
        list.add(new Advise("Bài tập thở số 5","D2UVMRSmc5g",638,454,getString(R.string.tho_5)));
    }
}