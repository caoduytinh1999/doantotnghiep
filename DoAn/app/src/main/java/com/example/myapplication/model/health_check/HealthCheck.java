package com.example.myapplication.model.health_check;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthCheck extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idHealthFacility")
    @Expose
    private String idHealthFacility;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("symptom")
    @Expose
    private String symptom;

    @SerializedName("status")
    @Expose
    private String status;


    public HealthCheck(String id,String idHealthFacility, String address, String service, float price, String time, String symptom, String status) {
        this.id = id;
        this.address = address;
        this.service = service;
        this.price = price;
        this.time = time;
        this.symptom = symptom;
        this.status = status;
        this.idHealthFacility = idHealthFacility;
    }

    protected HealthCheck(Parcel in) {
        id = in.readString();
        idHealthFacility = in.readString();
        address = in.readString();
        service = in.readString();
        price = in.readFloat();
        time = in.readString();
        symptom = in.readString();
        status = in.readString();
    }

    public static final Creator<HealthCheck> CREATOR = new Creator<HealthCheck>() {
        @Override
        public HealthCheck createFromParcel(Parcel in) {
            return new HealthCheck(in);
        }

        @Override
        public HealthCheck[] newArray(int size) {
            return new HealthCheck[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdHealthFacility() {
        return idHealthFacility;
    }

    public void setIdHealthFacility(String idHealthFacility) {
        this.idHealthFacility = idHealthFacility;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(idHealthFacility);
        dest.writeString(address);
        dest.writeString(service);
        dest.writeFloat(price);
        dest.writeString(time);
        dest.writeString(symptom);
        dest.writeString(status);
    }
}
