package com.example.myapplication.admin.model;

import com.example.myapplication.model.Account.Patients;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VaccineInfo {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("patients")
    @Expose
    private Patients patients;
    @SerializedName("numberOfInject")
    @Expose
    private String numberOfInject;
    @SerializedName("vaccine")
    @Expose
    private String vaccine;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("site")
    @Expose
    private Site site;

    public VaccineInfo(String id, Patients patients, String numberOfInject, String vaccine, String date, Site site) {
        this.id = id;
        this.patients = patients;
        this.numberOfInject = numberOfInject;
        this.vaccine = vaccine;
        this.date = date;
        this.site = site;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getNumberOfInject() {
        return numberOfInject;
    }

    public void setNumberOfInject(String numberOfInject) {
        this.numberOfInject = numberOfInject;
    }

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
}
