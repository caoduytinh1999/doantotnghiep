package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.myapplication.R;
import com.example.myapplication.retrofit.DataClient;

import retrofit2.Call;

public class DeclareMedicalDetailFragment extends Fragment {

    EditText editTextName,editTextBirthday,editTextID,editTextNation,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextPhone,editTextEmail;
    RadioGroup radioGroup1,radioGroup2,radioGroup3,radioGroup4,radioGroup5;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_declare_medical_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {

    }

    private void assign(View view) {
        editTextName        = view.findViewById(R.id.editTextNameHistoryDM);
        editTextBirthday    = view.findViewById(R.id.editTextBirthdayHistoryDM);
        editTextID          = view.findViewById(R.id.editTextBirthdayHistoryDM);
        editTextNation      = view.findViewById(R.id.editTextNationHistoryDM);
        editTextProvince    = view.findViewById(R.id.editTextCityHistoryDM);
        editTextDistrict    = view.findViewById(R.id.editTextDistricHistoryDM);
        editTextWard        = view.findViewById(R.id.editTextWardHistoryDM);
        editTextPlace       = view.findViewById(R.id.editTextPlaceHistoryDM);
        editTextPhone       = view.findViewById(R.id.editTextPhoneHistoryDM);
        editTextEmail       = view.findViewById(R.id.editTextEmailHistoryDM);
        radioGroup1         = view.findViewById(R.id.radiogroupHistoryDM1);
        radioGroup2         = view.findViewById(R.id.radiogroupHistoryDM2);
        radioGroup3         = view.findViewById(R.id.radiogroupHistoryDM3);
        radioGroup4         = view.findViewById(R.id.radiogroupHistoryDM4);
        radioGroup5         = view.findViewById(R.id.radiogroupHistoryDM5);
        radioButtonMale     = view.findViewById(R.id.radioSexMaleHistoryDM);
        radioButtonFemale   = view.findViewById(R.id.radioSexFeMaleHistoryDM);
        radioButtonOther    = view.findViewById(R.id.radioOtherMaleHistoryDM);
    }

    public void setData(){
    }
}