package com.example.myapplication.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.AppointmentCancelledFragment;
import com.example.myapplication.admin.fragment.AppointmentConfirmedFragment;
import com.example.myapplication.admin.fragment.AppointmentUnconfirmedFragment;
import com.example.myapplication.fragment.ReactFragment;
import com.example.myapplication.fragment.ReactHistoryFragment;

public class ViewPagerReactAdapter extends FragmentStatePagerAdapter {
    public ViewPagerReactAdapter(FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new ReactFragment();
            case 1:
                return new ReactHistoryFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Phiếu khảo sát";
                break;
            case 1 :
                title = "Danh sách phiếu khảo sát";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
