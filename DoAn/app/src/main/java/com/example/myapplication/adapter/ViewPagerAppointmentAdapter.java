package com.example.myapplication.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.fragment.MedicalAppointmentFragment;
import com.example.myapplication.fragment.VaccinationAppointmentFragment;

public class ViewPagerAppointmentAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAppointmentAdapter(FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new MedicalAppointmentFragment();
            case 1 :
                return new VaccinationAppointmentFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Lịch hẹn khám";
                break;
            case 1 :
                title = "Lịch hẹn tiêm";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
