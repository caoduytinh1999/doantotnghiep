package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.HealthRecordDetailActivity;
import com.example.myapplication.R;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class AddAllValueHRFragment extends Fragment {

    CardView cardViewPS,cardViewSPO2,cardViewBP,cardViewHB,cardViewT,cardViewH,cardViewW,cardViewBMI;
    TextView textViewPS,textViewTimePS,textViewSPO2,textViewTimeSPO2,textViewPB,textViewTimePB,textViewHB,textViewTimeHB,textViewT,textViewTimeT,
            textViewH,textViewTimeH,textViewW,textViewTimeW,textViewBMI,textViewTimeBMI;
    String time = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_all_value_h_r, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (HealthRecordDetailActivity.bloodpressure != ""){
            textViewPB.setText(HealthRecordDetailActivity.bloodpressure);
            textViewTimePB.setText("Đo lúc : " + time);
            cardViewBP.setEnabled(false);
        }

        if (HealthRecordDetailActivity.heartbeat != ""){
            textViewHB.setText(HealthRecordDetailActivity.heartbeat);
            textViewTimeHB.setText("Đo lúc : " + time);
            cardViewHB.setEnabled(false);
        }

        if (HealthRecordDetailActivity.tem != ""){
            textViewT.setText(HealthRecordDetailActivity.tem);
            textViewTimeT.setText("Đo lúc : " + time);
            cardViewT.setEnabled(false);
        }

        if (HealthRecordDetailActivity.height != ""){
            textViewH.setText(HealthRecordDetailActivity.height);
            textViewTimeH.setText("Đo lúc : " + time);
            cardViewH.setEnabled(false);
        }

        if (HealthRecordDetailActivity.weight != ""){
            textViewW.setText(HealthRecordDetailActivity.weight);
            textViewTimeW.setText("Đo lúc : " + time);
            cardViewW.setEnabled(false);
        }

        if (HealthRecordDetailActivity.bmi != ""){
            textViewBMI.setText(HealthRecordDetailActivity.bmi);
            textViewTimeBMI.setText("Đo lúc : " + time);
            cardViewBMI.setEnabled(false);
        }

        if (HealthRecordDetailActivity.bloodsugar != ""){
            textViewPS.setText(HealthRecordDetailActivity.bloodsugar);
            textViewTimePS.setText("Đo lúc : " + time);
            cardViewPS.setEnabled(false);
        }

        if (HealthRecordDetailActivity.spo2 != ""){
            textViewSPO2.setText(HealthRecordDetailActivity.spo2);
            textViewTimeSPO2.setText("Đo lúc : " + time);
            cardViewSPO2.setEnabled(false);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("AAA","pause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        HealthRecordDetailActivity.bloodpressure = "";
        HealthRecordDetailActivity.heartbeat = "";
        HealthRecordDetailActivity.tem = "";
        HealthRecordDetailActivity.height = "";
        HealthRecordDetailActivity.weight = "";
        HealthRecordDetailActivity.bmi = "";
        HealthRecordDetailActivity.bloodsugar = "";
        HealthRecordDetailActivity.spo2 = "";

        Log.d("AAA","destroy");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("AAA","stop");
    }

    public void replaceFragment(String title, String time){
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        bundle.putString("time",time);
        AddHealthQuotientFragment fragment = new AddHealthQuotientFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack("AddHealthQuotientFragment");
        fragmentTransaction.commit();
    }

    private void event(View view) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        int minute = calendar.get(Calendar.MINUTE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        time = (hour < 10 ? "0" + hour : hour) + ":" + (minute < 10 ? "0" + minute : minute);
        String date = (day < 10 ? "0" + day : day) + "/" +(month < 10 ? "0" + month : month) + "/" + year;

        cardViewPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BLOODSUGAR",time);
            }
        });

        cardViewSPO2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("SPO2",time);
            }
        });

        cardViewBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BLOODPRESSURE",time);
            }
        });

        cardViewHB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("HEARTBEAT",time);
            }
        });

        cardViewT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("TEMPERATURE",time);
            }
        });

        cardViewH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("HEIGHT",time);
            }
        });

        cardViewW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("WEIGHT",time);
            }
        });

        cardViewBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BMI",time);
            }
        });
    }

    private void assign(View view) {
        cardViewPS = view.findViewById(R.id.cardViewBS);
        textViewPS = view.findViewById(R.id.textViewBloodSugar);
        textViewTimePS = view.findViewById(R.id.textViewTimeBloodSugar);

        cardViewSPO2 = view.findViewById(R.id.cardViewSPO2);
        textViewSPO2 = view.findViewById(R.id.textViewSPO2);
        textViewTimeSPO2 = view.findViewById(R.id.textViewTimeSPO2);

        cardViewBP = view.findViewById(R.id.cardViewBP);
        textViewPB = view.findViewById(R.id.textViewBloodPressure);
        textViewTimePB = view.findViewById(R.id.textViewTimeBloodPressure);

        cardViewHB = view.findViewById(R.id.cardViewHB);
        textViewHB = view.findViewById(R.id.textViewHeartBeat);
        textViewTimeHB = view.findViewById(R.id.textViewTimeHeartBeat);

        cardViewT = view.findViewById(R.id.cardViewTempurature);
        textViewT = view.findViewById(R.id.textViewTempurature);
        textViewTimeT = view.findViewById(R.id.textViewTimeTempurature);

        cardViewH = view.findViewById(R.id.cardViewHeight);
        textViewH = view.findViewById(R.id.textViewHeight);
        textViewTimeH = view.findViewById(R.id.textViewTimeHeight);

        cardViewW = view.findViewById(R.id.cardViewWeight);
        textViewW = view.findViewById(R.id.textViewWeight);
        textViewTimeW = view.findViewById(R.id.textViewTimeWeight);

        cardViewBMI = view.findViewById(R.id.cardViewBMI);
        textViewBMI = view.findViewById(R.id.textViewBMI);
        textViewTimeBMI = view.findViewById(R.id.textViewTimeBMI);
    }
}