package com.example.myapplication.admin.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.HealthCheckAnalysisFragment;
import com.example.myapplication.admin.fragment.InjectAnalysisFragment;
import com.example.myapplication.admin.fragment.TestAnalysisFragment;
import com.example.myapplication.admin.fragment.TestCancelledFragment;
import com.example.myapplication.admin.fragment.TestConfirmedFragment;
import com.example.myapplication.admin.fragment.TestUnconfirmedFragment;

import org.jetbrains.annotations.NotNull;

public class ViewPagerAnalysisAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAnalysisAdapter(@NonNull @NotNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new HealthCheckAnalysisFragment();
            case 1:
                return new InjectAnalysisFragment();
            case 2 :
                return new TestAnalysisFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Đặt Khám";
                break;
            case 1 :
                title = "Tiêm Chủng";
                break;
            case 2 :
                title = "Xét Nghiệm";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
