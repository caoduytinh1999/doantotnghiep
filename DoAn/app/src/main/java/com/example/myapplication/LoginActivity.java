package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.admin.activity.ManageActivity;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.TokenObject;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView textViewSignUp;
    Button buttonLogin;
    TextView textViewFogotPassWord;
    EditText editTextPhone;
    EditText editTextPassWord;
    TextInputLayout textInputLayoutPhone, textInputLayoutPassWord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mapped();
        event();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void event() {
    }

    private void mapped() {
        textViewSignUp          = findViewById(R.id.textViewSignUp);
        buttonLogin             = findViewById(R.id.buttonLogin);
        textViewFogotPassWord   = findViewById(R.id.textViewForgotPassWord);
        editTextPhone           = findViewById(R.id.editTextPhoneLogin);
        editTextPassWord        = findViewById(R.id.editTextPassWordLogin);
        textInputLayoutPhone    = findViewById(R.id.textInputLayoutPhone);
        textInputLayoutPassWord   = findViewById(R.id.textInputLayoutPassworđ);
    }

    public void eventOnClick(View view){
        switch (view.getId()){
            case R.id.buttonLogin :
                login();
                break;
            case R.id.textViewSignUp:
                switchScreen();
                break;
            case R.id.textViewForgotPassWord:
                break;
        }
    }

    public void switchScreen(){
        startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
        overridePendingTransition(R.anim.slide_right_in,R.anim.slide_left_out);
    }

    private void login(){
        if (checkValidation()){
            String phone = editTextPhone.getText().toString().trim();
            String password = editTextPassWord.getText().toString().trim();
            requestLogin(phone,password);
        }
    }

    private boolean checkValidation(){
        String phone = editTextPhone.getText().toString().trim();
        String password = editTextPassWord.getText().toString().trim();

        if (phone.isEmpty()){
            editTextPhone.requestFocus();
            textInputLayoutPhone.setErrorEnabled(true);
            textInputLayoutPhone.setError("Vui lòng nhập số điện thoại");
            return false;
        }
        else{
            textInputLayoutPhone.setErrorEnabled(false);
        }

        if (password.isEmpty()){
            editTextPassWord.requestFocus();
            textInputLayoutPassWord.setErrorEnabled(true);
            textInputLayoutPassWord.setError("Vui lòng nhập mật khẩu");
            return false;
        }
        else{
            textInputLayoutPassWord.setErrorEnabled(false);
        }

        return true;
    }

    public void requestLogin(String phone,String password){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient dataClient = APIUltils.getData();
        Call<TokenObject> callback = dataClient.login(phone,password);
        callback.enqueue(new Callback<TokenObject>() {
            @Override
            public void onResponse(Call<TokenObject> call, Response<TokenObject> response) {
                TokenObject tokenObject = response.body();
                String token = tokenObject.getToken();
                if (token.compareTo("error") != 0){
                    Session session = new Session(getApplicationContext());
                    session.setID(tokenObject.getId());
                    session.setToken(tokenObject.getToken());
                    session.setName(tokenObject.getName());
                    session.setRole(tokenObject.getRole());
                    if (tokenObject.getRole().compareTo("2") ==0){
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                    else{
                        if (tokenObject.getRole().compareTo("1") ==0){
                            startActivity(new Intent(getApplicationContext(),AdminActivity.class));
                        }
                        else{
                            startActivity(new Intent(getApplicationContext(), ManageActivity.class));
                        }
                    }
                    //dialog.dismissDialog();
                }
                else{
                    Toast.makeText(LoginActivity.this, "Thông đăng nhập không đúng! Bạn vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TokenObject> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void checkLogin(){
        Session session = new Session(getApplicationContext());
        String token = session.getToken();
        if (token.compareTo("") != 0){
            DataClient dataClient = APIUltils.getData();
            Call<String> callback = dataClient.checkLogin(token); 
            callback.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String mes = response.body();
                    if (mes.compareTo("success") == 0){
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                    else{

                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}