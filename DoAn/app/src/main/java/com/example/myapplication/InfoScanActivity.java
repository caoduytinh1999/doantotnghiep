package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.vaccine_record.VaccineRecord;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoScanActivity extends AppCompatActivity {

    LinearLayout linearLayout,linearLayoutInfoVaccine1,linearLayoutInfoVaccine2;
    TextView textViewName,textViewBirthday,textViewSex,textViewId,textViewAddress,textViewVaccineNumber;
    String id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_scan);
        assign();
        event();
    }

    private void assign() {
        linearLayout    = findViewById(R.id.layoutHideIS);
        textViewName    = findViewById(R.id.textViewNameIS);
        textViewBirthday    = findViewById(R.id.textViewBirthdayIS);
        textViewSex   = findViewById(R.id.textViewSexIS);
        textViewId    = findViewById(R.id.textViewIDIS);
        textViewAddress    = findViewById(R.id.textViewAddressIS);
        textViewVaccineNumber = findViewById(R.id.textViewVaccineNumberIS);
        linearLayoutInfoVaccine1 = findViewById(R.id.layoutInfoVaccineIS1);
        linearLayoutInfoVaccine2 = findViewById(R.id.layoutInfoVaccineIS2);
    }

    private void event(){
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        Log.d("AAA","id" + id);
        setData();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<Patients> callback = data.getInfoAccount(id);
                callback.enqueue(new Callback<Patients>() {
                    @Override
                    public void onResponse(Call<Patients> call, Response<Patients> response) {
                        Patients patients = response.body();
                        dialog.dismissDialog();
                        setInforAccount(patients);
                        linearLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Call<Patients> call, Throwable t) {

                    }
                });
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<VaccineRecord> callback = data.getInfoVaccination(id);
                callback.enqueue(new Callback<VaccineRecord>() {
                    @Override
                    public void onResponse(Call<VaccineRecord> call, Response<VaccineRecord> response) {
                        VaccineRecord record = response.body();
                        setInforVaccine(record);
                    }

                    @Override
                    public void onFailure(Call<VaccineRecord> call, Throwable t) {

                    }
                });
            }
        }).start();
    }

    public void setInforAccount(Patients patients){
        String name = patients.getName();
        name = "Họ tên" + "\n" + name;
        SpannableString content = new SpannableString(name);
        content.setSpan(new StyleSpan(Typeface.BOLD),7,name.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textViewName.setText(content);

        String birthday = "Ngày tháng năm sinh" + "\n" + patients.getBirthday();
        content = new SpannableString(birthday);
        content.setSpan(new StyleSpan(Typeface.BOLD),20,birthday.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textViewBirthday.setText(content);

        String sex = patients.getSex();
        if (sex.compareTo("1")== 0) sex = "Giới tính" + "\n" + "Nam";
        else sex = "Giới tính" + "\n" +"Nữ";
        content = new SpannableString(sex);
        content.setSpan(new StyleSpan(Typeface.BOLD),10,sex.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textViewSex.setText(content);

        String idcode = "Số hộ chiếu/CMND/CCCD" + "\n" +patients.getIdcode();
        content = new SpannableString(idcode);
        content.setSpan(new StyleSpan(Typeface.BOLD),22,idcode.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textViewId.setText(content);

        String address = "Địa chỉ" + "\n" + patients.getAddress();
        content = new SpannableString(address);
        content.setSpan(new StyleSpan(Typeface.BOLD),8,address.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        textViewAddress.setText(content);
    }

    public void setInforVaccine(VaccineRecord vaccine){
        int size = vaccine.getList().size();
        if (size ==0){
            textViewVaccineNumber.setText("Chưa tiêm vắc xin");
            return;
        }

        linearLayoutInfoVaccine1.setVisibility(View.VISIBLE);
        textViewVaccineNumber.setText("Đã tiêm vắc xin (1)" );
        TextView textViewDay1,textViewMY1,textViewVaccineName1,textViewAddress1;
        textViewDay1 = linearLayoutInfoVaccine1.findViewById(R.id.textViewDaySyringe);
        textViewMY1 = linearLayoutInfoVaccine1.findViewById(R.id.textViewMonthYearSyringe);
        textViewVaccineName1 = linearLayoutInfoVaccine1.findViewById(R.id.textViewVaccineType);
        textViewAddress1 = linearLayoutInfoVaccine1.findViewById(R.id.textViewVaccineSite);

        String day1 = vaccine.getList().get(0).getDate().split(" ")[1].split("/")[0];
        if (day1.length() == 1) day1 = "0" + day1;
        textViewDay1.setText(day1);

        String month1 = vaccine.getList().get(0).getDate().split(" ")[1].split("/")[1];
        String year1 = vaccine.getList().get(0).getDate().split(" ")[1].split("/")[2];
        if (month1.length() == 1) month1 = "0" + month1;
        textViewMY1.setText(month1 + "/" + year1);

        textViewVaccineName1.setText(vaccine.getList().get(0).getVaccineName());
        textViewAddress1.setText(vaccine.getList().get(0).getAddress());

        if (size == 2){
            linearLayoutInfoVaccine2.setVisibility(View.VISIBLE);
            textViewVaccineNumber.setText("Đã tiêm vắc xin (2)" );
            TextView textViewDay2,textViewMY2,textViewVaccineName2,textViewAddress2;
            textViewDay2 = linearLayoutInfoVaccine2.findViewById(R.id.textViewDaySyringe);
            textViewMY2 = linearLayoutInfoVaccine2.findViewById(R.id.textViewMonthYearSyringe);
            textViewVaccineName2 = linearLayoutInfoVaccine2.findViewById(R.id.textViewVaccineType);
            textViewAddress2 = linearLayoutInfoVaccine2.findViewById(R.id.textViewVaccineSite);

            String day2 = vaccine.getList().get(1).getDate().split(" ")[1].split("/")[0];
            if (day2.length() == 1) day2 = "0" + day2;
            textViewDay2.setText(day2);

            String month2 = vaccine.getList().get(1).getDate().split(" ")[1].split("/")[1];
            String year2 = vaccine.getList().get(1).getDate().split(" ")[1].split("/")[2];
            if (month2.length() == 1) month2 = "0" + month2;
            textViewMY2.setText(month2 + "/" + year2);

            textViewVaccineName2.setText(vaccine.getList().get(1).getVaccineName());
            textViewAddress2.setText(vaccine.getList().get(1).getAddress());
        }



    }
}