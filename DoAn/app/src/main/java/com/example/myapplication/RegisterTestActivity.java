package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.adapter.DeclareMedicalAdapter;
import com.example.myapplication.adapter.RegisterTestAdapter;
import com.example.myapplication.admin.fragment.RegisterTestDetailFragment;
import com.example.myapplication.fragment.SelectBottomSheetFragment;
import com.example.myapplication.fragment.TestDetailFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterTestActivity extends AppCompatActivity {

    TextView textView;
    RecyclerView recyclerView;
    Button button;
    LinearLayout linearLayout;
    List<RegisterTest> list = new ArrayList<>();
    RegisterTestAdapter adapter;
    RegisterTest registerTest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_test);
        assign();
        event();
    }
    private void assign(){
        textView = findViewById(R.id.textViewNumberOfTest);
        recyclerView = findViewById(R.id.recyclerViewListRegisterTest);
        linearLayout = findViewById(R.id.layoutHideRT);
    }

    private void event() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);

        setData();

    }

    public void replaceFragment(RegisterTest test){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",test);
        TestDetailFragment fragment = new TestDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutRegisterTest,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<RegisterTest>> callback = data.getListRegisterTest(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<List<RegisterTest>>() {
            @Override
            public void onResponse(Call<List<RegisterTest>> call, Response<List<RegisterTest>> response) {
                dialog.dismissDialog();
                list = response.body();
                adapter = new RegisterTestAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {
                        registerTest = (RegisterTest) onClick;
                        createBottomSheet();
                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                textView.setText("Danh sách đăng ký xét nghiệm (" + list.size() + ")");
                linearLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<RegisterTest>> call, Throwable t) {

            }
        });
    }

    public void createBottomSheet(){
        SelectBottomSheetFragment fragment = new SelectBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String code = objectAU.getCode();
                if(code.compareTo("1") == 0){
                    replaceFragment(registerTest);
                }
                else{
                    if (registerTest.getStatus().compareTo("0") != 0){
                        createPopup();
                    }
                    else{
                        createPopup("Bạn có chắc chắn muốn hủy phiếu đăng kí xét nghiệm này không ? ");
                    }
                }
            }
        });
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void createPopup(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_regiser_success);

        Button buttonYes = dialog.findViewById(R.id.buttonClose);
        TextView textView = dialog.findViewById(R.id.textViewTitle);
        TextView textView1 = dialog.findViewById(R.id.textViewContent);

        textView.setText("Thông báo");
        textView1.setText("Không thể hủy phiếu đăng kí xét nghiệm đã xác nhận");
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void createPopup(String message){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRegisterTest(registerTest.getId(),"2");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void updateRegisterTest(String id,String status){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateRegisterTest(id,"00000",status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                setData();
                showToastSuccess("Hủy phiếu đăng kí xét nghiệm thành công");
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Lỗi Hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}