package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.admin.model.TestResult;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestDetailFragment extends Fragment {

    TextView textViewName,textViewHF,textViewDate,textViewTestDate,textViewAddress,textViewTestSite,textViewDateResult,textViewResult;
    String result = "0";
    RegisterTest test;
    String date = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        Bundle bundle = getArguments();
        test = bundle.getParcelable("object");
        textViewName.setText(new Session(getContext()).getName());
        textViewHF.setText(test.getTestingSite());
        textViewTestSite.setText(test.getTestingSite());
        textViewTestDate.setText(test.getTestingDate());
        textViewDate.setText(test.getDate());
        textViewAddress.setText(test.getAddress());
    }

    private void assign(View view) {
        textViewName  = view.findViewById(R.id.textViewNameTAD);
        textViewHF    = view.findViewById(R.id.textViewHealthFacilityTAD);
        textViewAddress = view.findViewById(R.id.textViewAddressTAD);
        textViewDate    = view.findViewById(R.id.textViewDateTAD);
        textViewTestDate = view.findViewById(R.id.textViewTesingDateTAD);
        textViewTestSite = view.findViewById(R.id.textViewTestingSiteTAD);
        textViewDateResult = view.findViewById(R.id.textViewDateResultTAD);
        textViewResult = view.findViewById(R.id.textViewResultTAD);
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<TestResult> callback = data.getInfoTestResult(test.getId());
        callback.enqueue(new Callback<TestResult>() {
            @Override
            public void onResponse(Call<TestResult> call, Response<TestResult> response) {
                dialog.dismissDialog();
                TestResult testResult = response.body();
                String dateTest = testResult.getDateResult();
                if (dateTest.compareTo("") == 0) {
                    dateTest = "Chưa Có";
                }
                else{
                }
                textViewDateResult.setText(dateTest);

                String result = testResult.getResult();
                if (result.compareTo("") == 0) result = "Chưa Có";
                else {
                    if (result.compareTo("0") == 0) result = "Âm Tính";
                    else result = "Dương Tính";
                }
                textViewResult.setText(result);
            }

            @Override
            public void onFailure(Call<TestResult> call, Throwable t) {
                dialog.dismissDialog();
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }
}