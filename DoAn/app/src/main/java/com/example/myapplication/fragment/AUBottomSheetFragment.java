package com.example.myapplication.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionActivity;
import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.administrative_units.District;
import com.example.myapplication.model.administrative_units.Province;
import com.example.myapplication.model.administrative_units.Ward;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AUBottomSheetFragment extends BottomSheetDialogFragment {
    private List<ObjectAU> list;
    private IOnClickItemListener listener;
    private AUAdapter adapter = new AUAdapter();
    private String title;
    private String codename;
    private String code;

    public AUBottomSheetFragment(List<ObjectAU> list, IOnClickItemListener listener,String title,String codename,String code) {
        this.list = list;
        this.listener = listener;
        this.title = title;
        this.code = code;
        this.codename = codename;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (codename.compareTo(RegisterInjectionActivity.CODE_NAME_PROVINCE) == 0){
            getListProvince();
        }
        if (codename.compareTo(RegisterInjectionActivity.CODE_NAME_DISTRICT) == 0){
            getListDistrict(code);
        }
        if (codename.compareTo(RegisterInjectionActivity.CODE_NAME_WARD) ==0){
            getListWard(code);
        }
        BottomSheetDialog dialog = (BottomSheetDialog) new BottomSheetDialog(getContext(),R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_administrative_units,null,false);
        dialog.setContentView(view);
        TextView textView = view.findViewById(R.id.textViewTitleBSAU);
        textView.setText(title);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewListAU);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        adapter = new AUAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                listener.onClick(onClick);
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        RecyclerView.ItemDecoration decoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(decoration);
        adapter.notifyDataSetChanged();
        setupFullHeight(dialog);
        return dialog;
    }

    public void getListProvince(){
        DataClient data = APIUltils.getAdministrativeUnits();
        Call<List<Province>> callback = data.getListProvince();
        callback.enqueue(new Callback<List<Province>>() {
            @Override
            public void onResponse(Call<List<Province>> call, Response<List<Province>> response) {
                List<Province> list_province = new ArrayList<>();
                list_province = response.body();
                list.add(new ObjectAU("","Không xác định"));
                for (Province item : list_province){
                    list.add(new ObjectAU(item.getCode(),item.getName()));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Province>> call, Throwable t) {
                Toast.makeText(getContext(), "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getListWard(String code){
        DataClient data = APIUltils.getAdministrativeUnits();
        Call<District> callback = data.getListWard(code);
        callback.enqueue(new Callback<District>() {
            @Override
            public void onResponse(Call<District> call, Response<District> response) {
                District district = response.body();
                List<Ward> wardList = district.getWards();
                list.add(new ObjectAU("","Không xác định"));
                for (Ward item :wardList){
                    list.add(new ObjectAU(item.getCode(),item.getName()));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<District> call, Throwable t) {

            }
        });
    }

    private void getListDistrict(String code) {
        DataClient data = APIUltils.getAdministrativeUnits();
        Call<Province> callback = data.getListDistrict(code);
        callback.enqueue(new Callback<Province>() {
            @Override
            public void onResponse(Call<Province> call, Response<Province> response) {
                Province province = response.body();
                List<District> districtList = province.getDistricts();
                list.add(new ObjectAU("","Không xác định"));
                for (District item : districtList){
                    list.add(new ObjectAU(item.getCode(),item.getName()));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Province> call, Throwable t) {

            }
        });
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }


}
