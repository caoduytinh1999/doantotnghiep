package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.adapter.RegisterSyringeAdapter;
import com.example.myapplication.adapter.RegisterTestAdapter;
import com.example.myapplication.admin.activity.ConfirmedRSActivity;
import com.example.myapplication.fragment.ButtonBottomSheetFragment;
import com.example.myapplication.fragment.RI3Fragment;
import com.example.myapplication.fragment.RIDetailFragment;
import com.example.myapplication.fragment.SelectBottomSheetFragment;
import com.example.myapplication.fragment.SelectionBottomSheetFragment;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterInjectionHistoryActivity extends AppCompatActivity {

    TextView textView;
    RecyclerView recyclerView;
    LinearLayout linearLayout;
    public static String birthday = "";
    List<RegisterSyringe> list = new ArrayList<>();
    RegisterSyringeAdapter adapter;
    Patients patients;
    RegisterSyringe syringe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_injection_history);
        assign();
        event();
    }

    public void assign(){
        textView = findViewById(R.id.textViewNumberOfSyringe);
        recyclerView = findViewById(R.id.recyclerViewListRegisterInjectionHistory);
        linearLayout = findViewById(R.id.layoutHideRI);
    }

    public void event(){
        setData();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<Patients> callback = data.getInfoAccount(new Session(getApplicationContext()).getID());
                callback.enqueue(new Callback<Patients>() {
                    @Override
                    public void onResponse(Call<Patients> call, Response<Patients> response) {
                        patients = response.body();
                        birthday = patients.getBirthday();
                        dialog.dismissDialog();
                    }

                    @Override
                    public void onFailure(Call<Patients> call, Throwable t) {

                    }
                });
            }
        }).start();

        loadData();
    }

    private void loadData(){
        DataClient data = APIUltils.getData();
        Call<List<RegisterSyringe>> callback = data.getListRegisterSyringe(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<List<RegisterSyringe>>() {
            @Override
            public void onResponse(Call<List<RegisterSyringe>> call, Response<List<RegisterSyringe>> response) {
                list = response.body();
                adapter = new RegisterSyringeAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {
                        syringe = (RegisterSyringe) onClick;
                        createBottomSheet();
                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                textView.setText("Danh sách đăng ký tiêm (" + list.size() + ")");
                linearLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<RegisterSyringe>> call, Throwable t) {
                Toast.makeText(RegisterInjectionHistoryActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("patients",patients);
        bundle.putParcelable("syringe",syringe);
        RIDetailFragment fragment = new RIDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
        fragmentTransaction.replace(R.id.layoutHisrotyRI,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void createBottomSheet(){
        SelectBottomSheetFragment fragment = new SelectBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String code = objectAU.getCode();
                if(code.compareTo("1") == 0){
                    replaceFragment();
                }
                else{
                    if (syringe.getStatus().compareTo("0") != 0){
                        createPopup();
                    }
                    else{
                        createPopup("Bạn có chắc chắn muốn hủy phiếu đăng kí tiêm này không ? ");
                    }
                }
            }
        });
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void createPopup(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_regiser_success);

        Button buttonYes = dialog.findViewById(R.id.buttonClose);
        TextView textView = dialog.findViewById(R.id.textViewTitle);
        TextView textView1 = dialog.findViewById(R.id.textViewContent);

        textView.setText("Thông báo");
        textView1.setText("Không thể hủy phiếu đăng kí tiêm đã xác nhận");
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void createPopup(String message){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRegisterSyringe(syringe.getId(),"00000","2");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void updateRegisterSyringe(String id,String idStaff,String status){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.cancelRegiserSyringe(id,idStaff,status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                loadData();
                dialog.dismissDialog();
                showToastSuccess("Hủy bỏ phiếu đăng kí tiêm thành công");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Lỗi Hệ Thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}