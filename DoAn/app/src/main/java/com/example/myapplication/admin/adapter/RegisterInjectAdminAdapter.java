package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RegisterInjectAdminAdapter extends RecyclerView.Adapter<RegisterInjectAdminAdapter.ViewHolder>{
    List<RegisterInjectAdmin> list;
    IOnClickItemListener listener;

    public RegisterInjectAdminAdapter(List<RegisterInjectAdmin> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_register_injection,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new RegisterInjectAdminAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        RegisterInjectAdmin syringe = list.get(position);
        String date = syringe.getDate();
        String day = date.split("/")[0];
        String month = date.split("/")[1];
        String year = date.split("/")[2];
        if (day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);
        if (month.length() == 1) month = "0" + month;
        holder.textViewMY.setText(month + "/" + year);
        String status = syringe.getStatus();
        if (status.compareTo("0") ==0){
            status = "Chưa Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.theme_main));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status));
        }
        if (status.compareTo("1") == 0){
            status = "Đã Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.confirmed));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_confirmed));
        }
        if (status.compareTo("2") == 0){
            status = "Đã Hủy";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.red));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_cancelled));
        }
        holder.textViewStatus.setText(status);
        holder.textViewName.setText(syringe.getPatients().getName());
        holder.textViewBirthday.setText(syringe.getPatients().getBirthday());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewName,textViewBirthday,textViewStatus;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewHistoryDayRI);
            textViewMY = itemView.findViewById(R.id.textViewHistoryMYRI);
            textViewName = itemView.findViewById(R.id.textViewHistoryNameRI);
            textViewBirthday = itemView.findViewById(R.id.textViewHistoryBirthdayRI);
            textViewStatus = itemView.findViewById(R.id.textViewStatusHistoryRI);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
