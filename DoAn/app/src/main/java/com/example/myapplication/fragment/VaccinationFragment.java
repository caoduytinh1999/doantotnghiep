package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.adapter.VaccineInfoAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.vaccine_record.VaccineRecord;
import com.example.myapplication.model.vaccine_record.VaccineRecordDetail;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VaccinationFragment extends Fragment {

    RecyclerView recyclerView;
    List<VaccineRecordDetail> list = new ArrayList<>();
    VaccineInfoAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_vaccination, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new VaccineInfoAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        });
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListVaccination);
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<VaccineRecord> callback = data.getInfoVaccination(new Session(getContext()).getID());
        callback.enqueue(new Callback<VaccineRecord>() {
            @Override
            public void onResponse(Call<VaccineRecord> call, Response<VaccineRecord> response) {
                VaccineRecord record = response.body();
                list.addAll(record.getList());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<VaccineRecord> call, Throwable t) {

            }
        });
    }
}