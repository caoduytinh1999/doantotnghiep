package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.StaffAdapter;
import com.example.myapplication.admin.fragment.StaffFragment;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Staff;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListStaffActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ImageView imageView;
    String idHF = "";
    List<Staff> list = new ArrayList<>();
    StaffAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_staff);

        recyclerView = findViewById(R.id.recyclerViewListStaff);
        imageView = findViewById(R.id.imageViewAdd);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new StaffAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                Staff staff = (Staff) onClick;
                replaceFragment(staff);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        assignData();
    }

    private void assignData() {
        list.clear();
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<Staff>> callback = data.getListStaff();
        callback.enqueue(new Callback<List<Staff>>() {
            @Override
            public void onResponse(Call<List<Staff>> call, Response<List<Staff>> response) {
                list.addAll(response.body());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<Staff>> call, Throwable t) {

            }
        });
    }

    public void replaceFragment(Staff staff){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",staff);
        StaffFragment fragment = new StaffFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthFacilities,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}