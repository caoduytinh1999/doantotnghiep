package com.example.myapplication.model.health_quotient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Health {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("type")
    @Expose
    private String type;

    public Health(String id, String time, String value,String type) {
        this.id = id;
        this.time = time;
        this.value = value;
        this.type = type;
    }



    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
