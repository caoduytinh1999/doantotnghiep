package com.example.myapplication.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.fragment.CovidTestFragment;
import com.example.myapplication.fragment.GeneralFragment;
import com.example.myapplication.fragment.HistoryFragment;
import com.example.myapplication.fragment.NoteFragment;
import com.example.myapplication.fragment.VaccinationFragment;

public class ViewPagerHealthRecordAdapter extends FragmentStatePagerAdapter {
    public ViewPagerHealthRecordAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new GeneralFragment();
            case 1 :
                return new VaccinationFragment();
            case 2 :
                return new HistoryFragment();
            case 3 :
                return new CovidTestFragment();
            case 4 :
                return new NoteFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Tổng quan";
                break;
            case 1 :
                title = "Tiêm chủng Covid - 19";
                break;
            case 2 :
                title = "Lịch sử khám";
                break;
            case 3 :
                title = "Xét Nghiệm Covid";
                break;
            case 4 :
                title = "Ghi chú";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;

    }
}
