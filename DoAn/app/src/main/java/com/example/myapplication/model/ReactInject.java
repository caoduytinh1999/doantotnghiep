package com.example.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReactInject extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("injectRecord")
    @Expose
    private InjectRecord injectRecord;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("symptom")
    @Expose
    private String symptom;

    public ReactInject(String id, InjectRecord idInjectRecord, String time, String symptom) {
        this.id = id;
        this.injectRecord = idInjectRecord;
        this.time = time;
        this.symptom = symptom;
    }

    protected ReactInject(Parcel in) {
        id = in.readString();
        time = in.readString();
        symptom = in.readString();
    }

    public static final Creator<ReactInject> CREATOR = new Creator<ReactInject>() {
        @Override
        public ReactInject createFromParcel(Parcel in) {
            return new ReactInject(in);
        }

        @Override
        public ReactInject[] newArray(int size) {
            return new ReactInject[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public InjectRecord getInjectRecord() {
        return injectRecord;
    }

    public void setInjectRecord(InjectRecord injectRecord) {
        this.injectRecord = injectRecord;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(time);
        dest.writeString(symptom);
    }
}
