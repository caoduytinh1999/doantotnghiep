package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.ViewPagerAdminRIRAdapter;
import com.example.myapplication.admin.adapter.ViewPagerAdminRegisterInjectAdapter;
import com.google.android.material.tabs.TabLayout;

public class RegisterInjectRecordActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_inject_record);
        assign();
        event();
    }

    private void event() {
        ViewPagerAdminRIRAdapter adapter = new ViewPagerAdminRIRAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void assign() {
        tabLayout = findViewById(R.id.tabLayoutRIRAdmin);
        viewPager = findViewById(R.id.viewPagerRIRAdmin);
    }
}