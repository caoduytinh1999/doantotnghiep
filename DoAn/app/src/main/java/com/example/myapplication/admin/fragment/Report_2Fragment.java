package com.example.myapplication.admin.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.admin.model.VaccineInfo;
import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Report_2Fragment extends Fragment {
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    List<ObjectAU> list_au = new ArrayList<>();
    AUAdapter adapter = new AUAdapter();
    String code_province = "";
    String code_district = "";
    String code_ward = "";
    String noi = "1";
    List<VaccineInfo> list = new ArrayList<>();
    EditText editTextProvince,editTextDistrict,editTextWard;
    Button button;
    TableLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_province.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,code_province,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_district.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else{
                    createBottomSheet(CODE_NAME_WARD,code_district,"Chọn Phường/Xã");
                }
            }
        });



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });
    }

    private void assign(View view) {
        editTextProvince = view.findViewById(R.id.editTextCityNameRI);
        editTextDistrict = view.findViewById(R.id.editTextDistricNameRI);
        editTextWard = view.findViewById(R.id.editTextWardNameRI);
        button = view.findViewById(R.id.buttonFillter);
        tabLayout = view.findViewById(R.id.tableLayout1);
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,getActivity().findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void createBottomSheet(String codename,String code,String title){
        list_au.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(list_au, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (code_province.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    code_province = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (code_district.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    code_district = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    code_ward = objectAU.getCode();
                }
            }
        },title,codename,code);
        fragment.show(getActivity().getSupportFragmentManager(),fragment.getTag());
    }

    public void getData(){
        tabLayout.removeViews(1,list.size());
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        String address = editTextProvince.getText().toString();
        if (editTextDistrict.getText().toString().isEmpty() == false){
            address = editTextDistrict.getText().toString() + "," + address;
        }

        if (editTextWard.getText().toString().isEmpty() == false){
            address = editTextWard.getText().toString() + "," + address;
        }
        DataClient data = APIUltils.getData();
        Call<List<VaccineInfo>> callback = data.getListVaccineReport(address);
        callback.enqueue(new Callback<List<VaccineInfo>>() {
            @Override
            public void onResponse(Call<List<VaccineInfo>> call, Response<List<VaccineInfo>> response) {
                list.addAll(response.body());
                setData();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<VaccineInfo>> call, Throwable t) {
                Log.d("AAA",t.getMessage());
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void setData(){
        int size = list.size();
        for (int i = 0 ; i < size ; i++){
            VaccineInfo vaccineInfo = list.get(i);
            TableRow tableRow = new TableRow(getContext());

            TextView textViewIndex = new TextView(getContext());
            textViewIndex.setText(String.valueOf(i + 1));
            textViewIndex.setTextColor(Color.BLACK);
            textViewIndex.setGravity(Gravity.CENTER);
            textViewIndex.setPadding(10,10,10,10);
            textViewIndex.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            tableRow.addView(textViewIndex);

            TextView textViewName = new TextView(getContext());
            textViewName.setText(vaccineInfo.getNumberOfInject());
            textViewName.setTextColor(Color.BLACK);
            textViewName.setGravity(Gravity.CENTER);
            textViewName.setPadding(10,10,10,10);
            textViewName.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            tableRow.addView(textViewName);

            TextView textViewDate = new TextView(getContext());
            textViewDate.setTextColor(Color.BLACK);
            textViewDate.setPadding(10,10,10,10);
            textViewDate.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            textViewDate.setText(vaccineInfo.getVaccine());
            tableRow.addView(textViewDate);

            TextView textViewAddress = new TextView(getContext());
            textViewAddress.setTextColor(Color.BLACK);
            textViewAddress.setPadding(10,10,10,10);
            textViewAddress.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            textViewAddress.setText(vaccineInfo.getSite().getName());
            tableRow.addView(textViewAddress);

            tabLayout.addView(tableRow);
        }
    }
}