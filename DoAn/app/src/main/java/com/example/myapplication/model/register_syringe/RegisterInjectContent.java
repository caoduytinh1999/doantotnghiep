package com.example.myapplication.model.register_syringe;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterInjectContent {
    @SerializedName("idRegisterSyringe")
    @Expose
    private String idRegisterSyringe;

    @SerializedName("question")
    @Expose
    private QuestionInject question;

    @SerializedName("answer")
    @Expose
    private String answer;

    @SerializedName("answerContent")
    @Expose
    private String answerContent;

    public RegisterInjectContent(String idRegisterSyringe, QuestionInject question, String answer, String answerContent) {
        this.idRegisterSyringe = idRegisterSyringe;
        this.question = question;
        this.answer = answer;
        this.answerContent = answerContent;
    }

    public String getIdRegisterSyringe() {
        return idRegisterSyringe;
    }

    public void setIdRegisterSyringe(String idRegisterSyringe) {
        this.idRegisterSyringe = idRegisterSyringe;
    }

    public QuestionInject getQuestion() {
        return question;
    }

    public void setQuestion(QuestionInject question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }
}
