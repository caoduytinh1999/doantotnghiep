package com.example.myapplication.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthServiceAdapter;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

public class SelectionBottomSheetFragment extends BottomSheetDialogFragment {
    IOnClickItemListener listener;

    public SelectionBottomSheetFragment(IOnClickItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(), R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_selection_admin,null,false);
        bottomSheetDialog.setContentView(view);
        Button buttonConfirm = bottomSheetDialog.findViewById(R.id.buttonBSConfirm);
        Button buttonCancel = bottomSheetDialog.findViewById(R.id.buttonBSCancel);
        Button button = bottomSheetDialog.findViewById(R.id.buttonDetail);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAU objectAU = new ObjectAU("0","detail");
                listener.onClick(objectAU);
                bottomSheetDialog.dismiss();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAU objectAU = new ObjectAU("1","confirm");
                listener.onClick(objectAU);
                bottomSheetDialog.dismiss();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ObjectAU objectAU = new ObjectAU("2","cancel");
                listener.onClick(objectAU);
                bottomSheetDialog.dismiss();
            }
        });

        return  bottomSheetDialog;
    }
}
