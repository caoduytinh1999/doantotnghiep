package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.activity.ConfirmedRSActivity;
import com.example.myapplication.admin.adapter.RegisterInjectAdminAdapter;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.fragment.RIDetailFragment;
import com.example.myapplication.fragment.SelectionBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RSUnconfirmedFragment extends Fragment {

    private static final String UNCONFIRMED_STATUS = "0";
    RecyclerView recyclerView;
    List<RegisterInjectAdmin> list = new ArrayList<>();
    RegisterInjectAdminAdapter adapter;
    RegisterInjectAdmin syringe;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_r_s_unconfirmed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }


    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListRSUnconfirmed);
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RegisterInjectAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                syringe = (RegisterInjectAdmin) onClick;
                createBottomSheet((RegisterInjectAdmin) onClick);
            }
        });
        recyclerView.setAdapter(adapter);
    }



    public void createBottomSheet(RegisterInjectAdmin syringe){
        SelectionBottomSheetFragment sheetFragment = new SelectionBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String status = objectAU.getCode();
                String mes = "";
                String id = syringe.getId();
                switch (status){
                    case "0":
                        replaceFragment();
                        break;
                    case "1" :
                        mes = "Bạn chắc chắn có muốn Xác Nhận phiếu đăng kí tiêm của " + syringe.getPatients().getName() + " vào lúc " + syringe.getDate() + " không ? ";
                        createPopup(mes,id,status);
                        break;
                    case "2" :
                        mes = "Bạn chắc chắn có muốn Xóa phiếu đăng kí tiêm của " + syringe.getPatients().getName() + " vào lúc " + syringe.getDate() + " không ? ";
                        createPopup(mes,id,status);
                        break;
                }
            }
        });
        sheetFragment.show(getActivity().getSupportFragmentManager(),sheetFragment.getTag());
    }

    public void createPopup(String message,String id,String status){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (status.compareTo("1") == 0){
                    confirmRegisterSyringe();
                }
                else{
                    updateRegisterSyringe(id,new Session(getContext()).getID(),status);
                }
            }
        });

        dialog.show();
    }

    private void setData() {
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<RegisterInjectAdmin>> callback = data.getLURSA(new Session(getContext()).getID(),UNCONFIRMED_STATUS);
        callback.enqueue(new Callback<List<RegisterInjectAdmin>>() {
            @Override
            public void onResponse(Call<List<RegisterInjectAdmin>> call, Response<List<RegisterInjectAdmin>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<RegisterInjectAdmin>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void updateRegisterSyringe(String id,String idStaff,String status){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.cancelRegiserSyringe(id,idStaff,status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                list.remove(syringe);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
                showToastSuccess("Hủy bỏ phiếu đăng kí tiêm thành công");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void confirmRegisterSyringe(){
        Intent intent = new Intent(getContext(),ConfirmedRSActivity.class);
        intent.putExtra("object",syringe);
        intent.putExtra("noi","1");
        startActivity(intent);
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,getActivity().findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("syringe",syringe);
        RIDetailAdminFragment fragment = new RIDetailAdminFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
        fragmentTransaction.replace(R.id.layoutRI,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}