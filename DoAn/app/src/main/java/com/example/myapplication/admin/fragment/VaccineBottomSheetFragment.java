package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.admin.adapter.VaccineAdapter;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VaccineBottomSheetFragment extends BottomSheetDialogFragment {
    List<String> list;
    IOnClickItemListener listener;

    public VaccineBottomSheetFragment(List<String> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) new BottomSheetDialog(getContext(), R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_vaccine,null,false);
        dialog.setContentView(view);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewListVaccineBS);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        VaccineAdapter adapter = new VaccineAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                listener.onClick(onClick);
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        RecyclerView.ItemDecoration decoration = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(decoration);
        adapter.notifyDataSetChanged();
        return dialog;
    }
}
