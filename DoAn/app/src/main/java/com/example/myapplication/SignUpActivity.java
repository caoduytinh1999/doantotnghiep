package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    TextView textViewLogin;
    EditText editTextPhone, editTextName, editTextBirthday,editTextSex,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextJob
            ,editTextCMND,editTextEmail,editTextBHYT,editTextPassWord, getEditTextPassWordRepeat;
    TextInputLayout textInputLayoutPhone,textInputLayoutName,textInputLayoutBirthday,textInputLayoutSex,textInputLayoutProvince
            ,textInputLayoutDistrict,textInputLayoutWard,textInputLayoutPlace,textInputLayoutJob,textInputLayoutCMND,
            textInputLayoutBHYT,textInputLayoutEmail,textInputLayoutPassword,getTextInputLayoutPasswordRepeat;
    Button buttonSignUp;
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    String province_code = "";
    String district_code = "";
    String ward_code = "";
    String province = "";
    String district = "";
    String ward = "";
    List<ObjectAU> auList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Mapped();
        Event();
    }

    private void Event() {
        textViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               switchScreen();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        editTextBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        editTextSex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextSex.getText().toString().compareTo("Nam") ==0) editTextSex.setText("Nữ");
                else editTextSex.setText("Nam");
            }
        });

        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (province_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,province_code,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (district_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else {
                    createBottomSheet(CODE_NAME_WARD,district_code,"Chọn Phường/Xã");
                }
            }
        });
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        switchScreen();
    }

    private void switchScreen(){
        startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
        overridePendingTransition(R.anim.slide_left_in,R.anim.slide_right_out);
    }

    private void Mapped() {
        textViewLogin               = findViewById(R.id.textViewLogin);
        editTextName                = findViewById(R.id.editTextName);
        editTextBirthday            = findViewById(R.id.editTextBirthday);
        editTextSex                 = findViewById(R.id.editTextSex);
        editTextProvince            = findViewById(R.id.editTextProvince);
        editTextWard                = findViewById(R.id.editTextWard);
        editTextDistrict            = findViewById(R.id.editTextDistric);
        editTextPlace               = findViewById(R.id.editTextPlace);
        editTextEmail               = findViewById(R.id.editTextEmail);
        editTextJob                 = findViewById(R.id.editTextJob);
        editTextCMND                = findViewById(R.id.editTextCMND);
        editTextBHYT                = findViewById(R.id.editTextBHYT);
        editTextPassWord            = findViewById(R.id.editTextPassWordSignUp);
        getEditTextPassWordRepeat   = findViewById(R.id.editTextPassWordRepeat);
        editTextPhone               = findViewById(R.id.editTextPhoneSignUp);
        buttonSignUp                = findViewById(R.id.buttionSignUp);
        textInputLayoutPhone        = findViewById(R.id.textInputLayoutPhoneSignup);
        textInputLayoutName         = findViewById(R.id.textInputLayoutNameSign);
        textInputLayoutBirthday     = findViewById(R.id.textInputLayoutBirthday);
        textInputLayoutSex          = findViewById(R.id.textInputLayoutSex);
        textInputLayoutProvince     = findViewById(R.id.textInputLayoutProvince);
        textInputLayoutDistrict     = findViewById(R.id.textInputLayoutDistrict);
        textInputLayoutWard     = findViewById(R.id.textInputLayoutWard);
        textInputLayoutPlace     = findViewById(R.id.textInputLayoutPlace);
        textInputLayoutEmail     = findViewById(R.id.textInputLayoutEmail);
        textInputLayoutJob     = findViewById(R.id.textInputLayoutJob);
        textInputLayoutBHYT     = findViewById(R.id.textInputLayoutBHYT);
        textInputLayoutCMND     = findViewById(R.id.textInputLayoutCMND);
        textInputLayoutPassword     = findViewById(R.id.textInputLayoutPassworđSignup);
        getTextInputLayoutPasswordRepeat = findViewById(R.id.textInputLayoutPassworđRepeatSignup);
    }

    public void createBottomSheet(String codename,String code,String title){
        auList.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(auList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (province_code.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    province_code = objectAU.getCode();
                    province = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (district_code.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    district_code = objectAU.getCode();
                    district = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    ward_code = objectAU.getCode();
                    ward = objectAU.getName();
                }
            }
        },title,codename,code);
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void showDatePicker(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
                editTextBirthday.setText(date);
            }
        }, year - 18, month, day);
        Date date = new Date(year - 18,month++,day);
        dialog.getDatePicker().setMaxDate(date.getTime());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void signup(){
        String phone = editTextPhone.getText().toString().trim();
        String name = editTextName.getText().toString().trim();
        String password = editTextPassWord.getText().toString().trim();
        String passwordrepeat = getEditTextPassWordRepeat.getText().toString();

        if (checkValidate(phone,name,password,passwordrepeat)){
            requestSignup(phone);
        }
    }

    public boolean checkValidate(String phone,String name,String password,String passwordrepeat){
        if (phone.compareTo("") == 0){
            textInputLayoutPhone.setErrorEnabled(true);
            textInputLayoutPhone.setError("Vui lòng nhập số diện thoại");
            return false;
        }
        else{
            if (phone.length() < 10){
                textInputLayoutPhone.setErrorEnabled(true);
                textInputLayoutPhone.setError("Vui lòng nhập đủ 10 số");
                return false;
            }
            else textInputLayoutPhone.setErrorEnabled(false);
        }
        if (name.compareTo("") == 0){
            textInputLayoutName.setErrorEnabled(true);
            textInputLayoutName.setError("Vui lòng nhập Họ và tên");
            return false;
        }
        else textInputLayoutName.setErrorEnabled(false);

        if (editTextBirthday.getText().toString().isEmpty()){
            textInputLayoutBirthday.setErrorEnabled(true);
            textInputLayoutBirthday.setError("Vui lòng chọn ngày sinh");
            return false;
        }
        else textInputLayoutBirthday.setErrorEnabled(false);

        if (editTextProvince.getText().toString().isEmpty()){
            textInputLayoutProvince.setErrorEnabled(true);
            textInputLayoutProvince.setError("Vui lòng chọn tỉnh/thành phố");
            return false;
        }
        else textInputLayoutProvince.setErrorEnabled(false);

        if (editTextDistrict.getText().toString().isEmpty()){
            textInputLayoutDistrict.setErrorEnabled(true);
            textInputLayoutDistrict.setError("Vui lòng chọn quận/huyện");
            return false;
        }
        else textInputLayoutDistrict.setErrorEnabled(false);

        if (editTextWard.getText().toString().isEmpty()){
            textInputLayoutWard.setErrorEnabled(true);
            textInputLayoutWard.setError("Vui lòng chọn phường/xã");
            return false;
        }
        else textInputLayoutWard.setErrorEnabled(false);

        if (editTextPlace.getText().toString().isEmpty()){
            textInputLayoutPlace.setErrorEnabled(true);
            textInputLayoutPlace.setError("Vui lòng nhập số nhà/thôn/xóm");
            return false;
        }
        else textInputLayoutPlace.setErrorEnabled(false);

        if (editTextJob.getText().toString().isEmpty()){
            textInputLayoutJob.setErrorEnabled(true);
            textInputLayoutJob.setError("Vui lòng nhập nghề nghiệp");
            return false;
        }
        else textInputLayoutJob.setErrorEnabled(false);

        if (editTextEmail.getText().toString().isEmpty()){
            textInputLayoutEmail.setErrorEnabled(true);
            textInputLayoutEmail.setError("Vui lòng nhập email");
            return false;
        }
        else textInputLayoutEmail.setErrorEnabled(false);

        if (editTextCMND.getText().toString().isEmpty()){
            textInputLayoutCMND.setErrorEnabled(true);
            textInputLayoutCMND.setError("Vui lòng nhập CMND");
            return false;
        }
        else textInputLayoutCMND.setErrorEnabled(false);

        if (editTextBHYT.getText().toString().isEmpty()){
            textInputLayoutBHYT.setErrorEnabled(true);
            textInputLayoutBHYT.setError("Vui lòng nhập bảo hiểm y tế");
            return false;
        }
        else textInputLayoutBHYT.setErrorEnabled(false);

        if (password.compareTo("") ==0){
            textInputLayoutPassword.setErrorEnabled(true);
            textInputLayoutPassword.setError("Vui lòng nhập mật khẩu");
            return false;
        }
        else{
            if (password.length() < 6){
                textInputLayoutPassword.setError("Mật khẩu phải từ 6 kí tự trở lên");
                textInputLayoutPassword.setErrorEnabled(true);
                return false;
            }
        }
        if (passwordrepeat.compareTo(password) !=0){
            getTextInputLayoutPasswordRepeat.setError("Vui lòng xác nhận lại mật khẩu");
            getTextInputLayoutPasswordRepeat.setErrorEnabled(true);
            return false;
        }
        else{
            getTextInputLayoutPasswordRepeat.setErrorEnabled(false);
        }
        return true;
    }

    public void requestSignup(String phone){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.checkSignup(phone);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String message = response.body();
                if (message.compareTo("success") == 0){
                    Intent intent = new Intent(getApplicationContext(),PhoneConfirmActivity.class);
                    String phone = editTextPhone.getText().toString();
                    String name = editTextName.getText().toString();
                    String birthday = editTextBirthday.getText().toString();
                    String sex = editTextSex.getText().toString();
                    if (sex.compareTo("Nam") == 0) sex = "1";
                    else sex = "2";
                    String address = editTextPlace.getText() + "," + editTextWard.getText() + "," + editTextDistrict.getText() + "," + editTextProvince.getText();
                    String job = editTextJob.getText().toString();
                    String email = editTextEmail.getText().toString();
                    String bhyt = editTextBHYT.getText().toString();
                    String cmnd = editTextCMND.getText().toString();
                    Patients patients = new Patients("",phone,name,birthday,sex,address,job,cmnd,email,bhyt);
                    intent.putExtra("object",patients);
                    intent.putExtra("phone",phone);
                    intent.putExtra("name",editTextName.getText().toString());
                    intent.putExtra("password",editTextPassWord.getText().toString());
                    dialog.dismissDialog();
                    startActivity(intent);
                }
                else{
                    Toast.makeText(SignUpActivity.this, "Số điện thoại này đã được đăng ký! Vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show();
                    dialog.dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dismissLoadingPopup(Dialog dialog){
        dialog.dismiss();
    }

    public void createLodingPopup(Dialog dialog){
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_loading);

        dialog.show();
    }
}