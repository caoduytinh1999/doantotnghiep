package com.example.myapplication.model.register_syringe;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterSyringe extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idPatients")
    @Expose
    private String idPatients;

    @SerializedName("idStaff")
    @Expose
    private String idStaff;

    @SerializedName("objectSyringe")
    @Expose
    private String objectSyringe;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("list")
    @Expose
    private List<RegisterSyringeContent> list;

    public RegisterSyringe(String id, String idPatients, String idStaff, String objectSringe, String date, String address, String status, List<RegisterSyringeContent> list) {
        this.id = id;
        this.idPatients = idPatients;
        this.idStaff = idStaff;
        this.objectSyringe = objectSringe;
        this.date = date;
        this.address = address;
        this.status = status;
        this.list = list;
    }

    protected RegisterSyringe(Parcel in) {
        id = in.readString();
        idPatients = in.readString();
        idStaff = in.readString();
        objectSyringe = in.readString();
        date = in.readString();
        address = in.readString();
        status = in.readString();
    }

    public static final Creator<RegisterSyringe> CREATOR = new Creator<RegisterSyringe>() {
        @Override
        public RegisterSyringe createFromParcel(Parcel in) {
            return new RegisterSyringe(in);
        }

        @Override
        public RegisterSyringe[] newArray(int size) {
            return new RegisterSyringe[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPatients() {
        return idPatients;
    }

    public void setIdPatients(String idPatients) {
        this.idPatients = idPatients;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getObjectSringe() {
        return objectSyringe;
    }

    public void setObjectSringe(String objectSringe) {
        this.objectSyringe = objectSringe;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RegisterSyringeContent> getList() {
        return list;
    }

    public void setList(List<RegisterSyringeContent> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(idPatients);
        dest.writeString(idStaff);
        dest.writeString(objectSyringe);
        dest.writeString(date);
        dest.writeString(address);
        dest.writeString(status);
    }
}
