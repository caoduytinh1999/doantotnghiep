package com.example.myapplication.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.HealthRecordDetailActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_quotient.BloodPressure;
import com.example.myapplication.model.health_quotient.BloodSugar;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthChartFragment extends Fragment {

    List<Health> list = new ArrayList<>();
    Health health;
    TextView textViewTitle,textViewNumber;
    ImageView button;
    LineChart chart;
    HealthAdapter adapter;
    RecyclerView recyclerView;
    String title = "";
    String content = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_health_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        HealthRecordDetailActivity.CURRENT_PAGE = 1;
    }

    private void event(View view) {

        Bundle bundle = this.getArguments();
        title = bundle.getString("title");
        switch (title){
            case "BLOODPRESSURE" :
                textViewTitle.setText("Biểu đồ huyết áp");
                content = "Huyết Áp";
                break;
            case "BLOODSUGAR" :
                textViewTitle.setText("Biểu đồ đường huyết");
                content = "Đường Huyết";
                break;
            case "BMI" :
                textViewTitle.setText("Biểu đồ BMI");
                content = "BMI";
                break;
            case "TEMPERATURE" :
                textViewTitle.setText("Biểu đồ nhiệt độ");
                content = "Nhiệt Độ";
                break;
            case "HEARTBEAT" :
                textViewTitle.setText("Biểu đồ nhịp tim");
                content = "Nhịp Tim";
                break;
            case "HEIGHT" :
                textViewTitle.setText("Biểu đồ chiều cao");
                content = "Chiều Cao";
                break;
            case "SPO2" :
                textViewTitle.setText("Biểu đồ SPO2");
                content = "SPO02";
                break;
            case "WEIGHT" :
                textViewTitle.setText("Biểu đồ cân nặng");
                content = "Cân Nặng";
                break;
        }

        setData();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                replaceFragment();
//            }
//        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        AddHealthQuotientFragment fragment = new AddHealthQuotientFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack("AddHealthQuotientFragment");
        fragmentTransaction.commit();
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListHealthQuotient);
        chart = view.findViewById(R.id.lineChart);
        button = view.findViewById(R.id.buttonPlus);
        textViewTitle = view.findViewById(R.id.textViewTitleChart);
    }


    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<Health>> callback = data.getHealthQuotient(new Session(getContext()).getID(),title);
        callback.enqueue(new Callback<List<Health>>() {
            @Override
            public void onResponse(Call<List<Health>> call, Response<List<Health>> response) {
                list = response.body();
                Collections.reverse(list);
                adapter = new HealthAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {

                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                createChart();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<Health>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void createChart(){
        ArrayList<Entry> list_chart = new ArrayList<>();
        ArrayList<Entry> list_chart1 = new ArrayList<>();
        List<Health> healthList = new ArrayList<>();
        healthList.addAll(list);
        if (healthList.size() > 0){
            if (healthList.size() > 10){
                healthList = healthList.subList(0 , 10);
            }
            Collections.reverse(healthList);
            LineDataSet lineDataSet = null;
            LineDataSet lineDataSet1 = null;
            LineData lineData = null;
            if (title.compareTo("BLOODPRESSURE") != 0){
                for (int i = 0 ; i < healthList.size() ; i++) {
                    list_chart.add(new Entry(i,Float.parseFloat(healthList.get(i).getValue())));
                }
                lineDataSet = new LineDataSet(list_chart,title);
                lineDataSet.setValueTextSize(0);
                lineDataSet.setLabel(content);
                lineDataSet.setLineWidth(2);
                lineDataSet.setColor(R.color.theme_main);
                lineDataSet.setCircleColor(R.color.theme_main);
                ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();
                lineDataSets.add(lineDataSet);
                lineData = new LineData(lineDataSets);
            }
            else{
                for (int i = 0 ; i < healthList.size() ; i++){
                    float value = Float.parseFloat(healthList.get(i).getValue().split("/")[0]);
                    float value1 = Float.parseFloat(healthList.get(i).getValue().split("/")[1]);
                    list_chart.add(new Entry(i,value));
                    list_chart1.add(new Entry(i,value1));
                }
                lineDataSet = new LineDataSet(list_chart,"Tâm Thu");
                lineDataSet.setColor(R.color.theme_main);
                lineDataSet1 = new LineDataSet(list_chart1, "Tâm Truong");
                lineDataSet1.setColor(Color.RED);
                ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();
                lineDataSets.add(lineDataSet);
                lineDataSets.add(lineDataSet1);
                lineData = new LineData(lineDataSets);
            }
            chart.setData(lineData);
            chart.getXAxis().setEnabled(false);
            chart.getAxisRight().setEnabled(false);
            chart.invalidate();
        }

        chart.setNoDataText("Không có dữ liệu");
        chart.setNoDataTextColor(Color.BLUE);
    }

    class MyValueFormatter extends ValueFormatter implements IValueFormatter{
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            int values = (int) value;
            return String.valueOf(values);
        }
    }
}