package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.HealthRecordDetailActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddHealthQuotientFragment extends Fragment {

    TextView textViewTitle,textView,textView1;
    EditText editTextDate,editTextTime,editTextValue,editTextValue1;
    TextInputLayout textInputLayout1;
    Button button;
    String title = "";
    String time = "";
    String value = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_health_quotient, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        HealthRecordDetailActivity.CURRENT_PAGE = 2;
    }

    private void event(View view) {
        Bundle bundle = this.getArguments();
        title = bundle.getString("title");
        time = bundle.getString("time");
        switch (title){
            case "BLOODPRESSURE" :
                textViewTitle.setText("Nhập chỉ số huyết áp");
                textView.setText("");
                textView.setText("Tâm thu (Chỉ số trên)");
                textInputLayout1.setVisibility(View.VISIBLE);
                textView1.setVisibility(View.VISIBLE);
                textView1.setText("Tâm trương (Chỉ số dưới)");
                break;
            case "BLOODSUGAR" :
                textViewTitle.setText("Nhập chỉ số đường huyết");
                textView.setText("Đường huyết (mg/dl)");
                break;
            case "BMI" :
                textViewTitle.setText("Nhập chỉ số BMI");
                textView.setText("Chiều cao (cm)");
                textInputLayout1.setVisibility(View.VISIBLE);
                textView1.setVisibility(View.VISIBLE);
                textView1.setText("Cân nặng (kg)");
                break;
            case "TEMPERATURE" :
                textViewTitle.setText("Nhập chỉ số nhiệt độ");
                textView.setText("Nhiệt độ (" + "\u2103" + ")");
                break;
            case "HEARTBEAT" :
                textViewTitle.setText("Nhập chỉ số nhịp tim");
                textView.setText("Nhịp tim (nhịp/phút)");
                break;
            case "HEIGHT" :
                textViewTitle.setText("Nhập chỉ số chiều cao");
                textView.setText("Chiều cao (cm)");
                break;
            case "SPO2" :
                textViewTitle.setText("Nhập chỉ số SPO2");
                textView.setText("Chỉ số SPO2 (%)");
                break;
            case "WEIGHT" :
                textViewTitle.setText("Nhập chỉ số cân nặng");
                textView.setText("Cân nặng  (kq)");
                break;
        }

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText((day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0" + month : month) + "/" + year);
        LocalDateTime dateTime = LocalDateTime.now();
        int hour = dateTime.getHour();;
        int minute = dateTime.getMinute();
        int second = dateTime.getSecond();
        editTextTime.setText(time);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    addHQ();
                }
            }
        });
    }

    private void assign(View view) {
        textViewTitle  = view.findViewById(R.id.textViewTitleAddHQ);
        textView       = view.findViewById(R.id.textViewAddHQ);
        textView1      = view.findViewById(R.id.textViewAddHQ1);
        editTextDate   = view.findViewById(R.id.editTextDateAddHQ);
        editTextTime   = view.findViewById(R.id.editTextTimeAddHQ);
        editTextValue  = view.findViewById(R.id.editTextValueAddHQ);
        button         = view.findViewById(R.id.buttonSendResult);
        editTextValue1 = view.findViewById(R.id.editTextValueAddHQ1);
        textInputLayout1 = view.findViewById(R.id.textInputLayoutValueAddHQ1);
    }

    public boolean checkValidation(){
        if (editTextValue.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập đầy đủ thông tin");
            editTextValue.requestFocus();
            return false;
        }

        if (title.compareTo("BLOODPRESSURE") == 0 && editTextValue1.getText().toString().isEmpty()){
            showToastWarning("Vùi lòng nhập đầy đủ thông tin");
            editTextValue1.requestFocus();
            return false;
        }

        if (title.compareTo("BMI") == 0 && editTextValue1.getText().toString().isEmpty()){
            showToastWarning("Vùi lòng nhập đầy đủ thông tin");
            editTextValue1.requestFocus();
            return false;
        }

        return true;
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,getActivity().findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void addHQ(){
        String time = editTextTime.getText() + " " + editTextDate.getText();
        if (title.compareTo("BLOODPRESSURE") == 0){
            value = editTextValue.getText() + "/" + editTextValue1.getText();
        }
        else{
            if (title.compareTo("BMI") == 0){
                float height = Float.parseFloat(editTextValue.getText().toString()) / 100;
                float weight = Float.parseFloat(editTextValue1.getText().toString());
                float temp = weight / (height * height);
                temp = Math.round((temp * 100.0) / 100.0);
                value = String.valueOf(temp);
            }
            else{
                value = editTextValue.getText().toString();
            }
        }

        Health health = new Health("",time,value,title);
        String object = new Gson().toJson(health);
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.addHealthQuotient(new Session(getContext()).getID(),object);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismissDialog();
                getFragmentManager().popBackStack();
                switch (title){
                    case "BLOODPRESSURE" :
                        HealthRecordDetailActivity.bloodpressure = value;
                        break;
                    case "BLOODSUGAR" :
                        HealthRecordDetailActivity.bloodsugar = value;
                        break;
                    case "BMI" :
                        HealthRecordDetailActivity.bmi = value;
                        break;
                    case "TEMPERATURE" :
                        HealthRecordDetailActivity.tem = value;
                        break;
                    case "HEARTBEAT" :
                        HealthRecordDetailActivity.heartbeat = value;
                        break;
                    case "HEIGHT" :
                        HealthRecordDetailActivity.height = value;
                        break;
                    case "SPO2" :
                        HealthRecordDetailActivity.spo2 = value;
                        break;
                    case "WEIGHT" :
                        HealthRecordDetailActivity.weight = value;
                        break;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        HealthChartFragment fragment = new HealthChartFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}