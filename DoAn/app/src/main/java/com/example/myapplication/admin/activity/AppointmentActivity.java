package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.adapter.ViewPagerAdminAppointmentAdapter;
import com.google.android.material.tabs.TabLayout;

public class AppointmentActivity extends AppCompatActivity {

    public static boolean needUpdateConfirmed = true;
    public static boolean needUpdateCancelled = true;
    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        assign();
        event();
    }

    private void event() {
        ViewPagerAdminAppointmentAdapter adapter = new ViewPagerAdminAppointmentAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void assign() {
        tabLayout = findViewById(R.id.tabLayoutAppointmentAdmin);
        viewPager = findViewById(R.id.viewPagerAppointmentAdmin);
    }


}