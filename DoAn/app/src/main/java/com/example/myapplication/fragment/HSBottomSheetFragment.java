package com.example.myapplication.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.adapter.HealthServiceAdapter;
import com.example.myapplication.model.HealthService;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.my_interface.ViewIOnClickItemListener;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HSBottomSheetFragment extends BottomSheetDialogFragment {
    List<HealthService> list;
    IOnClickItemListener listener;
    HealthServiceAdapter adapter = new HealthServiceAdapter();
    ViewIOnClickItemListener onClickListener;
    String idHealthFacility;

    public HSBottomSheetFragment(List<HealthService> list, IOnClickItemListener listener, ViewIOnClickItemListener onClickListener, String idHealthFacility) {
        this.list = list;
        this.listener = listener;
        this.onClickListener = onClickListener;
        this.idHealthFacility = idHealthFacility;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        getData();
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(),R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_health_service,null,false);
        bottomSheetDialog.setContentView(view);
        RecyclerView recyclerView = bottomSheetDialog.findViewById(R.id.recyclerViewListHS);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        adapter = new HealthServiceAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        adapter.notifyDataSetChanged();

        Button button = bottomSheetDialog.findViewById(R.id.buttonContinueBSHS);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                onClickListener.onViewClick(adapter.getSelectedItem());
            }
        });

        setupFullHeight(bottomSheetDialog);
        return  bottomSheetDialog;
    }

    public void getData(){
        DataClient data = APIUltils.getData();
        Call<List<HealthService>> callback = data.getListHealthService(idHealthFacility);
        callback.enqueue(new Callback<List<HealthService>>() {
            @Override
            public void onResponse(Call<List<HealthService>> call, Response<List<HealthService>> response) {
                List<HealthService> list1 = response.body();
                list.addAll(list1);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HealthService>> call, Throwable t) {

            }
        });
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
