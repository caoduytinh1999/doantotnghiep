package com.example.myapplication.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class AUAdapter extends RecyclerView.Adapter<AUAdapter.ViewHolder>{
   List<ObjectAU> list;
   IOnClickItemListener listener;

    public AUAdapter(List<ObjectAU> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    public AUAdapter(){}

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder((LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bottom_sheet_a_u,parent,false)));
    }

    @Override
    public void onBindViewHolder(@NonNull AUAdapter.ViewHolder holder, int position) {
        ObjectAU item = list.get(position);
        holder.textView.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textViewDepth);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
