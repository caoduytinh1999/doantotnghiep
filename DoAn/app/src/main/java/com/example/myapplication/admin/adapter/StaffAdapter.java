package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Staff;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.ViewHolder>{
    List<Staff> list;
    IOnClickItemListener listener;

    public StaffAdapter(List<Staff> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_staff,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        Staff staff = list.get(position);
        holder.textViewName.setText(staff.getName());
        holder.textViewBirthday.setText(staff.getBirthday());
        holder.textViewAddress.setText(staff.getAddress());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName,textViewBirthday,textViewAddress;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewStaffName);
            textViewBirthday = itemView.findViewById(R.id.textViewStaffBirthday);
            textViewAddress = itemView.findViewById(R.id.textViewStaffAddress);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
