package com.example.myapplication.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.adapter.GeneralAdapter;
import com.example.myapplication.adapter.ReactInjectAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ReactInject;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.model.health_record.HealthRecord;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GeneralFilterFragment extends Fragment {
    EditText editText;
    RecyclerView recyclerView;
    List<Health> healthList = new ArrayList<>();
    List<HealthRecord> healthRecordList = new ArrayList<>();
    HashMap<String,HealthRecord> hashMap = new HashMap<>();
    GeneralAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_general_filter, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        String date = (day < 10 ? "0" + day : day) + "/" + (month < 10 ? "0"  + month : month) + "/" + year;
        editText.setText(date);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new GeneralAdapter(healthRecordList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        });
        recyclerView.setAdapter(adapter);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    public void showDatePicker(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
                editText.setText(date);
                setData();
            }
        }, year , month, day);
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void assign(View view) {
        editText = view.findViewById(R.id.editTextDate);
        recyclerView = view.findViewById(R.id.recyclerView);
    }

    private void setData(){
        healthRecordList.clear();
        healthList.clear();
        hashMap.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<Health>> callback = data.getListHealthRecordFilter(new Session(getContext()).getID(),editText.getText().toString());
        callback.enqueue(new Callback<List<Health>>() {
            @Override
            public void onResponse(Call<List<Health>> call, Response<List<Health>> response) {
                dialog.dismissDialog();
                healthList.addAll(response.body());
                for (Health item : healthList){
                    HealthRecord record = new HealthRecord();
                    if (!hashMap.containsKey(item.getId())){
                        hashMap.put(item.getId(), record);
                    }
                    else{
                        record = hashMap.get(item.getId());
                    }
                    switch (item.getType()){
                        case "HuyetAp":
                            record.setBloodPressureValue(item.getValue());
                            record.setBloodPressureTime(item.getTime());
                            break;
                        case "NhipTim":
                            record.setHealthBeatValue(item.getValue());
                            record.setHealthBeatTime(item.getTime());
                            break;
                        case "NhietDo":
                            record.setTemperatureValue(item.getValue());
                            record.setTemperatureTime(item.getTime());
                            break;
                        case "CanNang":
                            record.setWeightValue(item.getValue());
                            record.setWeightTime(item.getTime());
                            break;
                        case "ChieuCao" :
                            record.setHeightValue(item.getValue());
                            record.setHeightTime(item.getTime());
                            break;
                        case "BMI":
                            record.setBmiValue(item.getValue());
                            record.setBmiTime(item.getTime());
                            break;
                        case "DuongHuyet":
                            record.setBloodSugarValue(item.getValue());
                            record.setBloodSugarTime(item.getTime());
                            break;
                        case "SPO2":
                            record.setSpo2Value(item.getValue());
                            record.setSpo2Time(item.getTime());
                            break;
                    }
                }
                for (String item : hashMap.keySet()){
                    healthRecordList.add(hashMap.get(item));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Health>> call, Throwable t) {

            }
        });
    }
}