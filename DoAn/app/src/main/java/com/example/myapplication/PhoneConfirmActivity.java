package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import org.json.JSONException;

import java.util.Calendar;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneConfirmActivity extends AppCompatActivity {

    EditText editText1,editText2,editText3,editText4,editText5,editText6;
    Button button;
    String phone = "";
    String password = "";
    String name = "";
    Patients patients = null;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_confirm);
        assign();
        event();
    }

    private void event() {
        Intent intent = getIntent();
        phone = intent.getStringExtra("phone");
        password = intent.getStringExtra("password");
        name = intent.getStringExtra("name");
        patients = intent.getParcelableExtra("object");
        textView.setText("Vui lòng nhập mã xác thực gửi đến số " + phone);
        String random = random();
        if (ContextCompat.checkSelfPermission(PhoneConfirmActivity.this, Manifest.permission.SEND_SMS)  == PackageManager.PERMISSION_GRANTED){
            sendSMS(phone,"Mã Xác Nhận : " + random);
        }else {
            ActivityCompat.requestPermissions(PhoneConfirmActivity.this,new String[]{Manifest.permission.SEND_SMS},100);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirm(random);
            }
        });
    }

    private void assign() {
        editText1 = findViewById(R.id.editTextNumber1);
        editText2 = findViewById(R.id.editTextNumber2);
        editText3 = findViewById(R.id.editTextNumber3);
        editText4 = findViewById(R.id.editTextNumber4);
        editText5 = findViewById(R.id.editTextNumber5);
        editText6 = findViewById(R.id.editTextNumber6);
        textView = findViewById(R.id.textViewMes);
        button    = findViewById(R.id.buttonConfirmPhone);
    }

    public void confirm(String random){
        String text1 = editText1.getText().toString().trim();
        String text2 = editText2.getText().toString().trim();
        String text3 = editText3.getText().toString().trim();
        String text4 = editText4.getText().toString().trim();
        String text5 = editText5.getText().toString().trim();
        String text6 = editText6.getText().toString().trim();

        if (text1.isEmpty() || text2.isEmpty() || text3.isEmpty() || text4.isEmpty() || text5.isEmpty() || text6.isEmpty()){
            Toast.makeText(this, "Vui lòng nhập đủ mã xác nhận", Toast.LENGTH_SHORT).show();
        }
        else{
            String code = text1 + text2 + text3 + text4 + text5 + text6;
            if (code.compareTo(random) != 0){
                Toast.makeText(this, "Mã Xác Nhận Không Chính Xác.Vui lòng kiểm tra lại", Toast.LENGTH_SHORT).show();
            }
            else{
                signup();
            }
        }
    }

    public void sendSMS(String phone,String mes){
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(phone,null,mes,null,null);
    }

    public String random(){
        Random random = new Random();
        int code = random.nextInt(899999) + 100000;
       return String.valueOf(code);
    }

    public void signup(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR) - 1;
        String birthday = day + "/" + month + "/" + year;
        String object = new Gson().toJson(patients);
        DataClient data =  APIUltils.getData();
        Call<String> callback = data.register(phone,object,password);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().compareTo("success") ==0){
                    createPopup();
                }
                else{
                    Toast.makeText(PhoneConfirmActivity.this, "Đăng Kí Không Thành Công", Toast.LENGTH_SHORT).show();
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(PhoneConfirmActivity.this, "Lỗi Hệ Thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void createPopup(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_regiser_success);

        Button buttonYes = dialog.findViewById(R.id.buttonClose);

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            }
        });

        dialog.show();
    }
}