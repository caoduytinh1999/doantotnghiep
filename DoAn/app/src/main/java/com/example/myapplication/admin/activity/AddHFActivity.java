package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddHFActivity extends AppCompatActivity {
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    EditText editTextName,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextPhone,editTextEmail,editTextWebsite,editTextFanpage;
    Button buttonYes,buttonReturn;
    List<ObjectAU> list_au = new ArrayList<>();
    AUAdapter adapter = new AUAdapter();
    String code_province = "";
    String code_district = "";
    String code_ward = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hfactivity);
        assign();
        event();
    }

    private void assign(){
        editTextName = findViewById(R.id.editTextNameRI);
        editTextProvince = findViewById(R.id.editTextCityNameRI);
        editTextDistrict = findViewById(R.id.editTextDistricNameRI);
        editTextWard = findViewById(R.id.editTextWardNameRI);
        editTextPlace = findViewById(R.id.editTextPlaceRI);
        editTextPhone = findViewById(R.id.editTextPhone);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextWebsite = findViewById(R.id.editTextWebsite);
        editTextFanpage = findViewById(R.id.editTextFanpage);
        buttonYes = findViewById(R.id.buttonConfirm);
        buttonReturn = findViewById(R.id.buttonReturn);
    }

    private void event(){
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    addHF();
                }
            }
        });

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_province.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,code_province,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_district.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else{
                    createBottomSheet(CODE_NAME_WARD,code_district,"Chọn Phường/Xã");
                }
            }
        });
    }

    private void addHF(){
        String name = editTextName.getText().toString();
        String address = editTextPlace.getText() + "," + editTextWard.getText() + "," + editTextDistrict.getText() + "," + editTextProvince.getText();
        String phone = editTextPhone.getText().toString();
        String email = editTextEmail.getText().toString();
        String website = editTextWebsite.getText().toString();
        String fanpage = editTextFanpage.getText().toString();
        HealthFacility healthFacility = new HealthFacility("",name,address,phone,email,website,fanpage,"0");
        String object = new Gson().toJson(healthFacility);
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.addHealthFacility(object);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().compareTo("success") ==0){
                    showToastSuccess("Thêm cơ sơ y tế thành công");
                    finish();
                }
                else{
                    showToastWarning("Thêm co sở y tế không thành công");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("AAA",t.getMessage());
                Toast.makeText(AddHFActivity.this, "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean checkValidation(){
        if (editTextName.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập tên cơ sở y tế");
            return false;
        }

        if (editTextProvince.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
            return false;
        }

        if (editTextDistrict.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn Quận/Huyện");
            return false;
        }

        if (editTextWard.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn Phường/Xã");
            return false;
        }

        if (editTextPlace.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập địa chỉ");
            return false;
        }

        if (editTextEmail.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập email");
            return false;
        }

        return true;
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public void createBottomSheet(String codename,String code,String title){
        list_au.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(list_au, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (code_province.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    code_province = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (code_district.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    code_district = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    code_ward = objectAU.getCode();
                }
            }
        },title,codename,code);
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }
}