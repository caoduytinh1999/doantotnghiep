package com.example.myapplication.fragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionActivity;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.HealthService;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.calendar_ui.CustomTimePickerDialog;
import com.example.myapplication.model.calendar_ui.RedDecorator;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.my_interface.ViewIOnClickItemListener;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class InfoBA1Fragment extends Fragment {
    private final static int TIME_PICKER_INTERVAL = 15;
    private final static int TIME_START = 7;
    private final static int TIME_END = 18;
    TextView textViewName;
    Button buttonContinue,buttonReturn;
    EditText editTextName,editTextMajor,editTextService, editTextDateCheck,editTextTimeCheck,editTextSymtom;
    List<HealthService> lists = new ArrayList<>();
    HealthFacility facility;
    float price = 0;
    int availableDay = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_b_a1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        setData();
        editTextService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getService();
            }
        });

        editTextDateCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCalendarDiaglog();
            }
        });

        editTextTimeCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTimeDialog();
            }
        });

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    replaceFragment();
                }
            }
        });
    }

    public void replaceFragment(){
        String idFacility = facility.getId();
        String address = facility.getAddress();
        String service = editTextService.getText().toString();
        String time = editTextTimeCheck.getText() + " " + editTextDateCheck.getText();
        String symptom = editTextSymtom.getText().toString();
        HealthCheck healthCheck = new HealthCheck("",idFacility,address,service,price,time,symptom,"0");
        Bundle bundle = new Bundle();
        bundle.putParcelable("health",healthCheck);
        bundle.putParcelable("facility",facility);
        InfoAB2Fragment fragment = new InfoAB2Fragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthFacilities,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public boolean checkValidation(){
        if (editTextTimeCheck.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn thời gian khám");
            return false;
        }
        return true;
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(getActivity());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,getActivity().findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void createTimeDialog(){
        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        int day = Integer.parseInt(editTextDateCheck.getText().toString().split("/")[0]);
        int hour = 0;
        int minute = 0;
        Log.d("AAA", "hihi " + day + " " + currentDay);
        if (day == currentDay){
            LocalDateTime dateTime = LocalDateTime.now();
            hour = dateTime.getHour();
            minute = dateTime.getMinute();
            if(minute % TIME_PICKER_INTERVAL != 0){
                minute = minute + TIME_PICKER_INTERVAL - (minute % TIME_PICKER_INTERVAL) + TIME_PICKER_INTERVAL;
            }
            else{
                minute += TIME_PICKER_INTERVAL;
            }
            if (minute >=60) {
                minute = minute % 60;
                hour++;
            }
        }
        else{
            hour = TIME_START;
        }

        CustomTimePickerDialog timer = new CustomTimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                editTextTimeCheck.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute));
            }
        },hour,minute,true);
        timer.setMin(hour,minute);
        timer.setMax(TIME_END,0);
        timer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        timer.show();
    }

    public void createCalendarDiaglog(){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_show_calendar);

        CalendarView calendar = dialog.findViewById(R.id.calendarAB1);
        Button buttonYes,buttonNo;
        buttonYes = dialog.findViewById(R.id.buttonContinue);
        buttonNo = dialog.findViewById(R.id.buttonReturn);
        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        LocalDateTime dateTime = LocalDateTime.now();
        int hour = dateTime.getHour();
        int minute = dateTime.getMinute();
        if (hour >= 17 && minute >= 30){
            min.add(Calendar.DAY_OF_MONTH,1);
            availableDay = min.get(Calendar.DAY_OF_MONTH);
        }
        max.add(Calendar.DAY_OF_MONTH,7);
        calendar.setMinDate(min.getTimeInMillis());
        calendar.setMaxDate(max.getTimeInMillis());

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                editTextDateCheck.setText((dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" +(month + 1 < 10 ? "0" + month + 1 : month + 1) + "/" + year);
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void setData(){
        Bundle bundle = this.getArguments();
        facility = bundle.getParcelable("object");
        textViewName.setText("Cơ sở y tế : " + facility.getName());
        editTextName.setText(new Session(getContext()).getName());
        LocalDateTime dateTime = LocalDateTime.now();
        int hour = dateTime.getHour();
        int minute = dateTime.getMinute();
        Calendar calendar = Calendar.getInstance();
        if (hour > 17 || (hour == 17 && minute > 30)){
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        String date = (day < 10 ? "0" + day : day) + "/" + (month + 1 < 10 ? "0" + (month + 1)  : (month + 1)) + "/" + year;
        editTextDateCheck.setText(date);
    }

    public void getService(){
        HSBottomSheetFragment fragment = new HSBottomSheetFragment(lists, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        }, new ViewIOnClickItemListener() {
            @Override
            public void onViewClick(List<HealthService> list) {
                String services = "";
                for (int i = 0 ; i < list.size() -1 ; i++){
                    services = services + list.get(i).getName() + ", ";
                    price += list.get(i).getPrice();
                }
                services = services + list.get(list.size() - 1).getName();
                price += list.get(list.size() -1).getPrice();
                editTextService.setText(services);
            }
        },facility.getId());
        fragment.show(getActivity().getSupportFragmentManager(),fragment.getTag());
    }

    private void assign(View view) {
        textViewName   = view.findViewById(R.id.textViewHospitalNameInfoAB1);
        buttonContinue = view.findViewById(R.id.buttonContinueInfoAB1);
        buttonReturn   = view.findViewById(R.id.buttonReturnInfoAB1);
        editTextName   = view.findViewById(R.id.editTextNameInfoAB1);
        editTextMajor  = view.findViewById(R.id.editTextMajorAB1);
        editTextService= view.findViewById(R.id.editTextServiceAB1);
        editTextDateCheck = view.findViewById(R.id.editTextDateCheckAB1);
        editTextTimeCheck = view.findViewById(R.id.editTextTimeCheckAB1);
        editTextSymtom = view.findViewById(R.id.editTextSymptomAB1);
    }
}