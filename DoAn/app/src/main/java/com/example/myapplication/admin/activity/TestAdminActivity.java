package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.ViewPagerAdminRegisterInjectAdapter;
import com.example.myapplication.admin.adapter.ViewPagerAdminTestAdapter;
import com.google.android.material.tabs.TabLayout;

public class TestAdminActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        assign();
        event();
    }

    private void assign() {
        tabLayout = findViewById(R.id.tabLayoutTestAdmin);
        viewPager = findViewById(R.id.viewPagerTestAdmin);
    }

    private void event(){
        ViewPagerAdminTestAdapter adapter = new ViewPagerAdminTestAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}