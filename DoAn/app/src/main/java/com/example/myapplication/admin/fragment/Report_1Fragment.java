package com.example.myapplication.admin.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.F0Info;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Report_1Fragment extends Fragment {
    private static int ROWNUMBER = 6;
    EditText editTextFromDate,editTextToDate;
    TableLayout tableLayout;
    Button button;
    List<F0Info> list = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        editTextFromDate.setText(year + "-" +  (month < 10 ? "0" + month : month) + "-" + (day - 1 < 10 ? "0" + (day - 1) : day - 1));
        editTextToDate.setText(year + "-" +  (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day));

        getDate();

        editTextFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker("0");
            }
        });

        editTextToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker("1");
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();
            }
        });
    }

    private void assign(View view) {
        editTextFromDate = view.findViewById(R.id.editTextFromDate);
        editTextToDate = view.findViewById(R.id.editTextToDate);
        tableLayout = view.findViewById(R.id.tableLayout1);
        button = view.findViewById(R.id.buttonFillter);
    }

    public void showDatePicker(String id){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = year + "-" +  (month < 10 ? "0" + month : month) + "-" + (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth);
                if (id.compareTo("0") == 0){
                    editTextFromDate.setText(date);
                }
                else{
                    editTextToDate.setText(date);
                }
            }
        }, year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void getDate(){
        tableLayout.removeViews(1,list.size());
        list.clear();
        String fromDate = editTextFromDate.getText().toString();
        String toDate = editTextToDate.getText().toString();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<F0Info>> callback = data.getListF0(fromDate,toDate);
        callback.enqueue(new Callback<List<F0Info>>() {
            @Override
            public void onResponse(Call<List<F0Info>> call, Response<List<F0Info>> response) {
                list.addAll(response.body());
                setData(list);
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<F0Info>> call, Throwable t) {

            }
        });
    }

    public void setData(List<F0Info> list){
        int rowNumber = list.size();
        for (int i = 0 ; i < rowNumber ; i++){
            TableRow tableRow = new TableRow(getContext());

            TextView textViewIndex = new TextView(getContext());
            textViewIndex.setText(String.valueOf(i + 1));
            textViewIndex.setTextColor(Color.BLACK);
            textViewIndex.setGravity(Gravity.CENTER);
            textViewIndex.setPadding(10,10,10,10);
            textViewIndex.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            tableRow.addView(textViewIndex);


            TextView textViewName = new TextView(getContext());
            textViewName.setText(list.get(i).getPatients().getName());
            textViewName.setTextColor(Color.BLACK);
            textViewName.setPadding(10,10,10,10);
            textViewName.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            tableRow.addView(textViewName);

            TextView textViewDate = new TextView(getContext());
            textViewDate.setTextColor(Color.BLACK);
            textViewDate.setGravity(Gravity.CENTER);
            textViewDate.setPadding(10,10,10,10);
            textViewDate.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            textViewDate.setText(list.get(i).getPatients().getBirthday());
            tableRow.addView(textViewDate);

            TextView textViewSex = new TextView(getContext());
            textViewSex.setTextColor(Color.BLACK);
            textViewSex.setGravity(Gravity.CENTER);
            textViewSex.setPadding(10,10,10,10);
            textViewSex.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            String sex = list.get(i).getPatients().getSex();
            if (sex.compareTo("1") == 0) sex = "Nam";
            else sex = "Nữ";
            textViewSex.setText(sex);
            tableRow.addView(textViewSex);

            TextView textViewAddress = new TextView(getContext());
            textViewAddress.setTextColor(Color.BLACK);
            textViewAddress.setGravity(Gravity.CENTER);
            textViewAddress.setPadding(10,10,10,10);
            textViewAddress.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            textViewAddress.setText(list.get(i).getAddress());
            tableRow.addView(textViewAddress);

            TextView textViewDateF0 = new TextView(getContext());
            textViewDateF0.setTextColor(Color.BLACK);
            textViewDateF0.setGravity(Gravity.CENTER);
            textViewDateF0.setPadding(10,10,10,10);
            textViewDateF0.setBackground(ContextCompat.getDrawable(getContext(),R.drawable.custom_body_ri2));
            textViewDateF0.setText(list.get(i).getDate());
            tableRow.addView(textViewDateF0);

            tableLayout.addView(tableRow);
        }
    }


}