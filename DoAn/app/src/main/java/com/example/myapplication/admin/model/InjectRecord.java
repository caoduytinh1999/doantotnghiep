package com.example.myapplication.admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InjectRecord extends OnClick implements Parcelable {
    @SerializedName("idRecord")
    @Expose
    private String idRecord;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("patients")
    @Expose
    private Patients patients;
    @SerializedName("idStaff")
    @Expose
    private String idStaff;
    @SerializedName("vaccine")
    @Expose
    private String vaccine;
    @SerializedName("appointDate")
    @Expose
    private String appointDate;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("address")
    @Expose
    private Site address;
    @SerializedName("numberOfInject")
    @Expose
    private String numberOfInject;
    @SerializedName("status")
    @Expose
    private String status;

    public InjectRecord(String idRecord, String id, Patients patients, String idStaff, String vaccine, String appointDate, String date, Site address, String numberOfInject, String status) {
        this.idRecord = idRecord;
        this.id = id;
        this.patients = patients;
        this.idStaff = idStaff;
        this.vaccine = vaccine;
        this.appointDate = appointDate;
        this.date = date;
        this.address = address;
        this.numberOfInject = numberOfInject;
        this.status = status;
    }

    protected InjectRecord(Parcel in) {
        idRecord = in.readString();
        id = in.readString();
        patients = in.readParcelable(Patients.class.getClassLoader());
        idStaff = in.readString();
        vaccine = in.readString();
        appointDate = in.readString();
        date = in.readString();
        numberOfInject = in.readString();
        status = in.readString();
    }

    public static final Creator<InjectRecord> CREATOR = new Creator<InjectRecord>() {
        @Override
        public InjectRecord createFromParcel(Parcel in) {
            return new InjectRecord(in);
        }

        @Override
        public InjectRecord[] newArray(int size) {
            return new InjectRecord[size];
        }
    };

    public String getAppointDate() {
        return appointDate;
    }

    public void setAppointDate(String appointDate) {
        this.appointDate = appointDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getVaccine() {
        return vaccine;
    }

    public void setVaccine(String vaccine) {
        this.vaccine = vaccine;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Site getAddress() {
        return address;
    }

    public void setAddress(Site address) {
        this.address = address;
    }

    public String getNumberOfInject() {
        return numberOfInject;
    }

    public void setNumberOfInject(String numberOfInject) {
        this.numberOfInject = numberOfInject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idRecord);
        dest.writeString(id);
        dest.writeParcelable(patients, flags);
        dest.writeString(idStaff);
        dest.writeString(vaccine);
        dest.writeString(appointDate);
        dest.writeString(date);
        dest.writeString(numberOfInject);
        dest.writeString(status);
    }
}
