package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.vaccine_record.VaccineRecord;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CertificationPreventCovidActivity extends AppCompatActivity {

    TextView textViewNumberOfVaccine,textViewName,textViewBirthday,textViewID;
    ImageView imageViewQRCODE;
    String idPatients;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certification_prevent_covid);
        assign();
        event();
    }

    private void event() {
        createQRCODE();
        setData();
    }

    private void assign() {
        textViewNumberOfVaccine     = findViewById(R.id.textViewNumberOfVaccine);
        textViewName                = findViewById(R.id.textViewNameCPC);
        textViewBirthday            = findViewById(R.id.textViewBirthdayCPC);
        textViewID                  = findViewById(R.id.textViewIDCPC);
        imageViewQRCODE             = findViewById(R.id.imageViewQRCodeCPC);
        linearLayout                = findViewById(R.id.layoutHideCPC);
    }

    public void createQRCODE(){
        Random r = new Random();
        int i1 = r.nextInt(999999 - 1) + 1;
        Session session = new Session(this);
        String id = session.getID();
        String token = session.getToken();
        JSONObject object = new JSONObject();
        try {
            object.put("id",id);
            object.put("token",token);
            object.put("random", i1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String content = String.valueOf(object);
        MultiFormatWriter writer = new MultiFormatWriter();
        try {
            BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE,180,180);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(matrix);
            imageViewQRCODE.setImageBitmap(bitmap);
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<Patients> callback = data.getInfoAccount(new Session(getApplicationContext()).getID());
                callback.enqueue(new Callback<Patients>() {
                    @Override
                    public void onResponse(Call<Patients> call, Response<Patients> response) {
                        Patients patients = response.body();
                        textViewName.setText(patients.getName());
                        textViewBirthday.setText(patients.getBirthday());
                        textViewID.setText(patients.getIdcode());
                        linearLayout.setVisibility(View.GONE);
                        dialog.dismissDialog();
                    }

                    @Override
                    public void onFailure(Call<Patients> call, Throwable t) {

                    }
                });
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<VaccineRecord> callback = data.getInfoVaccination(new Session(getApplicationContext()).getID());
                callback.enqueue(new Callback<VaccineRecord>() {
                    @Override
                    public void onResponse(Call<VaccineRecord> call, Response<VaccineRecord> response) {
                        VaccineRecord record = response.body();
                        int size = record.getList().size();
                        if (size >= 2){
                            textViewNumberOfVaccine.setText("ĐÃ TIÊM 02 MŨI VẮC XIN");
                        }
                        if (size == 1){
                            textViewNumberOfVaccine.setText("ĐÃ TIÊM 01 MŨI VẮC XIN");
                        }
                        if (size ==0){
                            textViewNumberOfVaccine.setText("CHƯA TIÊM VẮC XIN");
                        }
                    }

                    @Override
                    public void onFailure(Call<VaccineRecord> call, Throwable t) {

                    }
                });
            }
        }).start();
    }
}