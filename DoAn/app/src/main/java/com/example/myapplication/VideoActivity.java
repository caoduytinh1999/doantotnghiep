package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.myapplication.model.Advise;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

public class VideoActivity extends AppCompatActivity {

    YouTubePlayerView youTubePlayerView;
    TextView textViewTitle,textViewContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        youTubePlayerView = findViewById(R.id.videoView);
        textViewTitle  = findViewById(R.id.textViewTitleVideo);
        textViewContent = findViewById(R.id.textViewContentVideo);

        Intent intent = getIntent(); 
        Advise advise = intent.getParcelableExtra("object");

        textViewTitle.setText(advise.getTitle());
        textViewContent.setText(advise.getContent());
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                youTubePlayer.loadVideo(advise.getUrl(),advise.getTimeStart());
            }

            @Override
            public void onCurrentSecond(@NotNull YouTubePlayer youTubePlayer, float second) {
                if (second > advise.getTimeEnd()){
                    youTubePlayer.seekTo(advise.getTimeStart());
                    youTubePlayer.pause();
                }
            }

        });


    }
}