package com.example.myapplication.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.CertificationPreventCovidActivity;
import com.example.myapplication.ChangePassWordActivity;
import com.example.myapplication.HealthCheckHistoryActivity;
import com.example.myapplication.HealthRecordActivity;
import com.example.myapplication.LoginActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionHistoryActivity;
import com.example.myapplication.RegisterTestActivity;
import com.example.myapplication.UpdateInfoAccountActivity;
import com.example.myapplication.model.Session;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Random;

public class AccountFragment extends Fragment {

    ImageView imageView;
    TextView textView;
    Button buttonInfoAccount,buttonCPCAccount,buttonFamilyAccount,buttonHealthRecordAccount,buttonHistorySyringeAccount,buttonHistoryHealthCheckAccount
            ,buttonInfoApp,buttonSupport,buttonChangePassWord,buttonLogout,buttonHistoryRT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapped(view);
        event();
    }

    @Override
    public void onResume() {
        super.onResume();
        textView.setText(new Session(getContext()). getName());
    }

    private void event() {

        textView.setText(new Session(getContext()).getName());
        createQRCode();

        buttonInfoAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), UpdateInfoAccountActivity.class));
            }
        });

        buttonCPCAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CertificationPreventCovidActivity.class));
            }
        });

        buttonHealthRecordAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), HealthRecordActivity.class));
            }
        });

        buttonHistoryHealthCheckAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), HealthCheckHistoryActivity.class));
            }
        });

        buttonHistorySyringeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), RegisterInjectionHistoryActivity.class));
            }
        });

        buttonHistoryRT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), RegisterTestActivity.class));
            }
        });

        buttonInfoApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        buttonSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        buttonChangePassWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ChangePassWordActivity.class));
            }
        });

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopupLogout();
            }
        });
    }

    private void mapped(View view) {
        buttonInfoAccount           = view.findViewById(R.id.buttonInfoAccount);
        buttonCPCAccount            = view.findViewById(R.id.buttonCPC);
        buttonHealthRecordAccount   = view.findViewById(R.id.buttonHealthRecordAccount);
        buttonHistorySyringeAccount = view.findViewById(R.id.buttonHistorySyringeAccount);
        buttonHistoryHealthCheckAccount = view.findViewById(R.id.buttonHistoryHealthCheck);
        buttonInfoApp               = view.findViewById(R.id.buttonInforAppAccount);
        buttonSupport               = view.findViewById(R.id.buttonSupportAccount);
        buttonChangePassWord        = view.findViewById(R.id.buttonChangePassWord);
        buttonLogout                = view.findViewById(R.id.buttonLogout);
        buttonHistoryRT             = view.findViewById(R.id.buttonHistoryRegisterTest);
        imageView                   = view.findViewById(R.id.imageViewQRCodeAccount);
        textView                    = view.findViewById(R.id.textViewNameAccount);
    }

    public void setData(){

    }

    public void createQRCode(){
        Random r = new Random();
        int i1 = r.nextInt(999999 - 1) + 1;
        String content = getString(R.string.declare_medical_3) + i1;
        MultiFormatWriter writer = new MultiFormatWriter();
        try {
            BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE,180,180);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(matrix);
            imageView.setImageBitmap(bitmap);
            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void createPopupLogout(){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_logout);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopupLogout);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopupLogout);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void logout(){
        Session session = new Session(getContext());
        session.setToken("");
        session.setID("");
        startActivity(new Intent(getContext(), LoginActivity.class));
    }
}
