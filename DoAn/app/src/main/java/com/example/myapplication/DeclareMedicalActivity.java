package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.adapter.ViewPagerDeclareMedicalAdapter;
import com.google.android.material.tabs.TabLayout;

public class DeclareMedicalActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declare_medical);
        mapped();
        event();
    }

    private void event() {
        ViewPagerDeclareMedicalAdapter adapter = new ViewPagerDeclareMedicalAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void mapped() {
        tabLayout       = findViewById(R.id.tabLayoutDeclareMedical);
        viewPager       = findViewById(R.id.viewPagerDeclareMedical);
    }
}