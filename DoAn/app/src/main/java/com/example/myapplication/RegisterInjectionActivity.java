package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.adapter.AUAdapter;
import com.example.myapplication.admin.fragment.VaccineBottomSheetFragment;
import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.fragment.RI2Fragment;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.administrative_units.District;
import com.example.myapplication.model.administrative_units.Province;
import com.example.myapplication.model.administrative_units.Ward;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterInjectionActivity extends AppCompatActivity {

    public static int PAGE_NUMBER_FRAGMENT = 1;
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    Button buttonReturn,buttonContinue;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    EditText editTextName,editTextBirthday,editTextSex,editTextIDCode,editTextHI,editTextJob,editTextCity,editTextDistric,editTextWard,editTextPlace,editTextDate;
    Calendar calendar = Calendar.getInstance();
    List<ObjectAU> list_au = new ArrayList<>();
    List<String> vaccineList = new ArrayList<>();
    AUAdapter adapter = new AUAdapter();
    String code_province = "";
    String code_district = "";
    String code_ward = "";
    String name = "";
    String sex = "Nam";
    String birthday = "";
    String phone = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_injection);
        mapped();
        event();
    }

    private void event() {
        setData();

        final String date = "";

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()){
                    PAGE_NUMBER_FRAGMENT = 2;
                    replaceContinueFragment();
                    Session session = new Session(getApplicationContext());
                    String address = editTextPlace.getText() + "," + editTextWard.getText() + "," + editTextDistric.getText() + "," + editTextCity;
                    RegisterSyringe syringe = new RegisterSyringe("",session.getID(),"",editTextJob.getText().toString(),editTextDate.getText().toString(),address,"0",null);
                }
            }
        });

        editTextCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_province.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
                }
                else{
                     createBottomSheet(CODE_NAME_DISTRICT,code_province,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code_district.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else{
                    createBottomSheet(CODE_NAME_WARD,code_district,"Chọn Phường/Xã");
                }
            }
        });

        editTextJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheetVaccine();
            }
        });

    }

    private void mapped() {
        buttonContinue      = findViewById(R.id.buttonContinueRI1);
        buttonReturn        = findViewById(R.id.buttonReturnRI1);
        editTextName        = findViewById(R.id.editTextNameRI);
        editTextBirthday    = findViewById(R.id.editTextBirthdatRI);
        editTextIDCode      = findViewById(R.id.editTextIDIR);
        editTextHI          = findViewById(R.id.editTextHIRI);
        editTextJob         = findViewById(R.id.editTextObjectRI);
        editTextDate        = findViewById(R.id.editTextRDRI);
        editTextCity        = findViewById(R.id.editTextCityNameRI);
        editTextDistric     = findViewById(R.id.editTextDistricNameRI);
        editTextWard        = findViewById(R.id.editTextWardNameRI);
        editTextPlace       = findViewById(R.id.editTextPlaceRI);
        radioButtonMale     = findViewById(R.id.radioSexMaleRI);
        radioButtonFemale     = findViewById(R.id.radioSexFeMaleRI);
        radioButtonMale     = findViewById(R.id.radioOtherRI);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        createVaccineList();
    }

    private void createVaccineList(){
        vaccineList.add("Người làm việc trong các cơ sở y tế, ngành y tế");
        vaccineList.add("Người tham gia phòng chống dịch");
        vaccineList.add("Lựu lượng Quân đội");
        vaccineList.add("Lựu lượng Công an");
        vaccineList.add("Nhân viên, cán bộ ngoại giao của Việt Nam và thân nhân được cử đi nước ngoại; người làm việc trong các cơ quan Ngoại giao, Lãnh sự, các tổ chức quốc tế");
        vaccineList.add("Hải quan, cán bộ làm công tác xuất nhập cảnh");
        vaccineList.add("Người cung cấp dịch vụ thiết yếu");
        vaccineList.add("Giáo viên, người làm việc, học sinh, sinh viên,lực lượng bác sĩ trẻ,các tổ chức hành nghề luật sư, công chứng, đấu giá...");
        vaccineList.add("Người mắc bệnh mạn tính;Người tên 65 tuổi");
        vaccineList.add("Người sinh sống tại các vùng có dịch");
        vaccineList.add("Người nghèo, các đối tượng chính sách xã hội");
        vaccineList.add("Người được cơ quan nhà nước có thẩm quyền cử đi công tác, học tập, lao động ở nước ngoài hoặc có nhu cầu xuất cảnh để công tác, học tập và lao động ở nước ngoài; chuyên gia nước ngoài làm việc tại Việt Nam");
        vaccineList.add("Các đối tượng là người lao động, thân nhân người lao động đang làm việc tại các doanh nghiệp");
        vaccineList.add("Các chức sắc, chức việc các tôn giáo");
        vaccineList.add("Người lao động tự do");
        vaccineList.add("Các đối tượng khác theo Quyết định của Bộ trưởng Bộ Y tế hoặc Chủ tịch Ủy ban nhân dân tỉnh, thành phố và đề xuất của các đơn vị viện trợ vắc xin cho Bộ Y tế");
    }
    public void createBottomSheetVaccine(){
        VaccineBottomSheetFragment fragment = new VaccineBottomSheetFragment(vaccineList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU object = (ObjectAU) onClick;
                editTextJob.setText(object.getName());
            }
        });

        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void setData(){
        DataClient data = APIUltils.getData();
        Call<Patients> callback = data.getInfoAccount(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<Patients>() {
            @Override
            public void onResponse(Call<Patients> call, Response<Patients> response) {
                Patients patients = response.body();
                editTextName.setText(patients.getName());
                editTextBirthday.setText(patients.getBirthday());
                editTextIDCode.setText(patients.getIdcode());
                editTextHI.setText(patients.getHealthInsurance());
                if (patients.getSex().compareTo("2") ==0){
                    radioButtonFemale.setChecked(true);
                    sex = "Nữ";
                }
                if (patients.getSex().compareTo("3") ==0){
                    radioButtonOther.setChecked(true);
                    sex = "Khác";
                }
                 name = patients.getName();
                 phone = patients.getPhone();
                 birthday = patients.getBirthday();

                 String address = patients.getAddress();
                 String province = address.split(",")[3];
                 String district = address.split(",")[2];
                 String ward = address.split(",")[1];
                 String place = address.split(",")[0];

                 editTextCity.setText(province);
                 editTextDistric.setText(district);
                 editTextWard.setText(ward);
                 editTextPlace.setText(place);

            }

            @Override
            public void onFailure(Call<Patients> call, Throwable t) {

            }
        });

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(day + "/" + month + "/" +year);
    }

    public void replaceContinueFragment(){
        Session session = new Session(getApplicationContext());
        String address = editTextPlace.getText() + "," + editTextWard.getText() + "," + editTextDistric.getText() + "," + editTextCity.getText().toString();
        RegisterSyringe syringe = new RegisterSyringe("",session.getID(),"00000",editTextJob.getText().toString(),editTextDate.getText().toString(),address,"0",null);
        Bundle bundle = new Bundle();
        bundle.putParcelable("syringe",syringe);
        bundle.putString("name",name);
        bundle.putString("birthday",birthday);
        bundle.putString("sex",sex);
        bundle.putString("phone",phone);
        RI2Fragment fragment = new RI2Fragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.linearLayoutRegInj1,fragment);
        fragmentTransaction.addToBackStack("RI2Fragment");
        fragmentTransaction.commit();
    }

    public void showDatePicker(){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + " - " + (month < 10 ? "0" + month : month) + " - " + year;
                editTextBirthday.setText(date);
            }
        }, year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public boolean checkValidation(){
        if (editTextJob.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn đối tượng tiêm");
            return false;
        }

        if (editTextCity.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn tỉnh/thành phố");
            return false;
        }

        if (editTextDistric.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn quận/huyện");
            return false;
        }

        if (editTextWard.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn phường/xã");
            return false;
        }

        if (editTextPlace.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập nơi ở");
            return false;
        }
        return true;
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void createBottomSheet(String codename,String code,String title){
        list_au.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(list_au, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextCity.setText(objectAU.getName());
                    if (code_province.compareTo(objectAU.getCode()) != 0){
                        editTextDistric.setText("");
                        editTextWard.setText("");
                    }
                    code_province = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistric.setText(objectAU.getName());
                    if (code_district.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    code_district = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    code_ward = objectAU.getCode();
                }
            }
        },title,codename,code);
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }
}