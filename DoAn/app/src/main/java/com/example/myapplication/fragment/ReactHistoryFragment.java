package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.ReactInjectAdapter;
import com.example.myapplication.adapter.VaccineInfoAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ReactInject;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReactHistoryFragment extends Fragment {

    RecyclerView recyclerView;
    List<ReactInject> list = new ArrayList<>();
    ReactInjectAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_react_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new ReactInjectAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                replaceFragment((ReactInject) onClick);
            }
        });
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
    }

    public void replaceFragment(ReactInject reactInject){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",reactInject);
        ReactInjectDetailFragment fragment = new ReactInjectDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.add(R.id.layoutReactInject,fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    private void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<ReactInject>> callback = data.getListReactInject(new Session(getContext()).getID());
        callback.enqueue(new Callback<List<ReactInject>>() {
            @Override
            public void onResponse(Call<List<ReactInject>> call, Response<List<ReactInject>> response) {
                list.addAll(response.body());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<ReactInject>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_LONG).show();
                dialog.dismissDialog();
            }
        });
    }
}