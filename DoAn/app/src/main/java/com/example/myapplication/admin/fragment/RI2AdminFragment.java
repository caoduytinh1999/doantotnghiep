package com.example.myapplication.admin.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.InputEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionHistoryActivity;
import com.example.myapplication.adapter.GeneralAdapter;
import com.example.myapplication.adapter.InjectContentAdapter;
import com.example.myapplication.adapter.RegisterSyringeAdapter;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.register_syringe.RegisterInjectContent;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.model.register_syringe.RegisterSyringeContent;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RI2AdminFragment extends Fragment {


    RecyclerView recyclerView;
    List<RegisterInjectContent> list = new ArrayList<>();
    RegisterInjectAdmin syringe;
    InjectContentAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_i2_admin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void assign(View view){
        recyclerView = view.findViewById(R.id.recyclerView);
    }

    private void event(View view){
        Bundle bundle = this.getArguments();
        syringe = bundle.getParcelable("syringe");

        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new InjectContentAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<RegisterInjectContent>> callback = data.getListRegisterInjectContent(syringe.getId());
        callback.enqueue(new Callback<List<RegisterInjectContent>>() {
            @Override
            public void onResponse(Call<List<RegisterInjectContent>> call, Response<List<RegisterInjectContent>> response) {
                list.addAll(response.body());
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<RegisterInjectContent>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }


}