package com.example.myapplication.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.AdminAccountFragment;
import com.example.myapplication.admin.fragment.AdminCalendarFragment;
import com.example.myapplication.admin.fragment.AdminHomeFragment;
import com.example.myapplication.admin.fragment.AdminNotifyFragment;


public class ViewPagerAdminAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdminAdapter(FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new AdminHomeFragment();
            case 1:
                return new AdminCalendarFragment();
            case 2 :
                return new AdminNotifyFragment();
            case 3 :
                return new AdminAccountFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

}
