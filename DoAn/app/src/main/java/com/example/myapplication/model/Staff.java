package com.example.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Staff extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("idHF")
    @Expose
    private String idHF;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("idcode")
    @Expose
    private String idcode;
    @SerializedName("email")
    @Expose
    private String email;

    public Staff(String id, String idHF, String name, String phone, String birthday, String sex, String address, String idcode, String email) {
        this.id = id;
        this.idHF = idHF;
        this.name = name;
        this.phone = phone;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
        this.idcode = idcode;
        this.email = email;
    }

    protected Staff(Parcel in) {
        id = in.readString();
        idHF = in.readString();
        name = in.readString();
        phone = in.readString();
        birthday = in.readString();
        sex = in.readString();
        address = in.readString();
        idcode = in.readString();
        email = in.readString();
    }

    public static final Creator<Staff> CREATOR = new Creator<Staff>() {
        @Override
        public Staff createFromParcel(Parcel in) {
            return new Staff(in);
        }

        @Override
        public Staff[] newArray(int size) {
            return new Staff[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdHF() {
        return idHF;
    }

    public void setIdHF(String idHF) {
        this.idHF = idHF;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdcode() {
        return idcode;
    }

    public void setIdcode(String idcode) {
        this.idcode = idcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(idHF);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(birthday);
        dest.writeString(sex);
        dest.writeString(address);
        dest.writeString(idcode);
        dest.writeString(email);
    }
}
