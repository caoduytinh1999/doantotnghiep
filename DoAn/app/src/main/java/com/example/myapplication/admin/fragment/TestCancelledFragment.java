package com.example.myapplication.admin.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.RegisterTestAdminAdapter;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestCancelledFragment extends Fragment {

    private static String CANCELLED_STATUS = "2";
    RecyclerView recyclerView;
    RegisterTestAdminAdapter adapter;
    List<RegisterTestAdmin> list = new ArrayList<>();
    RegisterTestAdmin test;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_cancelled, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RegisterTestAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                test = (RegisterTestAdmin) onClick;
                replaceFragment();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListTestCancelled);
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<RegisterTestAdmin>> callback = data.getListRegisterTestAdmin(new Session(getContext()).getID(),CANCELLED_STATUS);
        callback.enqueue(new Callback<List<RegisterTestAdmin>>() {
            @Override
            public void onResponse(Call<List<RegisterTestAdmin>> call, Response<List<RegisterTestAdmin>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<RegisterTestAdmin>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",test);
        RegisterTestDetailFragment fragment = new RegisterTestDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutAdminTest,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}