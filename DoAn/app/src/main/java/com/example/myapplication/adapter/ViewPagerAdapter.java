package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.myapplication.fragment.AccountFragment;
import com.example.myapplication.fragment.CalendarFragment;
import com.example.myapplication.fragment.HomeFragment;
import com.example.myapplication.fragment.NotificationFragment;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new HomeFragment();
            case 1:
                return new CalendarFragment();
            case 2 :
                return new NotificationFragment();
            case 3 :
                return new AccountFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
