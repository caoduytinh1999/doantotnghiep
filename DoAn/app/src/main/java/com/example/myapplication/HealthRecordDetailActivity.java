package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.myapplication.adapter.ViewPagerHealthRecordAdapter;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.Session;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.tabs.TabLayout;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthRecordDetailActivity extends AppCompatActivity {

    public static int CURRENT_PAGE = 0;
    public static String bloodpressure = "";
    public static String heartbeat = "";
    public static String tem = "";
    public static String height = "";
    public static String weight = "";
    public static String bmi = "";
    public static String bloodsugar = "";
    public static String spo2 = "";
    TabLayout tabLayout;
    ViewPager viewPager;
    TextView textViewName,textViewInfo;
    ViewPagerHealthRecordAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_record_detail);
        assign();
        event();
    }

    private void assign() {
        tabLayout       = findViewById(R.id.tabLayoutHRDetail);
        viewPager       = findViewById(R.id.viewPagerHRDetail);
        textViewName    = findViewById(R.id.textViewNameHR);
        textViewInfo    = findViewById(R.id.textViewInfoHR);
    }

    private void event(){
        adapter = new ViewPagerHealthRecordAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        setData();
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager()!= null && getFragmentManager().getBackStackEntryCount() > 0){
            if (CURRENT_PAGE == 1){
                getFragmentManager().popBackStack("GeneralFragment",0);
            }
            if (CURRENT_PAGE == 2){
                getFragmentManager().popBackStack("HealthChartFragment",0);
            }
        }else{
            super.onBackPressed();
        }
    }

    public void setData(){
        DataClient data = APIUltils.getData();
        Call<Patients> callback = data.getInfoAccount(new Session(this).getID());
        callback.enqueue(new Callback<Patients>() {
            @Override
            public void onResponse(Call<Patients> call, Response<Patients> response) {
                Patients patients = response.body();
                textViewName.setText(patients.getName());
                String sex = patients.getSex(); // sex == null
                if (sex == null) sex = "Khác";
                if (sex.compareTo("1") ==0) sex = "Nam";
                if (sex.compareTo("2") ==0) sex = "Nữ";
                if (sex.compareTo("3") ==0) sex = "Khác";
                int birth = Integer.parseInt(patients.getBirthday().split("/")[2]);
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int age = year - birth;
                textViewInfo.setText("Tôi - " + sex  + " - " + age + " tuổi");
            }

            @Override
            public void onFailure(Call<Patients> call, Throwable t) {

            }
        });
    }
}