package com.example.myapplication.retrofit;

import com.example.myapplication.admin.model.F0Info;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.admin.model.Site;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.admin.model.TestResult;
import com.example.myapplication.admin.model.TopProvince;
import com.example.myapplication.admin.model.VaccineInfo;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.HealthService;
import com.example.myapplication.model.ReactInject;
import com.example.myapplication.model.Staff;
import com.example.myapplication.model.TokenObject;
import com.example.myapplication.model.administrative_units.District;
import com.example.myapplication.model.administrative_units.Province;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.model.health_record.HealthRecord;
import com.example.myapplication.model.register_syringe.RegisterInjectContent;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.model.register_syringe.RegisterSyringeContent;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.model.vaccine_record.VaccineRecord;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DataClient {
    @FormUrlEncoded
    @POST("login.php")
    Call<TokenObject> login(@Field("phone") String phone, @Field("password") String pass);

    @FormUrlEncoded
    @POST("checkLogin.php")
    Call<String> checkLogin(@Field("token") String token);

    @FormUrlEncoded
    @POST("signup.php")
    Call<String> checkSignup(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("register.php")
    Call<String> register(@Field("phone")String phone,@Field("patients")String patients,@Field("password")String password);

    @FormUrlEncoded
    @POST("createAccount.php")
    Call<String> createAccount(@Field("phone") String phone,@Field("name") String name,@Field("password")String password);

    @FormUrlEncoded
    @POST("declareMedical.php")
    Call<String> declareMedical(@Field("patiensId")String id, @Field("DeclareMedical") String declareMedical, @Field("contentList")JSONArray list);

    @FormUrlEncoded
    @POST("getInfoAccount.php")
    Call<Patients> getInfoAccount(@Field("id")String id);

    @FormUrlEncoded
    @POST("getListDeclareMedical.php")
    Call<List<DeclareMedical>> getListDeclareMedical(@Field("id")String id);

    @FormUrlEncoded
    @POST("getDeclareMedicalContent.php")
    Call<DeclareMedical> getDeclareMedicalContent (@Field("idDeclareMedical")String id);

    @FormUrlEncoded
    @POST("getInfoVaccination.php")
    Call<VaccineRecord> getInfoVaccination (@Field("idPatients")String id);

    @FormUrlEncoded
    @POST("registerTest.php")
    Call<String> registerTest (@Field("test")String test);

    @FormUrlEncoded
    @POST("getListRegisterTest.php")
    Call<List<RegisterTest>> getListRegisterTest (@Field("idPatients")String id);

    @FormUrlEncoded
    @POST("registerSyringe.php")
    Call<String> registerSyringe (@Field("syringe") String syringe,@Field("list") JSONArray list);

    @FormUrlEncoded
    @POST("getListRegisterSyringe.php")
    Call<List<RegisterSyringe>> getListRegisterSyringe(@Field("idPatients")String id);

    @FormUrlEncoded
    @POST("getListHealthQuotient.php")
    Call<List<Health>> getHealthQuotient(@Field("idPatients")String id,@Field("type")String type);

    @FormUrlEncoded
    @POST("addHealthQuotient.php")
    Call<String> addHealthQuotient(@Field("idPatients")String id,@Field("health")String health);

    @GET("getListHealthFacility.php")
    Call<List<HealthFacility>> getListHealthFacility();

    @FormUrlEncoded
    @POST("getListHealthService.php")
    Call<List<HealthService>> getListHealthService (@Field("idHealthFacility")String id);

    @FormUrlEncoded
    @POST("healthCheck.php")
    Call<String> healthCheck (@Field("healthCheck") String healthCheck,@Field("idPatients")String id);

    @FormUrlEncoded
    @POST("getListHealthCheck.php")
    Call<List<HealthCheck>> getListHealthCheck (@Field("idPatients") String id);

    @FormUrlEncoded
    @POST("updateInfoAccount.php")
    Call<String> updateInfoAccount (@Field("patients") String patiens);

    @FormUrlEncoded
    @POST("getInfoHealthRecord.php")
    Call<HealthRecord> getInfoHealthRecord (@Field("idPatients") String id);

    @FormUrlEncoded
    @POST("getListHealthRecordFilter.php")
    Call <List<Health>> getListHealthRecordFilter (@Field("id")String id,@Field("date")String date);

    @FormUrlEncoded
    @POST("getListAppointmentInject.php")
    Call<List<InjectRecord>> getListAppointmentInject (@Field("id")String id);

    @FormUrlEncoded
    @POST("reactInject.php")
    Call<String> reactInject(@Field("id")String id,@Field("time")String time,@Field("symptom")String symptom);

    @FormUrlEncoded
    @POST("getListReactInject.php")
    Call<List<ReactInject>> getListReactInject (@Field("id")String id);

    @FormUrlEncoded
    @POST("getListHealthCheckAdmin.php")
    Call<List<HealthCheckAdmin>> getListHealthCheckAdmin (@Field("idStaff") String id, @Field("status") String status);

    @FormUrlEncoded
    @POST("updateHealthCheckAdmin.php")
    Call<String> updateHealthCheckAdmin (@Field("idStaff")String idStaff,@Field("id")String id,@Field("status")String status);

    @FormUrlEncoded
    @POST("getListUnConfirmedRegisterSyringeAdmin.php")
    Call<List<RegisterInjectAdmin>> getLURSA(@Field("idStaff") String id, @Field("status") String status);

    @FormUrlEncoded
    @POST("cancelRegisterSyringe.php")
    Call<String> cancelRegiserSyringe (@Field("id") String id,@Field("idStaff") String idStaff,@Field("status") String status);

    @FormUrlEncoded
    @POST("getListInjectSite.php")
    Call<List<Site>> getListInjectSite (@Field("idStaff")String id);

    @FormUrlEncoded
    @POST("confirmedRegisterInject.php")
    Call<String> confirmedRegisterInject (@Field("injectRecord") String string);

    @FormUrlEncoded
    @POST("updateRegisterInject.php")
    Call<String> updateRegiserInject(@Field("injectRecord") String string);

    @FormUrlEncoded
    @POST("getListRegisterTestAdmin.php")
    Call<List<RegisterTestAdmin>> getListRegisterTestAdmin(@Field("idStaff") String id,@Field("status") String status);

    @FormUrlEncoded
    @POST("updateRegisterTest.php")
    Call<String> updateRegisterTest (@Field("id")String id,@Field("idStaff") String idStaff,@Field("status") String status);

    @FormUrlEncoded
    @POST("updateTestResult.php")
    Call<String> updateTestResult (@Field("id")String id,@Field("idStaff") String idStaff, @Field("date")String date,@Field("result")String result);

    @FormUrlEncoded
    @POST("getInfoTestResult.php")
    Call<TestResult> getInfoTestResult (@Field("id")String id);

    @FormUrlEncoded
    @POST("getListRegisterInjectRecord.php")
    Call<List<InjectRecord>> getListRegisterInjectRecord(@Field("idStaff")String id,@Field("status")String status);

    @FormUrlEncoded
    @POST("getListRegisterInjectRecord_1.php")
    Call<List<InjectRecord>> getListRegisterInjectRecord_1(@Field("idStaff")String id);

    @FormUrlEncoded
    @POST("updateRegisterInjectRecord.php")
    Call<String> updateRegisterInjectRecord (@Field("id")String id,@Field("noi")String noi,@Field("time")String time);

    @FormUrlEncoded
    @POST("getListRegisterInjectContent.php")
    Call<List<RegisterInjectContent>> getListRegisterInjectContent(@Field("id")String id);

    @FormUrlEncoded
    @POST("getListReactInjectAdmin.php")
    Call<List<ReactInject>> getListReactInjectAdmin(@Field("idStaff")String id);

    @FormUrlEncoded
    @POST("addHealthFacility.php")
    Call<String> addHealthFacility (@Field("hf")String hf);

    @FormUrlEncoded
    @POST("getListStaff.php")
    Call<List<Staff>> getListStaff (@Field("id")String id);

    @GET("getListStaff_1.php")
    Call<List<Staff>> getListStaff ();

    @FormUrlEncoded
    @POST("addStaff.php")
    Call<String> addStaff (@Field("staff")String staff,@Field("phone")String phone,@Field("password")String password);

    @GET("getTopListHealthFacility.php")
    Call<List<HealthFacility>> getTopListHealthFacility();

    @FormUrlEncoded
    @POST("getListF0.php")
    Call<List<F0Info>> getListF0(@Field("fromDate")String formDate,@Field("toDate")String toDate);

    @FormUrlEncoded
    @POST("getListVaccineReport.php")
    Call<List<VaccineInfo>> getListVaccineReport (@Field("address")String address);

    @FormUrlEncoded
    @POST("getListPatientsVaccineReport.php")
    Call<List<VaccineInfo>> getListPatientsVaccineReport (@Field("address")String address,@Field("noi")String noi);

    @GET("getTopProvinceInject.php")
    Call<List<TopProvince>> getTopProvinceInject();

    @GET("p")
    Call<List<Province>> getListProvince();

    @GET("p/{code}?depth=2")
    Call<Province> getListDistrict(@Path("code") String code);

    @GET("d/{code}?depth=2")
    Call<District> getListWard(@Path("code") String code);

}
