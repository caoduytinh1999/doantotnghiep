package com.example.myapplication.model.declare_medical;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DeclareMedicalContent implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("declareMedicalId")
    @Expose
    private String declareMedicalId;

    @SerializedName("questionId")
    @Expose
    private String questionId;

    @SerializedName("answer")
    @Expose
    private String answer;

    @SerializedName("answerContent")
    @Expose
    private String answerContent;

    public DeclareMedicalContent(String id, String declareMedicalId, String questionId, String answer, String answerContent) {
        this.id = id;
        this.declareMedicalId = declareMedicalId;
        this.questionId = questionId;
        this.answer = answer;
        this.answerContent = answerContent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeclareMedicalId() {
        return declareMedicalId;
    }

    public void setDeclareMedicalId(String declareMedicalId) {
        this.declareMedicalId = declareMedicalId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }
}
