package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.health_quotient.BloodPressure;
import com.example.myapplication.model.health_quotient.BloodSugar;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class HealthAdapter extends RecyclerView.Adapter<HealthAdapter.ViewHolder>{
    List<Health> list;
    IOnClickItemListener listener;

    public HealthAdapter(List<Health> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_quotient,parent,false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthAdapter.ViewHolder holder, int position) {
        Health health = list.get(position);
        holder.textViewTime.setText(health.getTime());
        int layout = 0;
        switch (health.getType()){
            case "BLOODPRESSURE" :
                layout = R.drawable.icon_blood_pressure;
                holder.textViewValue.setText(health.getValue() + " mmHG");
                break;
            case "BLOODSUGAR" :
                layout = R.drawable.icon_glucose_blood;
                holder.textViewValue.setText(health.getValue() + " mg/DL");
                break;
            case "BMI" :
                layout = R.drawable.icon_bmi;
                holder.textViewValue.setText(health.getValue() + " kg/m2");
                break;
            case "TEMPERATURE" :
                layout = R.drawable.ic_tempurature;
                holder.textViewValue.setText(health.getValue() + " \u2103");
                break;
            case "HEARTBEAT" :
                layout = R.drawable.icon_heartbeat;
                holder.textViewValue.setText(health.getValue() + " nhịp/phút");
                break;
            case "HEIGHT" :
                layout = R.drawable.icon_height;
                holder.textViewValue.setText(health.getValue() + " cm");
                break;
            case "SPO2" :
                layout = R.drawable.icon_spo2;
                holder.textViewValue.setText(health.getValue() + " %");
                break;
            case "WEIGHT" :
                layout = R.drawable.icon_body_scale;
                holder.textViewValue.setText(health.getValue() + " kg");
                break;
        }
        holder.imageViewType.setImageResource(layout);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageViewType,imageViewRemove;
        TextView textViewValue,textViewTime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewType = itemView.findViewById(R.id.imageViewTypeRowHQ);
            imageViewRemove = itemView.findViewById(R.id.imageViewRemoveRowHQ);
            textViewValue = itemView.findViewById(R.id.textViewValueRowHQ);
            textViewTime  = itemView.findViewById(R.id.textViewTimeRowHQ);
        }
    }
}
