package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RIRAdminAdapter extends RecyclerView.Adapter<RIRAdminAdapter.ViewHolder>{
    List<InjectRecord> list;
    IOnClickItemListener listener;

    public RIRAdminAdapter(List<InjectRecord> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_register_inject_record,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        InjectRecord record = list.get(position);

        holder.textViewName.setText(record.getPatients().getName());

        holder.textViewAddress.setText(record.getAddress().getName());

        String noi = record.getNumberOfInject();
        if (noi.compareTo("1") ==0) holder.textViewNOI.setBackground((ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_noi_1)));
        else holder.textViewNOI.setBackground((ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_noi_2)));
        holder.textViewNOI.setText(noi);

        String time = record.getDate().split(" ")[0];
        holder.textViewTime.setText(time);

        String date = record.getAppointDate().split(" ")[1];
        String day = date.split("/")[0];
        if (day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);

        String month = date.split("/")[1];
        String year = date.split("/")[2];
        if (month.length() == 1) month = "0" + month;
        holder.textViewMY.setText(month + "/" + year);

        String status = record.getStatus();
        if (status.compareTo("0") ==0){
            status = "Chưa Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.theme_main));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status));
        }
        if (status.compareTo("1") == 0){
            status = "Đã Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.confirmed));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_confirmed));
        }
        holder.textViewStatus.setText(status);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName,textViewAddress,textViewNOI,textViewStatus,textViewDay,textViewMY,textViewTime;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewNameRIR);
            textViewAddress = itemView.findViewById(R.id.textViewVaccineSite);
            textViewNOI = itemView.findViewById(R.id.textViewNumberOfInjectRIR);
            textViewStatus = itemView.findViewById(R.id.textViewStatusRIR);
            textViewDay = itemView.findViewById(R.id.textViewDayRIR);
            textViewMY = itemView.findViewById(R.id.textViewMonthYearRIR);
            textViewTime = itemView.findViewById(R.id.textViewTimeRIR);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
