package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionActivity;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.model.register_syringe.RegisterSyringeContent;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class RI2Fragment extends Fragment {

    Button buttonContinue,buttonReturn;
    RadioGroup radioGroup1, radioGroup2, radioGroup3, radioGroup4, radioGroup5, radioGroup6, radioGroup7, radioGroup8, radioGroup9, radioGroup10;
    RadioButton radioButton;
    EditText editText1,editText3,editText6,editText7,editText10;
    TextInputLayout textInputLayout1,textInputLayout3,textInputLayout6,textInputLayout7,textInputLayout10;
    String answer1,answer2,answer3,answer4,answer5,answer6,answer7,answer8,answer9,answer10;
    List<RadioGroup> list_radio_group = new ArrayList<>();
    List<RegisterSyringeContent> list = new ArrayList<>();
    String objectArray;
    RegisterSyringe syringe;
    String name = "";
    String sex = "";
    String birthday = "";
    String phone = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_r_i2, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapped(view);
        event(view);
    }

    private void event(View view) {
        Bundle bundle = this.getArguments();
        if (bundle != null){
            syringe = bundle.getParcelable("syringe");
            name = bundle.getString("name");
            birthday = bundle.getString("birthday");
            phone = bundle.getString("phone");
            sex = bundle.getString("sex");
        }

//        list_radio_group.add(radioGroup1);
//        list_radio_group.add(radioGroup2);
//        list_radio_group.add(radioGroup3);
//        list_radio_group.add(radioGroup4);
//        list_radio_group.add(radioGroup5);
//        list_radio_group.add(radioGroup6);
//        list_radio_group.add(radioGroup7);
//        list_radio_group.add(radioGroup8);
//        list_radio_group.add(radioGroup9);
//        list_radio_group.add(radioGroup10);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = view.findViewById(checkedId);
                if (button.getTag().toString().compareTo("1") ==0){
                    textInputLayout1.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout1.setVisibility(View.GONE);
                    editText1.setText("");
                }
            }
        });

        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = view.findViewById(checkedId);
                if (button.getTag().toString().compareTo("1") ==0){
                    textInputLayout3.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout3.setVisibility(View.GONE);
                    editText1.setText("");
                }
            }
        });

        radioGroup6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = view.findViewById(checkedId);
                if (button.getTag().toString().compareTo("1") ==0){
                    textInputLayout6.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout6.setVisibility(View.GONE);
                    editText6.setText("");
                }
            }
        });

        radioGroup7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = view.findViewById(checkedId);
                if (button.getTag().toString().compareTo("1") ==0){
                    textInputLayout7.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout7.setVisibility(View.GONE);
                    editText7.setText("");
                }
            }
        });

        radioGroup10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton button = view.findViewById(checkedId);
                if (button.getTag().toString().compareTo("1") ==0){
                    textInputLayout10.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout10.setVisibility(View.GONE);
                    editText10.setText("");
                }
            }
        });


        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceContinueFrament(view);
            }
        });

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private void mapped(View view) {
        buttonContinue = view.findViewById(R.id.buttonContinueRI2);
        buttonReturn   = view.findViewById(R.id.buttonReturnRI2);
        radioGroup1 = view.findViewById(R.id.radiogroupHistory1);
        radioGroup2 = view.findViewById(R.id.radiogroupHistory2);
        radioGroup3 = view.findViewById(R.id.radiogroupHistory3);
        radioGroup4 = view.findViewById(R.id.radiogroupHistory4);
        radioGroup5 = view.findViewById(R.id.radiogroupHistory5);
        radioGroup6 = view.findViewById(R.id.radiogroupHistory6);
        radioGroup7 = view.findViewById(R.id.radiogroupHistory7);
        radioGroup8 = view.findViewById(R.id.radiogroupHistory8);
        radioGroup9 = view.findViewById(R.id.radiogroupHistory9);
        radioGroup10 = view.findViewById(R.id.radiogroupHistory10);
        editText1   = view.findViewById(R.id.editTextContentRI1);
        editText3   = view.findViewById(R.id.editTextContentRI3);
        editText6   = view.findViewById(R.id.editTextContentRI6);
        editText7   = view.findViewById(R.id.editTextContentRI7);
        editText10   = view.findViewById(R.id.editTextContentRI10);
        textInputLayout1 = view.findViewById(R.id.textInputLayoutContentRI1);
        textInputLayout3 = view.findViewById(R.id.textInputLayoutContentRI3);
        textInputLayout6 = view.findViewById(R.id.textInputLayoutContentRI6);
        textInputLayout7 = view.findViewById(R.id.textInputLayoutContentRI7);
        textInputLayout10 = view.findViewById(R.id.textInputLayoutContentRI10);
    }


    public void replaceContinueFrament(View view){
        setData(view);
        Bundle bundle = new Bundle();
        bundle.putParcelable("syringe",syringe);
        bundle.putString("name", name);
        bundle.putString("birthday",birthday);
        bundle.putString("sex",sex);
        bundle.putString("phone",phone);
        RI3Fragment fragment = new RI3Fragment();
        fragment.setArguments(bundle);
        RegisterInjectionActivity.PAGE_NUMBER_FRAGMENT = 3;
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.linearLayoutRegInj1,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setData(View view){
        answer1 = view.findViewById(radioGroup1.getCheckedRadioButtonId()).getTag().toString();
        answer2 = view.findViewById(radioGroup2.getCheckedRadioButtonId()).getTag().toString();
        answer3 = view.findViewById(radioGroup3.getCheckedRadioButtonId()).getTag().toString();
        answer4 = view.findViewById(radioGroup4.getCheckedRadioButtonId()).getTag().toString();
        answer5 = view.findViewById(radioGroup5.getCheckedRadioButtonId()).getTag().toString();
        answer6 = view.findViewById(radioGroup6.getCheckedRadioButtonId()).getTag().toString();
        answer7 = view.findViewById(radioGroup7.getCheckedRadioButtonId()).getTag().toString();
        answer8 = view.findViewById(radioGroup8.getCheckedRadioButtonId()).getTag().toString();
        answer9 = view.findViewById(radioGroup9.getCheckedRadioButtonId()).getTag().toString();
        answer10 = view.findViewById(radioGroup10.getCheckedRadioButtonId()).getTag().toString();

        list.add(new RegisterSyringeContent("","00001",answer1,editText1.getText().toString()));
        list.add(new RegisterSyringeContent("","00003",answer3,editText3.getText().toString()));
        list.add(new RegisterSyringeContent("","00006",answer6,editText6.getText().toString()));
        list.add(new RegisterSyringeContent("","00007",answer7,editText7.getText().toString()));
        list.add(new RegisterSyringeContent("","00010",answer10,editText10.getText().toString()));
        list.add(new RegisterSyringeContent("","00002",answer2,""));
        list.add(new RegisterSyringeContent("","00004",answer4,""));
        list.add(new RegisterSyringeContent("","00005",answer5,""));
        list.add(new RegisterSyringeContent("","00008",answer8,""));
        list.add(new RegisterSyringeContent("","00009",answer9,""));

        syringe.setList(list);
    }

}