package com.example.myapplication.admin.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.RIRAdminAdapter;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RIRConfirmedFragment extends Fragment {
    private static String CONFIRMED_STATUS = "1";
    RecyclerView recyclerView;
    List<InjectRecord> list = new ArrayList<>();
    RIRAdminAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_i_r_confirmed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListRIRConfirmed);
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RIRAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {

            }
        });
    }


    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<InjectRecord>> callback = data.getListRegisterInjectRecord(new Session(getContext()).getID(),CONFIRMED_STATUS);
        callback.enqueue(new Callback<List<InjectRecord>>() {
            @Override
            public void onResponse(Call<List<InjectRecord>> call, Response<List<InjectRecord>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<InjectRecord>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }
}