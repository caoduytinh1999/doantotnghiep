package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.fragment.InjectSiteBottomSheetFragment;
import com.example.myapplication.admin.fragment.VaccineBottomSheetFragment;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.admin.model.Site;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmedRSActivity extends AppCompatActivity {

    EditText editTextName,editTextVaccine,editTextTime,editTextDate,editTextAddress;
    Calendar calendar = Calendar.getInstance();
    Button button;
    List<String> list = new ArrayList<>();
    RegisterInjectAdmin syringe;
    Site injectSite;
    String noi = "";
    String status = "";
    InjectRecord injectRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmed_rsactivity);
        assign();
        event();
    }

    private void event() {
        setData();

        editTextVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheetVaccine();
            }
        });

        editTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });

        editTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheetInjectSite();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmed();
            }
        });
    }

    private void assign() {
        editTextName = findViewById(R.id.editTextNameConfirmedRS);
        editTextVaccine = findViewById(R.id.editTextVaccineConfirmedRS);
        editTextTime = findViewById(R.id.editTextTimeConfirmedRS);
        editTextDate = findViewById(R.id.editTextDateComfirmedRS);
        editTextAddress = findViewById(R.id.editTextAddressConfirmedRS);
        button = findViewById(R.id.buttonConfirmRS);
    }


    public void setData(){
        Intent intent = getIntent();
        syringe = intent.getParcelableExtra("object");
        noi = intent.getStringExtra("noi");
        injectRecord = intent.getParcelableExtra("record");
        status = intent.getStringExtra("status");
        Log.d("AAA","status" + String.valueOf(status));
        if (status != "" && status != null){
            editTextName.setText(injectRecord.getPatients().getName());
        }
        else{
            editTextName.setText(syringe.getPatients().getName());
        }
    }

    public void showTimePicker(){
        LocalDateTime dateTime = LocalDateTime.now();
        int hour = dateTime.getHour();
        int minute = dateTime.getMinute();
        TimePickerDialog dialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                editTextTime.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute));
            }
        },hour,minute,true);
        dialog.show();
    }

    public void showDatePicker(){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
                editTextDate.setText(date);
            }
        }, year, month, day);
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void createBottomSheetVaccine(){
        list.clear();
        list.add("COVID - 19 Vaccine AstraZeneca (AstraZenec)");
        list.add("COVID - 19 Vaccine Comirnaty (Pfizer)");
        list.add("COVID - 19 Vaccine Sputnik-V (Gamalaya)");
        list.add("COVID - 19 Vaccine Spikevax (Moderna)");
        list.add("COVID - 19 Vaccine Vero Cell (Sinopharm)");
        list.add("COVID - 19 Vaccine Janssen (Janssen)");
        VaccineBottomSheetFragment fragment = new VaccineBottomSheetFragment(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU object = (ObjectAU) onClick;
                editTextVaccine.setText(object.getName());
            }
        });

        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void createBottomSheetInjectSite(){
        InjectSiteBottomSheetFragment fragment = new InjectSiteBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                injectSite = (Site) onClick;
                editTextAddress.setText(injectSite.getName());
            }
        });
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void confirmed(){
        String id  = "";
        Patients patients = null;
        String idStaff = "";
        String vaccine = editTextVaccine.getText().toString();;
        String time = editTextTime.getText() + " " + editTextDate.getText();
        InjectRecord record1 = null;
        if (status != "" && status != null){
            record1 = new InjectRecord(injectRecord.getIdRecord(),injectRecord.getId(),injectRecord.getPatients(),injectRecord.getIdStaff(),vaccine,time,"",injectSite,injectRecord.getNumberOfInject(),"0");
        }
        else{
            id = syringe.getId();
            patients = syringe.getPatients();
            idStaff = new Session(this).getID();
            vaccine = editTextVaccine.getText().toString();
            record1 = new InjectRecord("",id,patients,idStaff,vaccine,time,"",injectSite,noi,"0");
        }

        String object = new Gson().toJson(record1);
        DataClient dataClient = APIUltils.getData();
        Call<String> callback = null;
        if (status != "" && status != null){
            callback = dataClient.updateRegiserInject(object);
        }
        else{
            callback = dataClient.confirmedRegisterInject(object);
        }
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismissDialog();
                showToastSuccess("Xác Nhận Thành Công");
                finish();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(ConfirmedRSActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}