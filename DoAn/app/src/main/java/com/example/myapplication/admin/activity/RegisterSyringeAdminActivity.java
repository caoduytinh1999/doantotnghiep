package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.ViewPagerAdminRegisterInjectAdapter;
import com.google.android.material.tabs.TabLayout;

public class RegisterSyringeAdminActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_syringe_admin);
        assign();
        event();
    }

    private void event() {
        ViewPagerAdminRegisterInjectAdapter adapter = new ViewPagerAdminRegisterInjectAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),AddRegisterInjectActivity.class));
            }
        });
    }

    private void assign() {
        viewPager = findViewById(R.id.viewPagerRegisterSyringeAdmin);
        tabLayout = findViewById(R.id.tabLayoutRegisterSyringeAdmin);
        imageView = findViewById(R.id.imageViewAdd);
    }
}