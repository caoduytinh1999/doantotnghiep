package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.adapter.ViewPagerAdviseAdapter;
import com.google.android.material.tabs.TabLayout;

public class AdviseActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advise);
        assign();
        event();
    }

    private void event() {
        ViewPagerAdviseAdapter adapter = new ViewPagerAdviseAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void assign() {
        tabLayout = findViewById(R.id.tabLayoutAdvise);
        viewPager = findViewById(R.id.viewPagerAdvise);
    }
}