package com.example.myapplication.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.AdminAccountFragment;
import com.example.myapplication.admin.fragment.AdminCalendarFragment;
import com.example.myapplication.admin.fragment.AdminHomeFragment;
import com.example.myapplication.admin.fragment.AdminNotifyFragment;
import com.example.myapplication.admin.fragment.AppointmentCancelledFragment;
import com.example.myapplication.admin.fragment.AppointmentConfirmedFragment;
import com.example.myapplication.admin.fragment.AppointmentUnconfirmedFragment;

import org.jetbrains.annotations.NotNull;

public class ViewPagerAdminAppointmentAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdminAppointmentAdapter(FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new AppointmentUnconfirmedFragment();
            case 1:
                return new AppointmentConfirmedFragment();
            case 2 :
                return new AppointmentCancelledFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Chưa Xác Nhận";
                break;
            case 1 :
                title = "Đã Xác Nhận";
                break;
            case 2 :
                title = "Đã Hủy";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
