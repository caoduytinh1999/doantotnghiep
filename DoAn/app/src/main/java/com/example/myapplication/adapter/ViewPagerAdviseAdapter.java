package com.example.myapplication.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.TestCancelledFragment;
import com.example.myapplication.admin.fragment.TestConfirmedFragment;
import com.example.myapplication.admin.fragment.TestUnconfirmedFragment;
import com.example.myapplication.fragment.Advise_1Fragment;
import com.example.myapplication.fragment.Advise_2Fragment;

import org.jetbrains.annotations.NotNull;

public class ViewPagerAdviseAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdviseAdapter(@NonNull @NotNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new Advise_1Fragment();
            case 1:
                return new Advise_2Fragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Phương Pháp Thở";
                break;
            case 1 :
                title = "Phương Pháp Vận Động";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
