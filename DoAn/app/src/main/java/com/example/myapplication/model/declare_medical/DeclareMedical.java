package com.example.myapplication.model.declare_medical;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import retrofit2.http.POST;

public class DeclareMedical extends OnClick implements Serializable, Parcelable{
    @SerializedName("declareMedicalId")
    @Expose
    private String id;

    @SerializedName("patientsId")
    @Expose
    private String patientsId;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("listContent")
    @Expose
    private List<DeclareMedicalContent> list;

    public DeclareMedical(String id, String patientsId, String date, String address) {
        this.id = id;
        this.patientsId = patientsId;
        this.date = date;
        this.address = address;
    }

    public JSONObject convertToJsonObject(){
        JSONObject object = new JSONObject();
        try {
            object.put("id",id);
            object.put("patientsId",patientsId);
            object.put("date",date);
            object.put("address",address);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    protected DeclareMedical(Parcel in) {
        id = in.readString();
        patientsId = in.readString();
        date = in.readString();
        address = in.readString();
    }

    public static final Creator<DeclareMedical> CREATOR = new Creator<DeclareMedical>() {
        @Override
        public DeclareMedical createFromParcel(Parcel in) {
            return new DeclareMedical(in);
        }

        @Override
        public DeclareMedical[] newArray(int size) {
            return new DeclareMedical[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPatientsId() {
        return patientsId;
    }

    public void setPatientsId(String patientsId) {
        this.patientsId = patientsId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<DeclareMedicalContent> getList() {
        return list;
    }

    public void setList(List<DeclareMedicalContent> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(patientsId);
        dest.writeString(date);
        dest.writeString(address);
    }
}
