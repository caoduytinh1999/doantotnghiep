package com.example.myapplication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.DeclareMedicalActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class DeclareMedicalAdapter extends RecyclerView.Adapter<DeclareMedicalAdapter.ViewHolder>{
    List<DeclareMedical> list;
    IOnClickItemListener listener;

    public DeclareMedicalAdapter(List<DeclareMedical> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_file_declare_medical,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeclareMedicalAdapter.ViewHolder holder, int position) {
        DeclareMedical object = list.get(position);
        Session session = new Session(holder.textViewName.getContext());
        String time = object.getDate().split(" ")[0];
        String date = object.getDate().split(" ")[1];
        String second = time.split(":")[2];
        if (second.length() == 1) second = "0" + second;
        String minute = time.split(":")[1];
        if (minute.length() == 1) minute = "0" + minute;
        String hour = time.split(":")[0];
        if (hour.length() == 1) hour = "0" + hour;
        holder.textViewTime.setText(hour + ":" + minute + ":" + second);

        String day = date.split("-")[2];
        if(day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);
        String month = date.split("-")[1];
        if (month.length() == 1) month = "0" + month;
        String year = date.split("-")[0];
        holder.textViewMY.setText(month + "/" + year);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewName,textViewTime;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewHistoryDayDM);
            textViewMY  = itemView.findViewById(R.id.textViewHistoryMYDM);
            textViewName = itemView.findViewById(R.id.textViewHistoryNameDM);
            textViewTime = itemView.findViewById(R.id.textViewHistoryTimeDM);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
