package com.example.myapplication.my_interface;

import android.view.View;

import com.example.myapplication.model.HealthService;

import java.util.List;

public interface ViewIOnClickItemListener {
    void onViewClick(List<HealthService> list);
}
