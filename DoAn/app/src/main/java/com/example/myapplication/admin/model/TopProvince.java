package com.example.myapplication.admin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopProvince {
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("count")
    @Expose
    private String count;

    public TopProvince(String province, String count) {
        this.province = province;
        this.count = count;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
