package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthCheckAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicalAppointmentFragment extends Fragment {

    RecyclerView recyclerView;
    List<HealthCheck> healthChecks = new ArrayList<>();
    HealthCheckAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_medical_appointment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerViewListAConfirmed);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new HealthCheckAdapter(healthChecks, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                replaceFragment((HealthCheck) onClick);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }


    public void setData(){
        healthChecks.clear();
        DataClient data = APIUltils.getData();
        Call<List<HealthCheck>> callback = data.getListHealthCheck(new Session(getContext()).getID());
        callback.enqueue(new Callback<List<HealthCheck>>() {
            @Override
            public void onResponse(Call<List<HealthCheck>> call, Response<List<HealthCheck>> response) {
                if (response.body() != null) {
                    for (int i = 0 ; i < response.body().size() ; i++){
                        HealthCheck healthCheck = response.body().get(i);
                        if (healthCheck.getStatus().compareTo("1") ==0){
                            healthChecks.add(healthCheck);
                        }
                    }
                }
                Collections.reverse(healthChecks);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HealthCheck>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void replaceFragment(HealthCheck healthCheck){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",healthCheck);
        HealthCheckHistoryDetailFragment fragment = new HealthCheckHistoryDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.add(R.id.layoutCalendar,fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }
}