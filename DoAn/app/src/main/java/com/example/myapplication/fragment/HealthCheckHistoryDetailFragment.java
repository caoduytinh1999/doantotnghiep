package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;

import java.text.DecimalFormat;

public class HealthCheckHistoryDetailFragment extends Fragment {
    TextView textViewName,textViewaddress,textViewTime,textViewService,textViewMajor,textViewPrice,textViewSymptom;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_health_check_history_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        Bundle bundle = this.getArguments();
        HealthCheck healthCheck = bundle.getParcelable("object");
        textViewName.setText(new Session(getContext()).getName());
        textViewTime.setText(healthCheck.getTime());
        textViewSymptom.setText(healthCheck.getSymptom());
        textViewaddress.setText(healthCheck.getAddress());
        textViewMajor.setText("");
        DecimalFormat format = new DecimalFormat("###,###,###");
        textViewPrice.setText(String.valueOf(format.format(healthCheck.getPrice())) + " VNĐ");
        textViewService.setText(healthCheck.getService());
    }

    private void assign(View view) {
        textViewName = view.findViewById(R.id.textViewNameHHS);
        textViewaddress = view.findViewById(R.id.textViewAddressHHS);
        textViewTime = view.findViewById(R.id.textViewTimeHHS);
        textViewService = view.findViewById(R.id.textViewServiceHHS);
        textViewMajor = view.findViewById(R.id.textViewMajorHHS);
        textViewPrice = view.findViewById(R.id.textViewPriceHHS);
        textViewSymptom = view.findViewById(R.id.textViewSymptomHHS);
    }
}