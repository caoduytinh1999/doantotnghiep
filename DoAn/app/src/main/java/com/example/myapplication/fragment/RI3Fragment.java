package com.example.myapplication.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.model.register_syringe.RegisterSyringeContent;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RI3Fragment extends Fragment {

    Button buttonConfirm,buttonReturnRI3;
    CheckBox checkBox;
    RegisterSyringe syringe;
    JSONArray objectArray = new JSONArray();
    String name = "";
    String sex= "";
    String phone = "";
    String birthday = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_i3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapped(view);
        event();
    }

    private void event() {
        Bundle bundle = this.getArguments();
        if (bundle != null){
            syringe = bundle.getParcelable("syringe");
            name = bundle.getString("name");
            birthday = bundle.getString("birthday");
            phone = bundle.getString("phone");
            sex = bundle.getString("sex");
        }

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showPopup();
                registerSyringe();
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) buttonConfirm.setEnabled(true);
                else buttonConfirm.setEnabled(false);
            }
        });
    }

    private void mapped(View view) {
        buttonConfirm       = view.findViewById(R.id.buttonContinueRI3);
        buttonReturnRI3     = view.findViewById(R.id.buttonReturnRI3);
        checkBox            = view.findViewById(R.id.checkBoxConfirmRI3);
    }

    public void showPopup(){
        Dialog  dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_register_syringe_success);

        Button button = dialog.findViewById(R.id.buttonClosePopup);
        TextView textViewName = dialog.findViewById(R.id.textViewNamePopupRI);
        TextView textViewPhone = dialog.findViewById(R.id.textViewPhonePopupRI);
        TextView textViewBirthday = dialog.findViewById(R.id.textViewBirthdayPopupRI);
        TextView textViewSex = dialog.findViewById(R.id.textViewSexPopupRI);

        textViewName.setText(name);
        textViewBirthday.setText(birthday);
        textViewPhone.setText(phone);
        textViewSex.setText(sex);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(getContext(), MainActivity.class));
            }
        });

        dialog.show();
    }

    public void registerSyringe(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        String object = new Gson().toJson(syringe);
        for (RegisterSyringeContent item : syringe.getList()){
            objectArray.put(new Gson().toJson(item));
        }
        DataClient data = APIUltils.getData();
        Call<String> callback = data.registerSyringe(object,objectArray);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String mes = response.body();
                if (mes.compareTo("success") == 0){
                    showPopup();
                }
                else{
                    Toast.makeText(getContext(), "Bạn Đã Đăng Kí Tiêm Trước Đó.Vui Lòng Kiểm Tra Lại!", Toast.LENGTH_LONG).show();
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("AAA",t.getMessage());
                Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }
}