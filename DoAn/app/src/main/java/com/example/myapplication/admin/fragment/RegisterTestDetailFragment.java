package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.admin.model.TestResult;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterTestDetailFragment extends Fragment {
    TextView textViewName,textViewHF,textViewDate,textViewTestDate,textViewAddress,textViewTestSite,textViewDateResult,textViewResult;
    ImageView imageView;
    String result = "0";
    RegisterTestAdmin test;
    String date = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register_test_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        Bundle bundle = getArguments();
        test = bundle.getParcelable("object");
        textViewName.setText(test.getPatients().getName());
        textViewHF.setText(test.getTestingSite());
        textViewTestSite.setText(test.getTestingSite());
        textViewTestDate.setText(test.getTestingDate());
        textViewDate.setText(test.getDate());
        textViewAddress.setText(test.getAddress());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopup();
            }
        });
    }

    private void assign(View view) {
        textViewName  = view.findViewById(R.id.textViewNameTAD);
        textViewHF    = view.findViewById(R.id.textViewHealthFacilityTAD);
        textViewAddress = view.findViewById(R.id.textViewAddressTAD);
        textViewDate    = view.findViewById(R.id.textViewDateTAD);
        textViewTestDate = view.findViewById(R.id.textViewTesingDateTAD);
        textViewTestSite = view.findViewById(R.id.textViewTestingSiteTAD);
        textViewDateResult = view.findViewById(R.id.textViewDateResultTAD);
        textViewResult = view.findViewById(R.id.textViewResultTAD);
        imageView = view.findViewById(R.id.buttonPlus);
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<TestResult> callback = data.getInfoTestResult(test.getId());
        callback.enqueue(new Callback<TestResult>() {
            @Override
            public void onResponse(Call<TestResult> call, Response<TestResult> response) {
                dialog.dismissDialog();
                TestResult testResult = response.body();
                String dateTest = testResult.getDateResult();
                if (dateTest.compareTo("") == 0) {
                    dateTest = "Chưa Có";
                }
                else{
                    imageView.setVisibility(View.INVISIBLE);
                }
                textViewDateResult.setText(dateTest);

                String result = testResult.getResult();
                if (result.compareTo("") == 0) result = "Chưa Có";
                else {
                    if (result.compareTo("0") == 0) result = "Âm Tính";
                    else result = "Dương Tính";
                }
                textViewResult.setText(result);
            }

            @Override
            public void onFailure(Call<TestResult> call, Throwable t) {
                dialog.dismissDialog();
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createPopup(){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_test_result);

        TextView textViewTime = dialog.findViewById(R.id.textViewTime);
        RadioButton radioNo = dialog.findViewById(R.id.radioButtonNo);
        RadioButton radioYes = dialog.findViewById(R.id.radioButtonYes);
        Button buttonNo = dialog.findViewById(R.id.buttonNo);
        Button buttonYes = dialog.findViewById(R.id.buttonYes);

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        String date = year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day);
        textViewTime.setText(date);

        radioNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = "0";
            }
        });

        radioYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = "1";
            }
        });

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateResult(date,result);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void updateResult(String date,String result){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateTestResult(test.getId(),new Session(getContext()).getID(),date,result);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().compareTo("success") ==0){
                    showToastSuccess("Cập nhật thành công");
                    if (result.compareTo("0") ==0 ){
                        textViewResult.setText("Âm Tính");
                    }
                    else{
                        textViewResult.setText("Dương Tính");
                    }
                    textViewDateResult.setText(date);
                    imageView.setVisibility(View.INVISIBLE);
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,getActivity().findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}