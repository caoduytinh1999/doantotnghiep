package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.RIRAdminAdapter;
import com.example.myapplication.admin.model.InjectRecord;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRegisterInjectActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RIRAdminAdapter adapter;
    List<InjectRecord> list = new ArrayList<>();
    InjectRecord record;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_register_inject);
        assign();
        event();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    private void event() {
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RIRAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                record = (InjectRecord) onClick;
                RegisterInjectAdmin inject = new RegisterInjectAdmin(record.getId(),record.getPatients(),"",record.getDate(),"",record.getStatus());
                Intent intent = new Intent(getApplicationContext(),ConfirmedRSActivity.class);
                intent.putExtra("object",inject);
                intent.putExtra("noi","2");
                startActivity(intent);
            }
        });
       // setData();
    }

    private void assign() {
        recyclerView = findViewById(R.id.recyclerViewListRIR);
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<InjectRecord>> callback = data.getListRegisterInjectRecord_1(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<List<InjectRecord>>() {
            @Override
            public void onResponse(Call<List<InjectRecord>> call, Response<List<InjectRecord>> response) {
                dialog.dismissDialog();
                list.addAll(response.body());
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<InjectRecord>> call, Throwable t) {
                dialog.dismissDialog();
            }
        });
    }
}