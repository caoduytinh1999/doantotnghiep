package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.register_syringe.RegisterInjectContent;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class InjectContentAdapter extends RecyclerView.Adapter<InjectContentAdapter.ViewHolder>{
    List<RegisterInjectContent> list;
    IOnClickItemListener listener;

    public InjectContentAdapter(List<RegisterInjectContent> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inject_content,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        RegisterInjectContent content = list.get(position);
        holder.textView.setText(content.getQuestion().getContent());
        String answer = content.getAnswer();
        if (answer.compareTo("1") == 0) holder.radioButtonYes.setChecked(true);
        if (answer.compareTo("2") == 0) holder.radioButtonNo.setChecked(true);
        if (answer.compareTo("3") == 0) holder.radioButtonForget.setChecked(true);
        holder.editText.setText(content.getAnswerContent());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        RadioButton radioButtonYes,radioButtonNo,radioButtonForget;
        EditText editText;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            radioButtonYes = itemView.findViewById(R.id.radioYes);
            radioButtonNo = itemView.findViewById(R.id.radioNo);
            radioButtonForget = itemView.findViewById(R.id.radioForget);
            editText = itemView.findViewById(R.id.editTextContent);
        }
    }
}
