package com.example.myapplication.admin.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.Staff;

import org.jetbrains.annotations.NotNull;

public class StaffFragment extends Fragment {

    TextView textViewName,textViewPhone,textViewBirthday,textViewSex,textViewAddress,textViewEmail,textViewIdCode;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_staff, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        Bundle bundle = this.getArguments();
        Staff staff = bundle.getParcelable("object");
        textViewName.setText(staff.getName());
        textViewPhone.setText(staff.getPhone());
        textViewSex.setText((staff.getSex().compareTo("1") == 0 ? "Nam" : "Nữ"));
        textViewAddress.setText(staff.getAddress());
        textViewBirthday.setText(staff.getBirthday());
        textViewEmail.setText(staff.getEmail());
        textViewIdCode.setText(staff.getIdcode());
    }

    private void assign(View view) {
        textViewName = view.findViewById(R.id.textViewName);
        textViewPhone = view.findViewById(R.id.textViewPhone);
        textViewBirthday = view.findViewById(R.id.textViewBirthday);
        textViewSex = view.findViewById(R.id.textViewSex);
        textViewAddress = view.findViewById(R.id.textViewAddress);
        textViewEmail = view.findViewById(R.id.textViewEmail);
        textViewIdCode = view.findViewById(R.id.textViewIdCode);
    }
}