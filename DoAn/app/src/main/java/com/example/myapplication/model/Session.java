package com.example.myapplication.model;

import android.content.Context;
import android.content.SharedPreferences;

public class Session {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public Session(Context mContext){
        preferences = mContext.getSharedPreferences("AppKey",mContext.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    public void setRole(String role){
        editor.putString("ROLE",role);
        editor.apply();
    }

    public String getRole(){
        return preferences.getString("ROLE","");
    }

    public void setID(String id){
        editor.putString("ID",id);
        editor.apply();
    }

    public void setName(String name){
        editor.putString("NAME",name);
        editor.apply();
    }

    public String getName(){
        return preferences.getString("NAME","");
    }

    public String getID(){
        return preferences.getString("ID","");
    }

    public void setToken(String token){
        editor.putString("TOKEN",token);
        editor.apply();
    }

    public String getToken(){
        return preferences.getString("TOKEN","");
    }
}
