package com.example.myapplication.admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterTestAdmin extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("patients")
    @Expose
    private Patients patients;

    @SerializedName("idStaff")
    @Expose
    private String idStaff;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("testingDate")
    @Expose
    private String testingDate;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("testingSite")
    @Expose
    private String testingSite;

    @SerializedName("status")
    @Expose
    private String status;

    public RegisterTestAdmin(String id, Patients patients, String idStaff, String date, String testingDate, String address, String testingSite, String status) {
        this.id = id;
        this.patients = patients;
        this.idStaff = idStaff;
        this.date = date;
        this.testingDate = testingDate;
        this.address = address;
        this.testingSite = testingSite;
        this.status = status;
    }

    protected RegisterTestAdmin(Parcel in) {
        id = in.readString();
        patients = in.readParcelable(Patients.class.getClassLoader());
        idStaff = in.readString();
        date = in.readString();
        testingDate = in.readString();
        address = in.readString();
        testingSite = in.readString();
        status = in.readString();
    }

    public static final Creator<RegisterTestAdmin> CREATOR = new Creator<RegisterTestAdmin>() {
        @Override
        public RegisterTestAdmin createFromParcel(Parcel in) {
            return new RegisterTestAdmin(in);
        }

        @Override
        public RegisterTestAdmin[] newArray(int size) {
            return new RegisterTestAdmin[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTestingDate() {
        return testingDate;
    }

    public void setTestingDate(String testingDate) {
        this.testingDate = testingDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTestingSite() {
        return testingSite;
    }

    public void setTestingSite(String testingSite) {
        this.testingSite = testingSite;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(patients, flags);
        dest.writeString(idStaff);
        dest.writeString(date);
        dest.writeString(testingDate);
        dest.writeString(address);
        dest.writeString(testingSite);
        dest.writeString(status);
    }
}
