package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.ViewPagerAnalysisAdapter;
import com.example.myapplication.admin.adapter.ViewPagerReportAdapter;
import com.google.android.material.tabs.TabLayout;

public class ReportActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        tabLayout = findViewById(R.id.tabLayoutReport);
        viewPager = findViewById(R.id.viewPagerReport);

        ViewPagerReportAdapter adapter = new ViewPagerReportAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}