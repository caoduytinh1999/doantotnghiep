package com.example.myapplication.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoAB2Fragment extends Fragment {

    TextView textViewHospitalName,textViewName,textViewService,textViewAddress,textViewTime,textViewPrice,textViewSymptom;
    Button buttonReturn,buttonContinue;
    HealthCheck healthCheck;
    HealthFacility facility;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_a_b2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void assign(View view) {
        textViewHospitalName = view.findViewById(R.id.textViewHospitalNameInfoAB2);
        textViewName         = view.findViewById(R.id.textViewNameAB2);
        textViewService      = view.findViewById(R.id.textViewServiceAB2);
        textViewAddress      = view.findViewById(R.id.textViewAddressAB2);
        textViewTime         = view.findViewById(R.id.textViewTimeAB2);
        textViewPrice        = view.findViewById(R.id.textViewPriceAB2);
        textViewSymptom      = view.findViewById(R.id.textViewSymptomAB2);
        buttonContinue       = view.findViewById(R.id.buttonContinueInfoAB2);
        buttonReturn         = view.findViewById(R.id.buttonReturnInfoAB2);
    }

    private void event(View view){
        Bundle bundle = this.getArguments();
        healthCheck = bundle.getParcelable("health");
        facility = bundle.getParcelable("facility");

        textViewHospitalName.setText("Cơ sở y tê : " + facility.getName());
        textViewName.setText(new Session(getContext()).getName());
        textViewService.setText(healthCheck.getService());
        textViewAddress.setText(healthCheck.getAddress());
        textViewTime.setText(healthCheck.getTime());
        DecimalFormat format = new DecimalFormat("###,###,###");
        textViewPrice.setText(String.valueOf(format.format(healthCheck.getPrice())) + " đ");
        textViewSymptom.setText(healthCheck.getSymptom());

        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                healthCheck();
            }
        });
    }

    public void healthCheck(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        String object = new Gson().toJson(healthCheck);
        DataClient data = APIUltils.getData();
        Call<String> callback = data.healthCheck(object,new Session(getContext()).getID());
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                dialog.dismissDialog();
                showPopup();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showPopup(){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_appointment_health_check);

        Button button = dialog.findViewById(R.id.buttonClosePopup);
        TextView textViewName = dialog.findViewById(R.id.textViewNamePopupRI);
        TextView textViewAddress = dialog.findViewById(R.id.textViewPhonePopupRI);
        TextView textViewTime = dialog.findViewById(R.id.textViewBirthdayPopupRI);
        TextView textViewHName = dialog.findViewById(R.id.textViewSexPopupRI);

        textViewName.setText(new Session(getContext()).getName());
        textViewAddress.setText(facility.getAddress());
        textViewTime.setText(healthCheck.getTime());
        textViewHName.setText(facility.getName());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivity(new Intent(getContext(), MainActivity.class));
            }
        });

        dialog.show();
    }
}