package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class HealthFacilityAdapter extends RecyclerView.Adapter<HealthFacilityAdapter.ViewHolder>{
    List<HealthFacility> list;
    IOnClickItemListener listener;

    public HealthFacilityAdapter(List<HealthFacility> list,IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_health_facilities,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthFacilityAdapter.ViewHolder holder, int position) {
        HealthFacility object = list.get(position);
        holder.textViewHospitalName.setText(object.getName());
        holder.textViewHospitalAddress.setText(object.getAddress());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewHospitalName,textViewHospitalAddress;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewHospitalName    = itemView.findViewById(R.id.textViewHospitalName);
            textViewHospitalAddress = itemView.findViewById(R.id.textViewHospitalAddress);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }

}
