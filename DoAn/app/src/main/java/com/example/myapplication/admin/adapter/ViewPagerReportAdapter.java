package com.example.myapplication.admin.adapter;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.admin.fragment.HealthCheckAnalysisFragment;
import com.example.myapplication.admin.fragment.InjectAnalysisFragment;
import com.example.myapplication.admin.fragment.Report_1Fragment;
import com.example.myapplication.admin.fragment.Report_2Fragment;
import com.example.myapplication.admin.fragment.Report_3Fragment;
import com.example.myapplication.admin.fragment.TestAnalysisFragment;

import org.jetbrains.annotations.NotNull;

public class ViewPagerReportAdapter extends FragmentStatePagerAdapter {
    public ViewPagerReportAdapter(@NonNull @NotNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new Report_1Fragment();
            case 1:
                return new Report_2Fragment();
            case 2 :
                return new Report_3Fragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Số lượng người nhiễm";
                break;
            case 1 :
                title = "Số lượng liều vắc xin";
                break;
            case 2 :
                title = "Só lượng người đã tiêm";
                break;
        }
        SpannableString ss = new SpannableString(title);
        ss.setSpan(new StyleSpan(Typeface.BOLD),0,title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }
}
