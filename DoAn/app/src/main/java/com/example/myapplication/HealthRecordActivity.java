package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.myapplication.model.Session;

public class HealthRecordActivity extends AppCompatActivity {

    CardView cardView;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_record);
        assign();
        event();
    }

    private void event() {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),HealthRecordDetailActivity.class));
            }
        });
        textView.setText(new Session(this).getName());
    }

    private void assign() {
        cardView = findViewById(R.id.cardViewHealthRecord);
        textView = findViewById(R.id.textViewName);
    }
}