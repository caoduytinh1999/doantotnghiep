package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ReactInject;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ReactInjectAdapter extends RecyclerView.Adapter<ReactInjectAdapter.ViewHolder>{

    List<ReactInject> list;
    IOnClickItemListener listener;

    public ReactInjectAdapter(List<ReactInject> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_react_inject,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        ReactInject inject = list.get(position);
        String date = inject.getTime().split(" ")[0];
        String day = date.split("/")[0];
        String month = date.split("/")[1];
        String year = date.split("/")[2];
        String time = inject.getTime().split(" ")[1];
        holder.textViewDay.setText(day);
        holder.textViewMY.setText(month + "/" + year);
        holder.textViewTime.setText(time);
        holder.textViewName.setText(inject.getInjectRecord().getPatients().getName());
        holder.textViewPlace.setText(inject.getInjectRecord().getAddress().getName());
        holder.textViewVaccine.setText(inject.getInjectRecord().getVaccine());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewTime,textViewName,textViewVaccine,textViewPlace;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewDayRIR);
            textViewMY = itemView.findViewById(R.id.textViewMonthYearRIR);
            textViewTime = itemView.findViewById(R.id.textViewTimeRIR);
            textViewName = itemView.findViewById(R.id.textViewNameRIR);
            textViewVaccine = itemView.findViewById(R.id.textViewVaccine);
            textViewPlace = itemView.findViewById(R.id.textView999);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
