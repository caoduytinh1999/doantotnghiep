package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.model.Session;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class HealthCodeActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_code);
        assign();
        event();
    }

    private void event() {
        createQRCode();
    }

    private void assign() {
        imageView = findViewById(R.id.imageViewQRCodeHealthCode);
        textView  = findViewById(R.id.textViewNameHealthCode);
    }

    public void createQRCode(){
        Random r = new Random();
        int i1 = r.nextInt(999999 - 1) + 1;
        Session session = new Session(this);
        String id = session.getID();
        String token = session.getToken();
        JSONObject object = new JSONObject();
        try {
            object.put("id",id);
            object.put("token",token);
            object.put("random", i1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String content = String.valueOf(object);
        MultiFormatWriter writer = new MultiFormatWriter();
        try {
            BitMatrix matrix = writer.encode(content, BarcodeFormat.QR_CODE,180,180);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(matrix);
            imageView.setImageBitmap(bitmap);
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }
}