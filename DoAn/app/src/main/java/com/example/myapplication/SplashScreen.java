package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.admin.activity.ManageActivity;
import com.example.myapplication.model.Session;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLogin();
    }

    public void checkLogin(){
        Session session = new Session(getApplicationContext());
        String token = session.getToken();
        if (token.compareTo("") != 0){
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    DataClient dataClient = APIUltils.getData();
                    Call<String> callback = dataClient.checkLogin(token);
                    callback.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String mes = response.body();
                            if (mes.compareTo("success") == 0){
                                if (session.getRole().compareTo("2") == 0){
                                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                    finish();
                                }
                                else{
                                    if (session.getRole().compareTo("1") == 0){
                                        startActivity(new Intent(getApplicationContext(),AdminActivity.class));
                                        finish();
                                    }
                                    else{
                                        startActivity(new Intent(getApplicationContext(), ManageActivity.class));
                                        finish();
                                    }
                                }
                            }
                            else{
                                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
            thread.start();
        }else{
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            finish();
        }
    }
}
