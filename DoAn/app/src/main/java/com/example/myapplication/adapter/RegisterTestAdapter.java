package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class RegisterTestAdapter extends RecyclerView.Adapter<RegisterTestAdapter.ViewHolder>{
    List<RegisterTest> list;
    IOnClickItemListener listener;

    public RegisterTestAdapter(List<RegisterTest> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_register_test,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RegisterTestAdapter.ViewHolder holder, int position) {
        RegisterTest registerTest = list.get(position);
        holder.textViewName.setText(new Session(holder.textViewName.getContext()).getName());
        String day = registerTest.getTestingDate().split("/")[0].trim();
        if (day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);
        String month = registerTest.getTestingDate().split("/")[1].trim();
        if (month.length() == 1) month = "0" + month;
        String year = registerTest.getTestingDate().split("/")[2].trim();
        holder.textViewMY.setText(month + "/" + year);
        holder.textViewAddress.setText(registerTest.getTestingSite());
        String status = registerTest.getStatus();
        if (status.compareTo("0") == 0) holder.textViewStatus.setText("Chưa xác nhận");
        else {
            if (status.compareTo("1") == 0) holder.textViewStatus.setText("Đã xác nhận");
            else holder.textViewStatus.setText("Đã hủy");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewName,textViewAddress,textViewStatus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
             textViewDay = itemView.findViewById(R.id.textViewHistoryDayRT);
             textViewMY = itemView.findViewById(R.id.textViewHistoryMYRT);
             textViewName = itemView.findViewById(R.id.textViewHistoryNameRT);
             textViewAddress = itemView.findViewById(R.id.textViewHistoryAddressRT);
             textViewStatus = itemView.findViewById(R.id.textViewStatusHistoryRT);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
