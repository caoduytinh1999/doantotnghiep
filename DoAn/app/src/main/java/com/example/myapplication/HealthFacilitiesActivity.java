package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.adapter.HealthFacilityAdapter;
import com.example.myapplication.fragment.HealthFacilitiesInfoFragment;
import com.example.myapplication.fragment.RI2Fragment;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthFacilitiesActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView textView;
    HealthFacilityAdapter adapter;
    List<HealthFacility> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_facilities);
        assign();
        event();
    }

    private void event() {
        innitRecyclerView();
    }

    private void assign() {
        recyclerView = findViewById(R.id.recyclerViewListHealthFacilities);
        textView     = findViewById(R.id.textViewNumberOfHF);
    }

    private void innitRecyclerView(){

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        assignData();
    }

    public void assignData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<HealthFacility>> callback = data.getListHealthFacility();
        callback.enqueue(new Callback<List<HealthFacility>>() {
            @Override
            public void onResponse(Call<List<HealthFacility>> call, Response<List<HealthFacility>> response) {
                list = response.body();
                textView.setText("Danh sách cơ sở y tế (" + list.size() + ")");
                adapter = new HealthFacilityAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {
                        HealthFacility facility = (HealthFacility) onClick;
                        replaceFragment(facility);
                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<HealthFacility>> call, Throwable t) {

            }
        });
    }

    public void replaceFragment(HealthFacility facility){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",facility);
        HealthFacilitiesInfoFragment fragment = new HealthFacilitiesInfoFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthFacilities,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("AAA","pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("AAA","stop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AAA","resume");
    }
}