package com.example.myapplication.model.register_syringe;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterSyringeContent implements Parcelable {
    @SerializedName("idRegisterSyringe")
    @Expose
    private String idRegisterSyringe;

    @SerializedName("idQuestion")
    @Expose
    private String idQuestion;

    @SerializedName("answer")
    @Expose
    private String answer;

    @SerializedName("answerContent")
    @Expose
    private String answerContent;

    public RegisterSyringeContent(String idRegisterSyringe, String idQuestion, String answer, String answerContent) {
        this.idRegisterSyringe = idRegisterSyringe;
        this.idQuestion = idQuestion;
        this.answer = answer;
        this.answerContent = answerContent;
    }

    protected RegisterSyringeContent(Parcel in) {
        idRegisterSyringe = in.readString();
        idQuestion = in.readString();
        answer = in.readString();
        answerContent = in.readString();
    }

    public static final Creator<RegisterSyringeContent> CREATOR = new Creator<RegisterSyringeContent>() {
        @Override
        public RegisterSyringeContent createFromParcel(Parcel in) {
            return new RegisterSyringeContent(in);
        }

        @Override
        public RegisterSyringeContent[] newArray(int size) {
            return new RegisterSyringeContent[size];
        }
    };

    public String getIdRegisterSyringe() {
        return idRegisterSyringe;
    }

    public void setIdRegisterSyringe(String idRegisterSyringe) {
        this.idRegisterSyringe = idRegisterSyringe;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idRegisterSyringe);
        dest.writeString(idQuestion);
        dest.writeString(answer);
        dest.writeString(answerContent);
    }
}
