package com.example.myapplication.admin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestResult {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("idStaff")
    @Expose
    private String idStaff;
    @SerializedName("dateResult")
    @Expose
    private String dateResult;
    @SerializedName("result")
    @Expose
    private String result;

    public TestResult(String id, String idStaff, String dateResult, String result) {
        this.id = id;
        this.idStaff = idStaff;
        this.dateResult = dateResult;
        this.result = result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getDateResult() {
        return dateResult;
    }

    public void setDateResult(String dateResult) {
        this.dateResult = dateResult;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
