package com.example.myapplication.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.DeclareMedicalActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionActivity;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.model.declare_medical.DeclareMedicalContent;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeclareMedicalFragment extends Fragment {

    EditText editTextName,editTextBirthday,editTextID,editTextNation,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextPhone,editTextEmail,editTextContent1,editTextContent2;
    RadioGroup radioGroup1,radioGroup2,radioGroup3,radioGroup4,radioGroup5;
    TextInputLayout textInputLayout1,textInputLayout2;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    Button button;

    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    String name = "";
    String sex = "";
    String birthday = "";
    String phone = "";
    String provinvce_code = "";
    String district_code = "";
    String ward_code = "";
    List<ObjectAU> auList = new ArrayList<>();
    String province = "";
    String district = "";
    String ward = "";
    String place = "";
    String answer1 = "2";
    String ansser_content_1 = "";
    String answer2 = "2";
    String ansser_content_2 = "";
    String answer3 = "2";
    String answer4 = "2";
    String answer5 = "2";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_declare_medical, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        setData();

        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provinvce_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố",view);
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,provinvce_code,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (district_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Phường/Xã",view);
                }
                else {
                    createBottomSheet(CODE_NAME_WARD,district_code,"Chọn Phường/Xã");
                }
            }
        });

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                if (radioButton.getTag().toString().compareTo("1") == 0){
                    textInputLayout1.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout1.setVisibility(View.GONE);
                    editTextContent1.setText("");
                }
                answer1 = radioButton.getTag().toString();
            }
        });

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                if (radioButton.getTag().toString().compareTo("1") == 0){
                    textInputLayout2.setVisibility(View.VISIBLE);
                }
                else{
                    textInputLayout2.setVisibility(View.GONE);
                    editTextContent2.setText("");
                }
                answer2 = radioButton.getTag().toString();
            }
        });

        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                answer3 = radioButton.getTag().toString();
            }
        });

        radioGroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                answer4 = radioButton.getTag().toString();
            }
        });

        radioGroup5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = view.findViewById(checkedId);
                answer5 = radioButton.getTag().toString();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ansser_content_1 = editTextContent1.getText().toString();
                ansser_content_2 = editTextContent2.getText().toString();
                declareMedical();
            }
        });
    }

    private void assign(View view) {
        editTextName     = view.findViewById(R.id.editTextNameDM);
        editTextBirthday = view.findViewById(R.id.editTextBirthdayDM);
        editTextID       = view.findViewById(R.id.editTextIDDM);
        editTextProvince = view.findViewById(R.id.editTextCityDM);
        editTextDistrict = view.findViewById(R.id.editTextDistricDM);
        editTextWard     = view.findViewById(R.id.editTextWardDM);
        editTextPlace    = view.findViewById(R.id.editTextPlaceDM);
        editTextPhone    = view.findViewById(R.id.editTextPhoneDM);
        editTextEmail    = view.findViewById(R.id.editTextEmailDM);
        editTextContent1 = view.findViewById(R.id.editTextContentDM1);
        editTextContent2 = view.findViewById(R.id.editTextContentDM2);
        radioGroup1      = view.findViewById(R.id.radiogroupDM1);
        radioGroup2      = view.findViewById(R.id.radiogroupDM2);
        radioGroup3      = view.findViewById(R.id.radiogroupDM3);
        radioGroup4      = view.findViewById(R.id.radiogroupDM4);
        radioGroup5      = view.findViewById(R.id.radiogroupDM5);
        radioButtonMale  = view.findViewById(R.id.radioSexMaleDM);
        radioButtonFemale  = view.findViewById(R.id.radioSexFeMaleDM);
        radioButtonOther  = view.findViewById(R.id.radioOtherDM);
        textInputLayout1 = view.findViewById(R.id.textInputLayoutConentDM1);
        textInputLayout2 = view.findViewById(R.id.textInputLayoutContentDM2);
        button           = view.findViewById(R.id.buttonSendFileDM);
    }

    public void setData(){
        Session session = new Session(getContext());
        DataClient data = APIUltils.getData();
        Call<Patients> callback = data.getInfoAccount(session.getID());
        callback.enqueue(new Callback<Patients>() {
            @Override
            public void onResponse(Call<Patients> call, Response<Patients> response) {
                Patients patients = response.body();
                editTextName.setText(patients.getName());
                editTextBirthday.setText(patients.getBirthday());
                editTextID.setText(patients.getIdcode());
                editTextPhone.setText(patients.getPhone());
                editTextEmail.setText(patients.getEmail());
                sex = "Nam";
                if (patients.getSex().compareTo("2") ==0){
                    radioButtonFemale.setChecked(true);
                    sex = "Nữ";
                }
                if (patients.getSex().compareTo("3") ==0){
                    radioButtonOther.setChecked(true);
                    sex = "khác";
                }

                name = patients.getName();
                birthday = patients.getBirthday();
                phone = patients.getPhone();

                String address = patients.getAddress();
                String province = address.split(",")[3];
                String district = address.split(",")[2];
                String ward = address.split(",")[1];
                String place = address.split(",")[0];

                editTextProvince.setText(province);
                editTextDistrict.setText(district);
                editTextWard.setText(ward);
                editTextPlace.setText(place);
            }

            @Override
            public void onFailure(Call<Patients> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showPopup(){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_declare_medical_success);

        Button button = dialog.findViewById(R.id.buttonClosePopupDM);
        TextView textViewTitle,textViewMes,textViewName,textViewSex,textViewPhone,textViewBirthday;
        textViewTitle = dialog.findViewById(R.id.textViewTitlePopup);
        textViewMes = dialog.findViewById(R.id.textViewMessagePopup);
        textViewName = dialog.findViewById(R.id.textViewNamePopupDM);
        textViewSex = dialog.findViewById(R.id.textViewSexPopupDM);
        textViewBirthday = dialog.findViewById(R.id.textViewBirthdayPopupDM);
        textViewPhone = dialog.findViewById(R.id.textViewPhonePopupDM);

        textViewTitle.setText("Khai báo y tế thành công");
        textViewMes.setText("Cảm ơn quí khách đã khai báo y tế");
        textViewName.setText(name);
        textViewBirthday.setText(birthday);
        textViewSex.setText(sex);
        textViewPhone.setText(phone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void declareMedical(){
        if (checkValidate()){
            long millis=System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            LocalDateTime dateTime = LocalDateTime.now();
            int hour = dateTime.getHour();;
            int minute = dateTime.getMinute();
            int second = dateTime.getSecond();
            String time = hour + ":" + minute + ":" + second + " " + date; // 23:30:15 2021-02-10
            String address = province + "," + district + "," + ward + "," + editTextPlace.getText().toString();
            Session session = new Session(getContext());
            Log.d("AAA","ID : " + session.getID());
            DeclareMedical declareMedical = new DeclareMedical("",session.getID(),time,address);
            String declareMedicalObject = new Gson().toJson(declareMedical);

            JSONArray array = new JSONArray();
            array.put(new Gson().toJson(new DeclareMedicalContent("","","00001",answer1,ansser_content_1)));
            array.put(new Gson().toJson(new DeclareMedicalContent("","","00002",answer2,ansser_content_2)));
            array.put(new Gson().toJson(new DeclareMedicalContent("","","00003",answer3,"")));
            array.put(new Gson().toJson(new DeclareMedicalContent("","","00004",answer4,"")));
            array.put(new Gson().toJson(new DeclareMedicalContent("","","00005",answer5,"")));

            LoadingDialog dialog = new LoadingDialog(getActivity());
            dialog.createDialog();
            DataClient dataClient = APIUltils.getData();
            Call<String> callback = dataClient.declareMedical(session.getID(),declareMedicalObject,array);
            callback.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String mes = response.body();
                    if (mes.compareTo("success") ==0){
                        showPopup();
                    }
                    dialog.dismissDialog();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(getContext(), "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                    dialog.dismissDialog();
                }
            });
        }
    }

    public boolean checkValidate(){
        if (editTextProvince.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn tỉnh/thành phố",getView());
            return false;
        }

        if (editTextDistrict.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn quận/huyện",getView());
            return false;
        }

        if (editTextWard.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn phường/xã",getView());
            return false;
        }

        if (editTextPlace.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn nơi ở",getView());
            return false;
        }

        if (answer1.compareTo("1") == 0 && editTextContent1.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập thông tin",getView());
            editTextContent1.requestFocus();
            return false;
        }

        if (answer2.compareTo("1") == 0 && editTextContent2.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập thông tin",getView());
            editTextContent2.requestFocus();
            return false;
        }

        return true;
    }

    public void createBottomSheet(String codename,String code,String title){
        auList.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(auList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (provinvce_code.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    provinvce_code = objectAU.getCode();
                    province = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (district_code.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    district_code = objectAU.getCode();
                    district = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    ward_code = objectAU.getCode();
                    ward = objectAU.getName();
                }
            }
        },title,codename,code);
        fragment.show(getActivity().getSupportFragmentManager(),fragment.getTag());
    }

    public void showToastWarning(String content,View viewGroup){
        Toast toast = new Toast(getContext());
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,viewGroup.findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}