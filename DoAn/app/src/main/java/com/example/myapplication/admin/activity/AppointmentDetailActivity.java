package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.health_check.HealthCheckAdmin;

import java.text.DecimalFormat;

public class AppointmentDetailActivity extends AppCompatActivity {
    TextView textViewName,textViewaddress,textViewTime,textViewService,textViewMajor,textViewPrice,textViewSymptom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_detail);
        assign();
        event();
    }

    private void event() {
        Intent intent = getIntent();
        HealthCheckAdmin checkAdmin = intent.getParcelableExtra("object");
        textViewName.setText(checkAdmin.getNamePatients());
        textViewaddress.setText(checkAdmin.getNameHealthFacility());
        textViewMajor.setText("");
        DecimalFormat format = new DecimalFormat("###,###,###");
        textViewPrice.setText(format.format(checkAdmin.getPrice()));
        textViewService.setText(checkAdmin.getService());
        textViewSymptom.setText(checkAdmin.getSymptom());
        textViewTime.setText(checkAdmin.getTime());
    }

    private void assign() {
        textViewName = findViewById(R.id.textViewNameADA);
        textViewaddress = findViewById(R.id.textViewAddressADA);
        textViewTime = findViewById(R.id.textViewTimeADA);
        textViewService = findViewById(R.id.textViewServiceADA);
        textViewMajor = findViewById(R.id.textViewMajorADA);
        textViewPrice = findViewById(R.id.textViewPriceADA);
        textViewSymptom = findViewById(R.id.textViewSymptomADA);
    }
}