package com.example.myapplication.model.test;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterTest extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("idPatients")
    @Expose
    private String idPatients;

    @SerializedName("idStaff")
    @Expose
    private String idStaff;

    @SerializedName("testingDate")
    @Expose
    private String testingDate;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("testingSite")
    @Expose
    private String testingSite;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("status")
    @Expose
    private String status;

    public RegisterTest(String id, String idPatients, String idStaff, String testingDate, String date, String testingSite, String address, String status) {
        this.id = id;
        this.idPatients = idPatients;
        this.idStaff = idStaff;
        this.testingDate = testingDate;
        this.date = date;
        this.testingSite = testingSite;
        this.address = address;
        this.status = status;
    }

    protected RegisterTest(Parcel in) {
        id = in.readString();
        idPatients = in.readString();
        idStaff = in.readString();
        testingDate = in.readString();
        date = in.readString();
        testingSite = in.readString();
        address = in.readString();
        status = in.readString();
    }

    public static final Creator<RegisterTest> CREATOR = new Creator<RegisterTest>() {
        @Override
        public RegisterTest createFromParcel(Parcel in) {
            return new RegisterTest(in);
        }

        @Override
        public RegisterTest[] newArray(int size) {
            return new RegisterTest[size];
        }
    };

    public String getTestingDate() {
        return testingDate;
    }

    public void setTestingDate(String testingDate) {
        this.testingDate = testingDate;
    }

    public String getTestingSite() {
        return testingSite;
    }

    public void setTestingSite(String testingSite) {
        this.testingSite = testingSite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPatients() {
        return idPatients;
    }

    public void setIdPatients(String idPatients) {
        this.idPatients = idPatients;
    }

    public String getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(String idStaff) {
        this.idStaff = idStaff;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(idPatients);
        dest.writeString(idStaff);
        dest.writeString(testingDate);
        dest.writeString(date);
        dest.writeString(testingSite);
        dest.writeString(address);
        dest.writeString(status);
    }
}
