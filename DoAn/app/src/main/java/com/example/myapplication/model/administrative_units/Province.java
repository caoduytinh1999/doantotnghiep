package com.example.myapplication.model.administrative_units;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Province implements Serializable {
    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("division_type")
    @Expose
    private String division_type;

    @SerializedName("codename")
    @Expose
    private String codename;

    @SerializedName("phone_code")
    @Expose
    private String phone_code;

    @SerializedName("districts")
    @Expose
    private List<District> districts;

    public Province(String code, String name, String division_type, String codename, String phone_code, List<District> districts) {
        this.code = code;
        this.name = name;
        this.division_type = division_type;
        this.codename = codename;
        this.phone_code = phone_code;
        this.districts = districts;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision_type() {
        return division_type;
    }

    public void setDivision_type(String division_type) {
        this.division_type = division_type;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }
}
