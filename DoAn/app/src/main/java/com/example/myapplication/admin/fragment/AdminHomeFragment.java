package com.example.myapplication.admin.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.admin.activity.AppointmentActivity;
import com.example.myapplication.admin.activity.ReactInjectAdminActivity;
import com.example.myapplication.admin.activity.RegisterInjectRecordActivity;
import com.example.myapplication.admin.activity.RegisterSyringeAdminActivity;
import com.example.myapplication.admin.activity.TestAdminActivity;

import org.jetbrains.annotations.NotNull;


public class AdminHomeFragment extends Fragment {

    CardView cardViewSyringe,cardViewTest,cardViewcertification,cardViewAppointment,cardViewInjectRecord;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
          cardViewSyringe.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                    startActivity(new Intent(getContext(), RegisterSyringeAdminActivity.class));
              }
          });

          cardViewAppointment.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                    startActivity(new Intent(getContext(), AppointmentActivity.class));
              }
          });

          cardViewTest.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                    startActivity(new Intent(getContext(), TestAdminActivity.class));
              }
          });

          cardViewcertification.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                startActivity(new Intent(getContext(), ReactInjectAdminActivity.class));
              }
          });

          cardViewInjectRecord.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  startActivity(new Intent(getContext(), RegisterInjectRecordActivity.class));
              }
          });
    }

    private void assign(View view) {
        cardViewSyringe         = view.findViewById(R.id.cardViewSyringeAdmin);
        cardViewTest            = view.findViewById(R.id.cardViewTestAdmin);
        cardViewcertification   = view.findViewById(R.id.cardViewcertificationAdmin);
        cardViewAppointment     = view.findViewById(R.id.cardViewAppointmentAdmin);
        cardViewInjectRecord    = view.findViewById(R.id.cardViewInjectRecord);
    }
}