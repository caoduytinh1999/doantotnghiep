package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myapplication.adapter.ViewPagerAdapter;
import com.example.myapplication.model.Capture;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.journeyapps.barcodescanner.CaptureActivity;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationMain;
    ViewPager viewPagerMain;
    FloatingActionButton buttonCenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapped();
        event();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
            finishAffinity();
    }

    private void event() {
        bottomNavigationMain.setBackground(null);
        bottomNavigationMain.getMenu().getItem(2).setEnabled(false);

        setUpViewPager();

        bottomNavigationMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.menuHome:
                        viewPagerMain.setCurrentItem(0);
                        break;
                    case R.id.menuCalendar:
                        viewPagerMain.setCurrentItem(1);
                        break;
                    case R.id.menuNotification:
                        viewPagerMain.setCurrentItem(2);
                        break;
                    case R.id.menuAccount:
                        viewPagerMain.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });

        buttonCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanQrcode();
            }
        });
    }

    private void setUpViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPagerMain.setAdapter(adapter);

        viewPagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0 :
                        bottomNavigationMain.getMenu().findItem(R.id.menuHome).setChecked(true);
                        break;
                    case 1 :
                        bottomNavigationMain.getMenu().findItem(R.id.menuCalendar).setChecked(true);
                        break;
                    case 2 :
                        bottomNavigationMain.getMenu().findItem(R.id.menuNotification).setChecked(true);
                        break;
                    case 3 :
                        bottomNavigationMain.getMenu().findItem(R.id.menuAccount).setChecked(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void mapped() {
        bottomNavigationMain  = findViewById(R.id.bottomNavigationMain);
        viewPagerMain = findViewById(R.id.viewPager);
        buttonCenter = findViewById(R.id.buttonCenter);
    }

    public void scanQrcode(){
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.setBeepEnabled(true);
        intentIntegrator.setOrientationLocked(true);
        intentIntegrator.setCaptureActivity(Capture.class);
        intentIntegrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);
        if (result.getContents() != null) {
            try {
                String contents = result.getContents();
                JSONObject object = new JSONObject(contents);
                String id = object.getString("id");
                Intent intent = new Intent(MainActivity.this,InfoScanActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);
            } catch (Throwable t) {
                Toast.makeText(this, "QRCODE không hợp lệ", Toast.LENGTH_SHORT).show();
            }
        }
    }
}