package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.model.declare_medical.DeclareMedicalContent;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeclareMedicalContentActivity extends AppCompatActivity {

    EditText editTextName,editTextBirthday,editTextID,editTextNation,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextPhone,editTextEmail,
    editTextContent1,editTextContent2;
    TextInputLayout textInputLayout1,textInputLayout2;
    RadioGroup radioGroup1,radioGroup2,radioGroup3,radioGroup4,radioGroup5;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    String idDeclareMedical ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declare_medical_content);
        assign();
        event();
    }

    private void event() {
        Intent intent = getIntent();
        idDeclareMedical = intent.getStringExtra("idDeclareMedical");
        setData();
    }

    private void assign() {
        editTextName        = findViewById(R.id.editTextNameHistoryDM);
        editTextBirthday    = findViewById(R.id.editTextBirthdayHistoryDM);
        editTextID          = findViewById(R.id.editTextIDHistoryDM);
        editTextNation      = findViewById(R.id.editTextNationHistoryDM);
        editTextProvince    = findViewById(R.id.editTextCityHistoryDM);
        editTextDistrict    = findViewById(R.id.editTextDistricHistoryDM);
        editTextWard        = findViewById(R.id.editTextWardHistoryDM);
        editTextPlace       = findViewById(R.id.editTextPlaceHistoryDM);
        editTextPhone       = findViewById(R.id.editTextPhoneHistoryDM);
        editTextEmail       = findViewById(R.id.editTextEmailHistoryDM);
        editTextContent1    = findViewById(R.id.editTextContentHistoryDM1);
        editTextContent2    = findViewById(R.id.editTextContentHistoryDM2);
        textInputLayout1    = findViewById(R.id.textInputLayoutConentHistoryDM1);
        textInputLayout2    = findViewById(R.id.textInputLayoutConentHistoryDM2);
        radioGroup1         = findViewById(R.id.radiogroupHistoryDM1);
        radioGroup2         = findViewById(R.id.radiogroupHistoryDM2);
        radioGroup3         = findViewById(R.id.radiogroupHistoryDM3);
        radioGroup4         = findViewById(R.id.radiogroupHistoryDM4);
        radioGroup5         = findViewById(R.id.radiogroupHistoryDM5);
        radioButtonMale     = findViewById(R.id.radioSexMaleHistoryDM);
        radioButtonFemale   = findViewById(R.id.radioSexFeMaleHistoryDM);
        radioButtonOther    = findViewById(R.id.radioOtherMaleHistoryDM);
    }

    public void setData(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<Patients> callback = data.getInfoAccount(new Session(getApplicationContext()).getID());
                callback.enqueue(new Callback<Patients>() {
                    @Override
                    public void onResponse(Call<Patients> call, Response<Patients> response) {
                        Patients patients = response.body();
                        editTextName.setText(patients.getName());
                        editTextBirthday.setText(patients.getBirthday());
                        editTextID.setText(patients.getIdcode());
                        editTextPhone.setText(patients.getPhone());
                        editTextEmail.setText(patients.getEmail());
                        if (patients.getSex().compareTo("2") ==0){
                            radioButtonFemale.setChecked(true);
                        }
                        if (patients.getSex().compareTo("3") ==0){
                            radioButtonOther.setChecked(true);
                        }
                    }

                    @Override
                    public void onFailure(Call<Patients> call, Throwable t) {

                    }
                });
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                DataClient data = APIUltils.getData();
                Call<DeclareMedical> callback = data.getDeclareMedicalContent(idDeclareMedical);
                callback.enqueue(new Callback<DeclareMedical>() {
                    @Override
                    public void onResponse(Call<DeclareMedical> call, Response<DeclareMedical> response) {
                        DeclareMedical declareMedical = response.body();
                        setDeclareMedicalContent(declareMedical);
                    }

                    @Override
                    public void onFailure(Call<DeclareMedical> call, Throwable t) {

                    }
                });
            }
        }).start();
    }

    public void setDeclareMedicalContent(DeclareMedical medical){
        editTextProvince.setText(medical.getAddress().split(",")[0]);
        editTextDistrict.setText(medical.getAddress().split(",")[1]);
        editTextWard.setText(medical.getAddress().split(",")[2]);
        editTextPlace.setText(medical.getAddress().split(",")[3]);

        Log.d("AAA",medical.getAddress().split(",")[0]);

        String answer1 = medical.getList().get(0).getAnswer();
        RadioButton radioButton1 = radioGroup1.findViewWithTag(answer1);
        radioButton1.setChecked(true);
        if (answer1.compareTo("1") == 0){
            textInputLayout1.setVisibility(View.VISIBLE);
            editTextContent1.setText(medical.getList().get(0).getAnswerContent());
        }

        String answer2 = medical.getList().get(1).getAnswer();
        RadioButton radioButton2 = radioGroup2.findViewWithTag(answer2);
        radioButton2.setChecked(true);
        if (answer2.compareTo("1") == 0){
            textInputLayout2.setVisibility(View.VISIBLE);
            editTextContent2.setText(medical.getList().get(1).getAnswerContent());
        }

        String answer3 = medical.getList().get(2).getAnswer();
        RadioButton radioButton3 = radioGroup3.findViewWithTag(answer3);
        radioButton3.setChecked(true);

        String answer4 = medical.getList().get(3).getAnswer();
        RadioButton radioButton4 = radioGroup4.findViewWithTag(answer4);
        radioButton4.setChecked(true);

        String answer5 = medical.getList().get(4).getAnswer();
        RadioButton radioButton5 = radioGroup5.findViewWithTag(answer5);
        radioButton5.setChecked(true);
    }
}