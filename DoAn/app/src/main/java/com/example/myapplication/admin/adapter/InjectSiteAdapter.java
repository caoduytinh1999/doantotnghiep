package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.Site;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class InjectSiteAdapter extends RecyclerView.Adapter<InjectSiteAdapter.ViewHolder> {
    List<Site> list;
    IOnClickItemListener listener;


    public InjectSiteAdapter(List<Site> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    public InjectSiteAdapter(){}

    @NonNull
    @NotNull
    @Override
    public InjectSiteAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_vaccine,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        String name = list.get(position).getName();
        holder.textView.setText(name);
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textViewRowVaccine);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
