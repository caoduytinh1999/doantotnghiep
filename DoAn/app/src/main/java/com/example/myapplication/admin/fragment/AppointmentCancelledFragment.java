package com.example.myapplication.admin.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.admin.activity.AppointmentActivity;
import com.example.myapplication.admin.activity.AppointmentDetailActivity;
import com.example.myapplication.admin.adapter.HealthCheckAdminAdapter;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AppointmentCancelledFragment extends Fragment {
    private static final String STATUS_CANCELLED = "2";
    RecyclerView recyclerView;
    List<HealthCheckAdmin> list = new ArrayList<>();
    HealthCheckAdminAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_appointment_cancelled, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new HealthCheckAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                changeActivity((HealthCheckAdmin) onClick);
            }
        });
        setData();
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListACancelled);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<HealthCheckAdmin>> callback = data.getListHealthCheckAdmin(new Session(getContext()).getID(),STATUS_CANCELLED);
        callback.enqueue(new Callback<List<HealthCheckAdmin>>() {
            @Override
            public void onResponse(Call<List<HealthCheckAdmin>> call, Response<List<HealthCheckAdmin>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<HealthCheckAdmin>> call, Throwable t) {

            }
        });
    }

    public void changeActivity(HealthCheckAdmin healthCheck){
        Intent intent = new Intent(getContext(), AppointmentDetailActivity.class);
        intent.putExtra("object",healthCheck);
        startActivity(intent);
    }
}