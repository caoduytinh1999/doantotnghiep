package com.example.myapplication.admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.RegisterTestAdapter;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class RegisterTestAdminAdapter extends RecyclerView.Adapter<RegisterTestAdminAdapter.ViewHolder>{
    List<RegisterTestAdmin> list;
    IOnClickItemListener listener;

    public RegisterTestAdminAdapter(List<RegisterTestAdmin> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RegisterTestAdminAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_register_test,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new RegisterTestAdminAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RegisterTestAdminAdapter.ViewHolder holder, int position) {
        RegisterTestAdmin test = list.get(position);

        holder.textViewName.setText(test.getPatients().getName());

        holder.textViewAddress.setText(test.getTestingSite().toString());

        String day = test.getTestingDate().split("/")[0].trim();
        if (day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);
        String month = test.getTestingDate().split("/")[1].trim();
        if (month.length() == 1) month = "0" + month;
        String year = test.getTestingDate().split("/")[2].trim();
        holder.textViewMY.setText(month + "/" + year);

        String status = test.getStatus();
        if (status.compareTo("0") ==0){
            status = "Chưa Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.theme_main));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status));
        }
        if (status.compareTo("1") == 0){
            status = "Đã Xác Nhận";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.confirmed));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_confirmed));
        }
        if (status.compareTo("2") == 0){
            status = "Đã Hủy";
            holder.textViewStatus.setTextColor(ContextCompat.getColor(holder.textViewDay.getContext(),R.color.red));
            holder.textViewStatus.setBackground(ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_status_cancelled));
        }
        holder.textViewStatus.setText(status);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewName,textViewAddress,textViewStatus;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewHistoryDayRT);
            textViewMY = itemView.findViewById(R.id.textViewHistoryMYRT);
            textViewName = itemView.findViewById(R.id.textViewHistoryNameRT);
            textViewAddress = itemView.findViewById(R.id.textViewHistoryAddressRT);
            textViewStatus = itemView.findViewById(R.id.textViewStatusHistoryRT);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
