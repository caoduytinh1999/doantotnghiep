package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateInfoAccountActivity extends AppCompatActivity {

    EditText editTextName,editTextBitrthday,editTextPhone,editTextID,editTextEmail,editTextProvince,editTextDistrict,editTextWard,editTextPlace,
    editTextBHYT,editTextDT,editTextJob;
    ImageView imageView;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    Button button;
    RadioGroup radioGroup;
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    String provinvce_code = "";
    String district_code = "";
    String ward_code = "";
    List<ObjectAU> auList = new ArrayList<>();
    Patients patients = null;
    Calendar calendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info_account);
        assign();
        event();
    }

    private void event() {
        setData();

        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provinvce_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn tỉnh/thành phố");
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,provinvce_code,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provinvce_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else{
                    createBottomSheet(CODE_NAME_WARD,district_code,"Chọn Phường/Xã");
                }
            }
        });

        editTextBitrthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkValidation()){
                    updateInfoAccount();
                }
            }
        });
    }

    public void updateInfoAccount(){

        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        String name = editTextName.getText().toString();
        String birthday = editTextBitrthday.getText().toString();
        int id = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(id);
        String sex = radioButton.getTag().toString();
        String phone = editTextPhone.getText().toString();
        String IDCode = editTextID.getText().toString();
        String email = editTextEmail.getText().toString();
        String province = editTextProvince.getText().toString();
        String district = editTextDistrict.getText().toString();
        String ward = editTextWard.getText().toString();
        String place = editTextPlace.getText().toString();
        String address = place + "," + ward + "," + district + "," + province;
        String bhyt = editTextBHYT.getText().toString();
        String job = editTextJob.getText().toString();
        Patients patients = new Patients(new Session(this).getID(),phone,name,birthday,sex,address,job,IDCode,email,bhyt);
        String object = new Gson().toJson(patients);
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateInfoAccount(object);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(UpdateInfoAccountActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                Session session = new Session(getApplicationContext());
                session.setName(name);
                dialog.dismissDialog();
                showToastSuccess("Cập nhât thông tin thành công");
                finish();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(UpdateInfoAccountActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    private void assign() {
        button                  = findViewById(R.id.buttonUpdateInfoAccount);
        editTextName            = findViewById(R.id.editTextNameUIA);
        editTextBitrthday       = findViewById(R.id.editTextBirthdatUIA);
        editTextPhone           = findViewById(R.id.editTextPhoneUIA);
        editTextID              = findViewById(R.id.editTextIDUIA);
        editTextEmail           = findViewById(R.id.editTextEmailUIA);
        editTextProvince        = findViewById(R.id.editTextProvinceUIA);
        editTextDistrict        = findViewById(R.id.editTextDistrictUIA);
        editTextWard            = findViewById(R.id.editTextWardUIA);
        editTextPlace           = findViewById(R.id.editTextPlaceUIA);
        editTextBHYT            = findViewById(R.id.editTextHealthInsuranceUIA);
        editTextJob             = findViewById(R.id.editTextJobUIA);
        imageView               = findViewById(R.id.imageViewInfoAccount);
        radioButtonFemale       = findViewById(R.id.radioSexFeMaleUIA);
        radioButtonMale       = findViewById(R.id.radioSexMaleUIA);
        radioButtonOther       = findViewById(R.id.radioSexOtherUIA);
        radioGroup              = findViewById(R.id.radio);
    }

    public void createBottomSheet(String codename,String code,String title){
        auList.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(auList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (provinvce_code.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    provinvce_code = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (district_code.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    district_code = objectAU.getCode();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    ward_code = objectAU.getCode();
                }
            }
        },title,codename,code);
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void showDatePicker(){
        String birthday = patients.getBirthday();
        int year = Integer.parseInt(birthday.split("/")[2]);
        int month = Integer.parseInt(birthday.split("/")[1]);
        int day = Integer.parseInt(birthday.split("/")[0]);

        calendar.add(year,-1);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + " - " + (month < 10 ? "0" + month : month) + " - " + year;
                editTextBitrthday.setText(date);
            }
        }, year, month, day);
        dialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<Patients> callback = data.getInfoAccount(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<Patients>() {
            @Override
            public void onResponse(Call<Patients> call, Response<Patients> response) {
                patients = response.body();
                editTextName.setText(patients.getName());
                editTextBitrthday.setText(patients.getBirthday());
                String sex = patients.getSex();
                if (sex.compareTo("2") ==0) radioButtonFemale.setChecked(true);
                if (sex.compareTo("3") ==0) radioButtonOther.setChecked(true);
                editTextPhone.setText(patients.getPhone());
                editTextID.setText(patients.getIdcode());
                editTextEmail.setText(patients.getEmail());
                if (patients.getAddress().compareTo("") != 0){
                    String province = patients.getAddress().split(",")[3];
                    String district = patients.getAddress().split(",")[2];
                    String ward = patients.getAddress().split(",")[1];
                    String place = patients.getAddress().split(",")[0];
                    editTextProvince.setText(province);
                    editTextDistrict.setText(district);
                    editTextWard.setText(ward);
                    editTextPlace.setText(place);
                }
                editTextBHYT.setText(patients.getHealthInsurance());
                editTextJob.setText(patients.getJob());
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<Patients> call, Throwable t) {

            }
        });
    }

    public boolean checkValidation(){
        if (editTextName.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập tên người dùng");
            return false;
        }
        if (editTextID.getText().toString().isEmpty()){
            showToastWarning("Vui lòng nhập CMND/CCCD");
            return false;
        }
        if (editTextProvince.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
            return false;
        }
        if (editTextDistrict.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn quận/huyện");
            return false;
        }
        if (editTextWard.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn phường/xã");
            return false;
        }
        if (editTextJob.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn nghề nghiệp");
            return false;
        }
        return true;
    }
}