package com.example.myapplication.retrofit;

public class APIUltils {
    public static final String base_url = "https://duytinh2021.000webhostapp.com/";
    public static final String base_url_administrative_units = "https://provinces.open-api.vn/api/";

    public static DataClient getData(){
        return RetrofitClient.getClient(base_url).create(DataClient.class);
    }

    public static DataClient getAdministrativeUnits(){
        return RetrofitClient.getClient(base_url_administrative_units).create(DataClient.class);
    }
}
