package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.vaccine_record.VaccineRecord;
import com.example.myapplication.model.vaccine_record.VaccineRecordDetail;
import com.example.myapplication.my_interface.IOnClickItemListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VaccineInfoAdapter extends RecyclerView.Adapter<VaccineInfoAdapter.ViewHolder>{
    List<VaccineRecordDetail> list;
    IOnClickItemListener listener;

    public VaccineInfoAdapter(List<VaccineRecordDetail> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inject_vaccine,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        VaccineRecordDetail vaccine = list.get(position);

        String day1 = vaccine.getDate().split(" ")[1].split("/")[0];
        if (day1.length() == 1) day1 = "0" + day1;
        holder.textViewDay.setText(day1);

        String month1 = vaccine.getDate().split(" ")[1].split("/")[1];
        String year1 = vaccine.getDate().split(" ")[1].split("/")[2];
        if (month1.length() == 1) month1 = "0" + month1;
        holder.textViewMY.setText(month1 + "/" + year1);

        String time = vaccine.getDate().split(" ")[0];
        holder.textViewTime.setText(time);

        holder.textViewVaccine.setText(vaccine.getVaccineName());

        holder.textViewName.setText(new Session(holder.textViewDay.getContext()).getName());

        String noi = vaccine.getVaccineNumber();
        if (noi.compareTo("1") ==0) holder.textViewNoi.setBackground((ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_noi_1)));
        else holder.textViewNoi.setBackground((ContextCompat.getDrawable(holder.textViewDay.getContext(),R.drawable.custom_bg_noi_2)));
        holder.textViewNoi.setText(noi);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName,textViewDay,textViewMY,textViewTime,textViewVaccine,textViewNoi;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewNameRIR);
            textViewVaccine = itemView.findViewById(R.id.textViewVaccine);
            textViewNoi = itemView.findViewById(R.id.textViewNumberOfInjectRIR);
            textViewDay = itemView.findViewById(R.id.textViewDayRIR);
            textViewMY = itemView.findViewById(R.id.textViewMonthYearRIR);
            textViewTime = itemView.findViewById(R.id.textViewTimeRIR);
        }
    }
}
