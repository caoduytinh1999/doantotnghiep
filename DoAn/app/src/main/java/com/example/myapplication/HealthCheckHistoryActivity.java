package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.adapter.HealthCheckAdapter;
import com.example.myapplication.admin.activity.AppointmentActivity;
import com.example.myapplication.fragment.HealthCheckHistoryDetailFragment;
import com.example.myapplication.fragment.SelectBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheck;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthCheckHistoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<HealthCheck> healthChecks = new ArrayList<>();
    HealthCheckAdapter adapter;
    HealthCheck healthCheck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_check_history);
        assign();
        event();
    }

    private void event() {
        adapter = new HealthCheckAdapter(healthChecks, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                healthCheck = (HealthCheck) onClick;
                createBottomSheet();
            }
        });
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        setData();
    }

    private void assign() {
        recyclerView = findViewById(R.id.recyclerViewListHealthCheckHistory);
    }

    public void setData(){
        healthChecks.clear();
        LoadingDialog dialog  = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<HealthCheck>> callback = data.getListHealthCheck(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<List<HealthCheck>>() {
            @Override
            public void onResponse(Call<List<HealthCheck>> call, Response<List<HealthCheck>> response) {
                healthChecks.addAll(response.body());
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<HealthCheck>> call, Throwable t) {

            }
        });
    }

    public void replaceFragment(HealthCheck healthCheck){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",healthCheck);
        HealthCheckHistoryDetailFragment fragment = new HealthCheckHistoryDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
        fragmentTransaction.replace(R.id.layoutHealthCheckHistory,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void createBottomSheet(){
        SelectBottomSheetFragment fragment = new SelectBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String code = objectAU.getCode();
                if(code.compareTo("1") == 0){
                    replaceFragment(healthCheck);
                }
                else{
                    if (healthCheck.getStatus().compareTo("0") != 0){
                        createPopup();
                    }
                    else{
                        createPopup("Bạn có chắc chắn muốn hủy phiếu đăng kí khám này không ? ");
                    }
                }
            }
        });
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void createPopup(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_regiser_success);

        Button buttonYes = dialog.findViewById(R.id.buttonClose);
        TextView textView = dialog.findViewById(R.id.textViewTitle);
        TextView textView1 = dialog.findViewById(R.id.textViewContent);

        textView.setText("Thông báo");
        textView1.setText("Không thể hủy phiếu đăng kí khám đã xác nhận");
        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void createPopup(String message){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateHealthCheck(healthCheck.getId(),"2");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void updateHealthCheck(String id,String status){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateHealthCheckAdmin("00000",id,status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
                showToastSuccess("Hủy phiếu đăng kí khám thành công");
                setData();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismissDialog();
            }
        });
    }

    public void showToastSuccess(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_success,findViewById(R.id.layoutToastSuccess));
        TextView mes = view.findViewById(R.id.textViewContentSuccess);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }
}