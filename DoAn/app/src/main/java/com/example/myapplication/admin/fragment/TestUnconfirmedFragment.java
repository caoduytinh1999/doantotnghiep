package com.example.myapplication.admin.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.admin.adapter.RegisterInjectAdminAdapter;
import com.example.myapplication.admin.adapter.RegisterTestAdminAdapter;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.admin.model.RegisterTestAdmin;
import com.example.myapplication.fragment.HealthCheckHistoryDetailFragment;
import com.example.myapplication.fragment.SelectionBottomSheetFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_check.HealthCheckAdmin;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestUnconfirmedFragment extends Fragment {

    private static String UNCONFIRMED_STATUS = "0";
    RecyclerView recyclerView;
    RegisterTestAdminAdapter adapter;
    List<RegisterTestAdmin> list = new ArrayList<>();
    RegisterTestAdmin test;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_unconfirmed_test, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    private void event(View view) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new RegisterTestAdminAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                test = (RegisterTestAdmin) onClick;
                createBottomSheet(test);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListTestUnconfirmed);
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<RegisterTestAdmin>> callback = data.getListRegisterTestAdmin(new Session(getContext()).getID(),UNCONFIRMED_STATUS);
        callback.enqueue(new Callback<List<RegisterTestAdmin>>() {
            @Override
            public void onResponse(Call<List<RegisterTestAdmin>> call, Response<List<RegisterTestAdmin>> response) {
                list.addAll(response.body());
                Collections.reverse(list);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<RegisterTestAdmin>> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối với hệ thống", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void createBottomSheet(RegisterTestAdmin test){
        SelectionBottomSheetFragment sheetFragment = new SelectionBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                String status = objectAU.getCode();
                String mes = "";
                SpannableString ss;
                String id = test.getId();
                switch (status){
                    case "0":
                        replaceFragment();
                        break;
                    case "1" :
                        mes = "Bạn chắc chắn có muốn xác nhận lịch xét nghiệm của " + test.getPatients().getName() + " vào lúc " + test.getTestingDate() + " không ? ";
                        ss = new SpannableString(mes);
                        ss.setSpan(new StyleSpan(Typeface.BOLD),22,30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        createPopup(ss,id,status);
                        break;
                    case "2" :
                        mes = "Bạn chắc chắn có muốn xóa lịch xét nghiệm của " + test.getPatients().getName() + " vào lúc " + test.getTestingDate() + " không ? ";
                        ss = new SpannableString(mes);
                        ss.setSpan(new StyleSpan(Typeface.BOLD),22,25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        createPopup(ss,id,status);
                        break;
                }
            }
        });
        sheetFragment.show(getActivity().getSupportFragmentManager(),sheetFragment.getTag());
    }

    public void createPopup(SpannableString message,String id,String status){
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_selection);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopup);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopup);
        TextView textView = dialog.findViewById(R.id.textViewMessagePopup);
        textView.setText(message);

        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                updateRegisterTest(id,status);
            }
        });

        dialog.show();
    }

    public void updateRegisterTest(String id,String status){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<String> callback = data.updateRegisterTest(id,new Session(getContext()).getID(),status);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                list.remove(test);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi", Toast.LENGTH_SHORT).show();
                dialog.dismissDialog();
            }
        });
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",test);
        RegisterTestDetailFragment fragment = new RegisterTestDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutAdminTest,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}