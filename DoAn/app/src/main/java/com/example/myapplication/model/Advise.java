package com.example.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;

public class Advise extends OnClick implements Parcelable {
    private String title;
    private String url;
    private float timeStart;
    private float timeEnd;
    private String content;

    public Advise(String title, String url, float timeStart, float timeEnd, String content) {
        this.title = title;
        this.url = url;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.content = content;
    }

    protected Advise(Parcel in) {
        title = in.readString();
        url = in.readString();
        timeStart = in.readFloat();
        timeEnd = in.readFloat();
        content = in.readString();
    }

    public static final Creator<Advise> CREATOR = new Creator<Advise>() {
        @Override
        public Advise createFromParcel(Parcel in) {
            return new Advise(in);
        }

        @Override
        public Advise[] newArray(int size) {
            return new Advise[size];
        }
    };

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public float getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(float timeStart) {
        this.timeStart = timeStart;
    }

    public float getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(float timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(url);
        dest.writeFloat(timeStart);
        dest.writeFloat(timeEnd);
        dest.writeString(content);
    }
}
