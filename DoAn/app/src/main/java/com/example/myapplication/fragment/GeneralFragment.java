package com.example.myapplication.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.HealthRecordActivity;
import com.example.myapplication.HealthRecordDetailActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.health_quotient.Health;
import com.example.myapplication.model.health_record.HealthRecord;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralFragment extends Fragment {

    CardView cardViewPS,cardViewSPO2,cardViewBP,cardViewHB,cardViewT,cardViewH,cardViewW,cardViewBMI;
    TextView textViewPS,textViewTimePS,textViewSPO2,textViewTimeSPO2,textViewPB,textViewTimePB,textViewHB,textViewTimeHB,textViewT,textViewTimeT,
    textViewH,textViewTimeH,textViewW,textViewTimeW,textViewBMI,textViewTimeBMI;
    Button buttonAdd,buttonSee;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_general, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        HealthRecordDetailActivity.CURRENT_PAGE = 0;
//        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.addToBackStack("GeneralFragment");
//        fragmentTransaction.commit();
        setData();
    }

    public void setData(){
        LoadingDialog dialog = new LoadingDialog(getActivity());
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<HealthRecord> callback = data.getInfoHealthRecord(new Session(getContext()).getID());
        callback.enqueue(new Callback<HealthRecord>() {
            @Override
            public void onResponse(Call<HealthRecord> call, Response<HealthRecord> response) {
                HealthRecord record = response.body();
                if(record.getBloodSugarValue() != "" && record.getBloodPressureValue() != null){
                    textViewPS.setText(record.getBloodSugarValue());
                    textViewTimePS.setText("Đo lúc : \n" + record.getBloodSugarTime());
                }

                if (record.getBloodPressureValue() != "" && record.getBloodPressureValue() != null){
                    textViewPB.setText(record.getBloodPressureValue());
                    textViewTimePB.setText("Đo lúc : \n" + record.getBloodPressureTime());
                }

                if (!record.getSpo2Value().isEmpty()){
                    textViewSPO2.setText(record.getSpo2Value() + " %");
                    textViewTimeSPO2.setText("Đo lúc : \n" + record.getSpo2Time());
                }

                if (record.getHealthBeatValue() != ""){
                    textViewHB.setText(record.getHealthBeatValue());
                    textViewTimeHB.setText("Đo lúc : \n" + record.getHealthBeatTime());
                }

                if (!record.getTemperatureValue().isEmpty()){
                    textViewT.setText(record.getTemperatureValue());
                    textViewTimeT.setText("Đo lúc : \n" + record.getTemperatureTime());
                }

                if (record.getHeightValue() != ""){
                    textViewH.setText(record.getHeightValue());
                    textViewTimeH.setText("Đo lúc : \n" + record.getHeightTime());
                }

                if (record.getWeightValue() != ""){
                    Log.d("AAA",  "hihih :" + record.getWeightValue());
                    textViewW.setText(record.getWeightValue());
                    textViewTimeW.setText(record.getWeightTime());
                }

                if (record.getBmiValue() != ""){
                    textViewBMI.setText(record.getBmiValue());
                    textViewTimeBMI.setText(record.getBmiTime());
                }
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<HealthRecord> call, Throwable t) {

            }
        });
    }

    private void event(View view) {
        //setData();
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment2();
            }
        });

        buttonSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment3();
            }
        });

        cardViewPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BLOODSUGAR");
            }
        });

        cardViewSPO2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("SPO2");
            }
        });

        cardViewBP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BLOODPRESSURE");
            }
        });

        cardViewHB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("HEARTBEAT");
            }
        });

        cardViewT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("TEMPERATURE");
            }
        });

        cardViewH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("HEIGHT");
            }
        });

        cardViewW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("WEIGHT");
            }
        });

        cardViewBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment("BMI");
            }
        });

    }

    public void replaceFragment(String title){
        HealthChartFragment fragment = new HealthChartFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack("HealthChartFragment");
        fragmentTransaction.commit();
    }

    private void replaceFragment2(){
        AddAllValueHRFragment fragment = new AddAllValueHRFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack("HealthChartFragment");
        fragmentTransaction.commit();
    }

    private void replaceFragment3(){
        GeneralFilterFragment fragment = new GeneralFilterFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthRecord,fragment);
        fragmentTransaction.addToBackStack("HealthChartFragment");
        fragmentTransaction.commit();
    }

    private void assign(View view) {
        buttonSee = view.findViewById(R.id.buttonSee);
        buttonAdd = view.findViewById(R.id.buttonAdd);

        cardViewPS = view.findViewById(R.id.cardViewBS);
        textViewPS = view.findViewById(R.id.textViewBloodSugar);
        textViewTimePS = view.findViewById(R.id.textViewTimeBloodSugar);

        cardViewSPO2 = view.findViewById(R.id.cardViewSPO2);
        textViewSPO2 = view.findViewById(R.id.textViewSPO2);
        textViewTimeSPO2 = view.findViewById(R.id.textViewTimeSPO2);

        cardViewBP = view.findViewById(R.id.cardViewBP);
        textViewPB = view.findViewById(R.id.textViewBloodPressure);
        textViewTimePB = view.findViewById(R.id.textViewTimeBloodPressure);

        cardViewHB = view.findViewById(R.id.cardViewHB);
        textViewHB = view.findViewById(R.id.textViewHeartBeat);
        textViewTimeHB = view.findViewById(R.id.textViewTimeHeartBeat);

        cardViewT = view.findViewById(R.id.cardViewTempurature);
        textViewT = view.findViewById(R.id.textViewTempurature);
        textViewTimeT = view.findViewById(R.id.textViewTimeTempurature);

        cardViewH = view.findViewById(R.id.cardViewHeight);
        textViewH = view.findViewById(R.id.textViewHeight);
        textViewTimeH = view.findViewById(R.id.textViewTimeHeight);

        cardViewW = view.findViewById(R.id.cardViewWeight);
        textViewW = view.findViewById(R.id.textViewWeight);
        textViewTimeW = view.findViewById(R.id.textViewTimeWeight);

        cardViewBMI = view.findViewById(R.id.cardViewBMI);
        textViewBMI = view.findViewById(R.id.textViewBMI);
        textViewTimeBMI = view.findViewById(R.id.textViewTimeBMI);
    }
}