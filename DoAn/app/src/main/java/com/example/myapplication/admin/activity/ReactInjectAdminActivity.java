package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.adapter.ReactInjectAdapter;
import com.example.myapplication.fragment.ReactInjectDetailFragment;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ReactInject;
import com.example.myapplication.model.Session;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReactInjectAdminActivity extends AppCompatActivity {

    List<ReactInject> list = new ArrayList<>();
    ReactInjectAdapter adapter;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_react_inject_admin);
        recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        adapter = new ReactInjectAdapter(list, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                replaceFragment((ReactInject) onClick);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    public void setData(){
        list.clear();
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        DataClient data = APIUltils.getData();
        Call<List<ReactInject>> callback = data.getListReactInjectAdmin(new Session(getApplicationContext()).getID());
        callback.enqueue(new Callback<List<ReactInject>>() {
            @Override
            public void onResponse(Call<List<ReactInject>> call, Response<List<ReactInject>> response) {
                list.addAll(response.body());
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<ReactInject>> call, Throwable t) {
                Toast.makeText(ReactInjectAdminActivity.this, "Lỗi hệ thống", Toast.LENGTH_SHORT).show();
                dialog.createDialog();
            }
        });
    }

    public void replaceFragment(ReactInject reactInject){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",reactInject);
        ReactInjectDetailFragment fragment = new ReactInjectDetailFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.add(R.id.layoutReactInjectAdmin,fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }
}