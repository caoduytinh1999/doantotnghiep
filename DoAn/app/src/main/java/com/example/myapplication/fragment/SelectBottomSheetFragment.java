package com.example.myapplication.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

public class SelectBottomSheetFragment extends BottomSheetDialogFragment {
    IOnClickItemListener listener;

    public SelectBottomSheetFragment(IOnClickItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(), R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_selection,null,false);
        bottomSheetDialog.setContentView(view);
        Button btnDetail = bottomSheetDialog.findViewById(R.id.buttonDetail);
        Button btnCancel = bottomSheetDialog.findViewById(R.id.buttonCancel);

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(new ObjectAU("1","detail"));
                bottomSheetDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(new ObjectAU("2","cancel"));
                bottomSheetDialog.dismiss();
            }
        });

        return bottomSheetDialog;
    }
}
