package com.example.myapplication.admin.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.admin.model.RegisterInjectAdmin;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.register_syringe.RegisterSyringe;

import org.jetbrains.annotations.NotNull;

public class RIDetailAdminFragment extends Fragment {

    ConstraintLayout layoutName,layoutBirthday,layoutSex,layoutID,layoutHI,layoutProvince,layoutDistrict,layoutWard,layoutPlace;
    TextView textViewTName,textViewName,textViewTBirthday,textViewBirthday,textViewTSex,textViewSex,textViewTID,textViewID,textViewTHI,
            textViewHI,textViewTProvince,textViewProvince,textViewTDistrict,textViewDistrict,textViewTWard,textViewWard,textViewTPlace,textViewPlace;
    ImageView imageViewName,imageViewBirthday,imageViewSex,imageViewID,imageViewHI,imageViewProvince,imageViewDistrict,imageViewWard,imageViewPlace;
    RegisterInjectAdmin syringe;
    Button button;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_i_detail_admin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event(view);
    }

    public void replaceFragment(){
        Bundle bundle = new Bundle();
        bundle.putParcelable("syringe",syringe);
        RI2AdminFragment fragment = new RI2AdminFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out);
        fragmentTransaction.replace(R.id.layoutRI,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void event(View view) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment();
            }
        });

        Bundle bundle = this.getArguments();
        syringe = bundle.getParcelable("syringe");
        Patients patients = syringe.getPatients();
        textViewName.setText(patients.getName());
        textViewTName.setText("Người tới tiêm");
        imageViewName.setImageResource(R.drawable.icon_user_small);

        textViewBirthday.setText(patients.getBirthday());
        textViewTBirthday.setText("Ngày tháng năm sinh");
        imageViewBirthday.setImageResource(R.drawable.icon_calendar_date);

        textViewSex.setText((patients.getSex().compareTo("1") ==0) ? "Name" : "Nữ");
        textViewTSex.setText("Giới tính");
        imageViewSex.setImageResource(R.drawable.icon_user_small);

        textViewID.setText(patients.getIdcode());
        textViewTID.setText("Số hộ chiếu/CMND/CCCD");
        imageViewID.setImageResource(R.drawable.icon_id);

        textViewHI.setText(patients.getHealthInsurance());
        textViewTHI.setText("Số thẻ BHYT");
        imageViewHI.setImageResource(R.drawable.icon_id);

        String province = patients.getAddress().split(",")[3];
        String district = patients.getAddress().split(",")[2];
        String ward = patients.getAddress().split(",")[1];
        String place = patients.getAddress().split(",")[0];

        textViewProvince.setText(province);
        textViewTProvince.setText("Tỉnh/Thành phố");
        imageViewProvince.setImageResource(R.drawable.icon_place);

        textViewDistrict.setText(district);
        textViewTDistrict.setText("Quận/Huyện");
        imageViewDistrict.setImageResource(R.drawable.icon_place);

        textViewWard.setText(ward);
        textViewTWard.setText("Phường/Xã");
        imageViewWard.setImageResource(R.drawable.icon_place);

        textViewPlace.setText(place);
        textViewTPlace.setText("Địa chỉ nơi ở");
        imageViewPlace.setImageResource(R.drawable.icon_place);

    }

    private void assign(View view) {
        button = view.findViewById(R.id.button);

        layoutName = view.findViewById(R.id.layoutNameDetailRI);
        layoutBirthday = view.findViewById(R.id.layoutBirthdayDetailRI);
        layoutSex = view.findViewById(R.id.layoutSexDetailRI);
        layoutID = view.findViewById(R.id.layoutIDDetailRI);
        layoutHI = view.findViewById(R.id.layoutHIDetailRI);
        layoutProvince = view.findViewById(R.id.layoutProvinceDetailRI);
        layoutDistrict = view.findViewById(R.id.layoutDistrictDetailRI);
        layoutWard = view.findViewById(R.id.layoutWardDetailRI);
        layoutPlace = view.findViewById(R.id.layoutPlaceDetailRI);

        textViewTName = layoutName.findViewById(R.id.textViewTitleRowRI);
        textViewName = layoutName.findViewById(R.id.textViewContentRowRI);
        imageViewName = layoutName.findViewById(R.id.imageViewRowRI);

        textViewTBirthday = layoutBirthday.findViewById(R.id.textViewTitleRowRI);
        textViewBirthday = layoutBirthday.findViewById(R.id.textViewContentRowRI);
        imageViewBirthday = layoutBirthday.findViewById(R.id.imageViewRowRI);

        textViewTSex = layoutSex.findViewById(R.id.textViewTitleRowRI);
        textViewSex = layoutSex.findViewById(R.id.textViewContentRowRI);
        imageViewSex = layoutSex.findViewById(R.id.imageViewRowRI);

        textViewTID = layoutID.findViewById(R.id.textViewTitleRowRI);
        textViewID = layoutID.findViewById(R.id.textViewContentRowRI);
        imageViewID = layoutID.findViewById(R.id.imageViewRowRI);

        textViewTHI = layoutHI.findViewById(R.id.textViewTitleRowRI);
        textViewHI = layoutHI.findViewById(R.id.textViewContentRowRI);
        imageViewHI = layoutHI.findViewById(R.id.imageViewRowRI);

        textViewTProvince = layoutProvince.findViewById(R.id.textViewTitleRowRI);
        textViewProvince = layoutProvince.findViewById(R.id.textViewContentRowRI);
        imageViewProvince = layoutProvince.findViewById(R.id.imageViewRowRI);

        textViewTDistrict = layoutDistrict.findViewById(R.id.textViewTitleRowRI);
        textViewDistrict = layoutDistrict.findViewById(R.id.textViewContentRowRI);
        imageViewDistrict = layoutDistrict.findViewById(R.id.imageViewRowRI);

        textViewTWard = layoutWard.findViewById(R.id.textViewTitleRowRI);
        textViewWard = layoutWard.findViewById(R.id.textViewContentRowRI);
        imageViewWard = layoutWard.findViewById(R.id.imageViewRowRI);

        textViewTPlace = layoutPlace.findViewById(R.id.textViewTitleRowRI);
        textViewPlace = layoutPlace.findViewById(R.id.textViewContentRowRI);
        imageViewPlace = layoutPlace.findViewById(R.id.imageViewRowRI);
    }
}