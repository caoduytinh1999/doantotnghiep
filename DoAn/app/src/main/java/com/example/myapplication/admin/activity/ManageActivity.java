package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.myapplication.LoginActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.Session;

public class ManageActivity extends AppCompatActivity {

    CardView cardView1,cardView2,cardView3,cardView4,cardView5;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        assign();
        event();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    private void event() {
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),AnalysisActivity.class));
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ReportActivity.class));
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HealthFacilityAdminActivity.class));
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ListStaffActivity.class));
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopupLogout();
            }
        });

    }

    private void assign() {
        cardView1 = findViewById(R.id.cardViewSyringeAdmin);
        cardView2 = findViewById(R.id.cardViewAppointmentAdmin);
        cardView3 = findViewById(R.id.cardViewTestAdmin);
        cardView4 = findViewById(R.id.cardViewStaff);
        cardView5 = findViewById(R.id.cardViewLogout);
    }

    public void createPopupLogout(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_confirm_logout);

        Button buttonNo = dialog.findViewById(R.id.buttonNoPopupLogout);
        Button buttonYes = dialog.findViewById(R.id.buttonYesPopupLogout);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void logout(){
        Session session = new Session(this);
        session.setToken("");
        session.setID("");
        startActivity(new Intent(this, LoginActivity.class));
    }
}