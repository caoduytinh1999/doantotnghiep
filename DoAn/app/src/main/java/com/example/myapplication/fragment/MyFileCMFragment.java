package com.example.myapplication.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.DeclareMedicalContentActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.DeclareMedicalAdapter;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.declare_medical.DeclareMedical;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFileCMFragment extends Fragment {

    RecyclerView recyclerView;
    TextView textView;
    List<DeclareMedical> list = new ArrayList<>();
    DeclareMedicalAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_file_c_m, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assign(view);
        event();
    }

    private void event() {
        setData();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
    }

    private void assign(View view) {
        recyclerView = view.findViewById(R.id.recyclerViewListDM);
        textView     = view.findViewById(R.id.textViewTotalDM);
    }

    public void setData(){
        DataClient data = APIUltils.getData();
        Call<List<DeclareMedical>> callback = data.getListDeclareMedical(new Session(getContext()).getID());
        callback.enqueue(new Callback<List<DeclareMedical>>() {
            @Override
            public void onResponse(Call<List<DeclareMedical>> call, Response<List<DeclareMedical>> response) {
                List<DeclareMedical> medicalList = response.body();
                list = medicalList;
                adapter = new DeclareMedicalAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {
                        DeclareMedical declareMedical = (DeclareMedical) onClick;
                        Log.d("AAA","id Trước : " + declareMedical.getId());
                        Intent intent = new Intent(getContext(), DeclareMedicalContentActivity.class);
                        intent.putExtra("idDeclareMedical",declareMedical.getId());
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                textView.setText("Danh sách tờ khai (" + list.size() + ")");
            }

            @Override
            public void onFailure(Call<List<DeclareMedical>> call, Throwable t) {

            }
        });
    }
}