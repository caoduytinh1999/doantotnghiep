package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.RegisterInjectionHistoryActivity;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.register_syringe.RegisterSyringe;
import com.example.myapplication.my_interface.IOnClickItemListener;

import java.util.List;

public class RegisterSyringeAdapter extends RecyclerView.Adapter<RegisterSyringeAdapter.ViewHolder>{
    List<RegisterSyringe> list;
    IOnClickItemListener listener;

    public RegisterSyringeAdapter(List<RegisterSyringe> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_history_register_injection,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RegisterSyringeAdapter.ViewHolder holder, int position) {
        RegisterSyringe syringe = list.get(position);
        holder.textViewName.setText(new Session(holder.textViewBirthday.getContext()).getName());
        String date = syringe.getDate();
        String day = date.split("/")[0];
        String month = date.split("/")[1];
        String year = date.split("/")[2];
        if (day.length() == 1) day = "0" + day;
        holder.textViewDay.setText(day);
        if (month.length() == 1) month = "0" + month;
        holder.textViewMY.setText(month + "/" + year);
        String status = syringe.getStatus();
        if (status.compareTo("0") == 0){
            status = "Chưa xác nhận";
        }
        else{
            if (status.compareTo("1") ==0){
                status = "Đã xác nhận";
            }
            else{
                status = "Đã hủy";
            }
        }
        holder.textViewBirthday.setText(RegisterInjectionHistoryActivity.birthday);
        holder.textViewStatus.setText(status);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textViewDay,textViewMY,textViewName,textViewBirthday,textViewStatus;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewDay = itemView.findViewById(R.id.textViewHistoryDayRI);
            textViewMY = itemView.findViewById(R.id.textViewHistoryMYRI);
            textViewName = itemView.findViewById(R.id.textViewHistoryNameRI);
            textViewBirthday = itemView.findViewById(R.id.textViewHistoryBirthdayRI);
            textViewStatus = itemView.findViewById(R.id.textViewStatusHistoryRI);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(list.get(getAdapterPosition()));
                }
            });
        }
    }
}
