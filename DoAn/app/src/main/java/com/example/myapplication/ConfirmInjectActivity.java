package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.admin.fragment.TestSiteBottomSheetFragment;
import com.example.myapplication.fragment.AUBottomSheetFragment;
import com.example.myapplication.model.Account.Patients;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.Session;
import com.example.myapplication.model.test.RegisterTest;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;
import com.google.gson.Gson;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmInjectActivity extends AppCompatActivity {

    EditText editTextName,editTextBirthday,editTextID,editTextNation,editTextProvince,editTextDistrict,editTextWard,editTextPlace,editTextPhone,editTextTestingDate,editTextTestingSite;
    RadioButton radioButtonMale,radioButtonFemale,radioButtonOther;
    Button buttonConfirm;
    public static String CODE_NAME_PROVINCE = "PROVINCE";
    public static String CODE_NAME_DISTRICT = "DISTRICT";
    public static String CODE_NAME_WARD = "WARD";
    List<ObjectAU> auList = new ArrayList<>();
    Calendar calendar = Calendar.getInstance();
    String name = "";
    String sex = "Nam";
    String birthday = "";
    String phone = "";
    String province_code = "";
    String district_code = "";
    String ward_code = "";
    String province = "";
    String district = "";
    String ward = "";
    String idFacility = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_inject);
        assign();
        event();
    }

    private void event() {
        setData();

        editTextProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheet(CODE_NAME_PROVINCE,"","Chọn Tỉnh/Thành Phố");
            }
        });

        editTextDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (province_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Tỉnh/Thành Phố");
                }
                else{
                    createBottomSheet(CODE_NAME_DISTRICT,province_code,"Chọn Quận/Huyện");
                }
            }
        });

        editTextWard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (district_code.compareTo("") == 0){
                    showToastWarning("Vui lòng chọn Quận/Huyện");
                }
                else {
                    createBottomSheet(CODE_NAME_WARD,district_code,"Chọn Phường/Xã");
                }
            }
        });

        editTextTestingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        editTextTestingSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBottomSheetTestSite();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidate()){
                    registerTest();
                }
            }
        });
    }

    public void createBottomSheetTestSite(){
        TestSiteBottomSheetFragment fragment = new TestSiteBottomSheetFragment(new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                HealthFacility facility = (HealthFacility) onClick;
                editTextTestingSite.setText(facility.getName());
                idFacility = facility.getId();
            }
        });
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void createBottomSheet(String codename,String code,String title){
        auList.clear();
        AUBottomSheetFragment fragment = new AUBottomSheetFragment(auList, new IOnClickItemListener() {
            @Override
            public void onClick(OnClick onClick) {
                ObjectAU objectAU = (ObjectAU) onClick;
                if (codename.compareTo(CODE_NAME_PROVINCE) == 0){
                    editTextProvince.setText(objectAU.getName());
                    if (province_code.compareTo(objectAU.getCode()) != 0){
                        editTextDistrict.setText("");
                        editTextWard.setText("");
                    }
                    province_code = objectAU.getCode();
                    province = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_DISTRICT) == 0){
                    editTextDistrict.setText(objectAU.getName());
                    if (district_code.compareTo(objectAU.getCode()) != 0){
                        editTextWard.setText("");
                    }
                    district_code = objectAU.getCode();
                    district = objectAU.getName();
                }
                if (codename.compareTo(CODE_NAME_WARD) == 0){
                    editTextWard.setText(objectAU.getName());
                    ward_code = objectAU.getCode();
                    ward = objectAU.getName();
                }
            }
        },title,codename,code);
        fragment.show(getSupportFragmentManager(),fragment.getTag());
    }

    public void showDatePicker(){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month++;
                String date = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "/" + (month < 10 ? "0" + month : month) + "/" + year;
                editTextTestingDate.setText(date);
            }
        }, year, month, day);
        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void showToastWarning(String content){
        Toast toast = new Toast(this);
        View view = getLayoutInflater().inflate(R.layout.layout_toast_notify_warning,findViewById(R.id.layoutToastWarning));
        TextView mes = view.findViewById(R.id.textViewContentWaring);
        mes.setText(content);
        toast.setView(view);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.TOP,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }


    private void assign() {
        editTextName        = findViewById(R.id.editTextNameTest);
        editTextBirthday    = findViewById(R.id.editTextBirthdayTest);
        editTextID          = findViewById(R.id.editTextIDTest);
        editTextNation      = findViewById(R.id.editTextNationTest);
        editTextProvince    = findViewById(R.id.editTextCityTest);
        editTextDistrict    = findViewById(R.id.editTextDistricTest);
        editTextWard        = findViewById(R.id.editTextWardTest);
        editTextPlace       = findViewById(R.id.editTextPlaceTest);
        editTextPhone       = findViewById(R.id.editTextPhoneTest);
        editTextTestingDate = findViewById(R.id.editTextTestingDate);
        editTextTestingSite = findViewById(R.id.editTextTestingSite);
        radioButtonMale     = findViewById(R.id.radioSexMaleTest);
        radioButtonFemale   = findViewById(R.id.radioSexFeMaleTest);
        radioButtonOther    = findViewById(R.id.radioOtherTest);
        buttonConfirm       = findViewById(R.id.buttonConfirmTest);
    }

    public void setData(){
        Session session = new Session(getApplicationContext());
        DataClient data = APIUltils.getData();
        Call<Patients> callback = data.getInfoAccount(session.getID());
        callback.enqueue(new Callback<Patients>() {
            @Override
            public void onResponse(Call<Patients> call, Response<Patients> response) {
                Patients patients = response.body();
                editTextName.setText(patients.getName());
                editTextBirthday.setText(patients.getBirthday());
                editTextID.setText(patients.getIdcode());
                editTextPhone.setText(patients.getPhone());
                if (patients.getSex().compareTo("2") ==0){
                    radioButtonFemale.setChecked(true);
                    sex = "Nữ";
                }
                if (patients.getSex().compareTo("3") ==0){
                    radioButtonOther.setChecked(true);
                    sex = "Khác";
                }
                name = patients.getName();
                birthday = patients.getBirthday();
                phone = patients.getPhone();

                String address = patients.getAddress();
                String province = address.split(",")[3];
                String district = address.split(",")[2];
                String ward = address.split(",")[1];
                String place = address.split(",")[0];

                editTextProvince.setText(province);
                editTextDistrict.setText(district);
                editTextWard.setText(ward);
                editTextPlace.setText(place);
            }

            @Override
            public void onFailure(Call<Patients> call, Throwable t) {
                Toast.makeText(ConfirmInjectActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkValidate(){
        if (editTextProvince.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn tỉnh/thành phố");
            return false;
        }

        if (editTextDistrict.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn quận/huyện");
            return false;
        }

        if (editTextWard.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn phường/xã");
            return false;
        }

        if (editTextPlace.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn nơi ở");
            return false;
        }

        if (editTextTestingDate.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn ngày xét nghiệm");
            return  false;
        }

        if (editTextTestingSite.getText().toString().isEmpty()){
            showToastWarning("Vui lòng chọn nơi xét nghiệm");
            return false;
        }

        return true;
    }

    public void registerTest(){
        String idPatients = new Session(this).getID();

        String idStaff = "00000";

        String testingDate = editTextTestingDate.getText().toString();

        long millis=System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        LocalDateTime dateTime = LocalDateTime.now();
        int hour = dateTime.getHour();;
        int minute = dateTime.getMinute();
        int second = dateTime.getSecond();
        String time = hour + ":" + minute + ":" + second + " " + date;

        String address = province + "," + district + "," + ward + "," + editTextPlace.getText().toString();

        String testingSite = editTextTestingSite.getText().toString();
        RegisterTest test = new RegisterTest("",idPatients,idStaff,testingDate,time,idFacility,address,"0");
        String object = new Gson().toJson(test);
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
         DataClient data = APIUltils.getData();
         Call<String> callback = data.registerTest(object);
         callback.enqueue(new Callback<String>() {
             @Override
             public void onResponse(Call<String> call, Response<String> response) {
                 String mes = response.body();
                 if (mes.compareTo("success") ==0){
                     showPopup();
                 }
                 else{
                     Toast.makeText(ConfirmInjectActivity.this, "Đăng ký xét nghiệm không thành công", Toast.LENGTH_SHORT).show();
                 }
                 dialog.dismissDialog();
             }

             @Override
             public void onFailure(Call<String> call, Throwable t) {
                 Toast.makeText(ConfirmInjectActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                 dialog.dismissDialog();
             }
         });
    }

    public void showPopup(){
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.setContentView(R.layout.popup_declare_medical_success);

        Button button = dialog.findViewById(R.id.buttonClosePopupDM);
        TextView textViewTitle,textViewMes,textViewName,textViewSex,textViewPhone,textViewBirthday;
        textViewTitle = dialog.findViewById(R.id.textViewTitlePopup);
        textViewMes = dialog.findViewById(R.id.textViewMessagePopup);
        textViewName = dialog.findViewById(R.id.textViewNamePopupDM);
        textViewSex = dialog.findViewById(R.id.textViewSexPopupDM);
        textViewBirthday = dialog.findViewById(R.id.textViewBirthdayPopupDM);
        textViewPhone = dialog.findViewById(R.id.textViewPhonePopupDM);

        textViewTitle.setText("Đăng ký xét nghiệm thành công");
        textViewMes.setText("Cảm ơn quí khách đã đăng ký xét nghiệm covid");
        textViewName.setText(name);
        textViewBirthday.setText(birthday);
        textViewSex.setText(sex);
        textViewPhone.setText(phone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}