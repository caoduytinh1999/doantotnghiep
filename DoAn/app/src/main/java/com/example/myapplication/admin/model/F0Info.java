package com.example.myapplication.admin.model;

import com.example.myapplication.model.Account.Patients;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class F0Info {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("patients")
    @Expose
    private Patients patients;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("date")
    @Expose
    private String date;

    public F0Info(String id, Patients patients, String address, String date) {
        this.id = id;
        this.patients = patients;
        this.address = address;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patients getPatients() {
        return patients;
    }

    public void setPatients(Patients patients) {
        this.patients = patients;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
