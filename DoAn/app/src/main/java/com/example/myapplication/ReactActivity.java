package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.example.myapplication.adapter.ViewPagerAdminAppointmentAdapter;
import com.example.myapplication.adapter.ViewPagerReactAdapter;
import com.google.android.material.tabs.TabLayout;

public class ReactActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_react);
        assign();
        event();
    }

    private void event() {
        ViewPagerReactAdapter adapter = new ViewPagerReactAdapter(getSupportFragmentManager(),  FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void assign() {
        tabLayout = findViewById(R.id.tabLayoutAppointmentAdmin);
        viewPager = findViewById(R.id.viewPagerAppointmentAdmin);
    }
}