 package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChangePassWordActivity extends AppCompatActivity {

    EditText editTextOlgPassWord,editTextNewPassWord,getEditTextNewPassWordRepeat;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_word);
        assign();
        event();
    }

    private void event() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });
    }

    private void assign() {
        editTextNewPassWord = findViewById(R.id.editTextNewPassWord);
        editTextOlgPassWord = findViewById(R.id.editTextOldPassWord);
        getEditTextNewPassWordRepeat = findViewById(R.id.editTextNewPassWordRepeat);
        button  = findViewById(R.id.buttonUpdatePassWord);
    }
}