package com.example.myapplication.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

public class ButtonBottomSheetFragment extends BottomSheetDialogFragment {
    IOnClickItemListener listener;

    public ButtonBottomSheetFragment(IOnClickItemListener listener) {
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext(), R.style.AppBottomSheetDialogTheme);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_bottom_sheet_button,null,false);
        bottomSheetDialog.setContentView(view);
        Button buttonConfirm = bottomSheetDialog.findViewById(R.id.buttonBSConfirm);
        Button buttonUpdate = bottomSheetDialog.findViewById(R.id.buttonBSUdate);

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(new ObjectAU("1","update"));
                bottomSheetDialog.dismiss();
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(new ObjectAU("2","confirm"));
                bottomSheetDialog.dismiss();
            }
        });

        return bottomSheetDialog;
    }
}
