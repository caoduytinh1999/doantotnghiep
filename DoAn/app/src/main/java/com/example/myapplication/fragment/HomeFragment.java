package com.example.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.AdviseActivity;
import com.example.myapplication.CertificationPreventCovidActivity;
import com.example.myapplication.ConfirmInjectActivity;
import com.example.myapplication.DeclareMedicalActivity;
import com.example.myapplication.HealthCodeActivity;
import com.example.myapplication.HealthFacilitiesActivity;
import com.example.myapplication.HealthRecordActivity;
import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.ReactActivity;
import com.example.myapplication.RegisterInjectionActivity;
import com.example.myapplication.model.Advise;
import com.example.myapplication.model.Session;

public class HomeFragment extends Fragment {

    TextView textViewName;
    LinearLayout linearLayout1,linearLayout2,linearLayout3,linearLayout4,linearLayout5,linearLayout6,linearLayout7,linearLayout8,linearLayout9;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home,container,false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapped(view);
        event();
    }

    private void event() {
        setData();

        linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), DeclareMedicalActivity.class));
            }
        });

        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CertificationPreventCovidActivity.class));
            }
        });

        linearLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AdviseActivity.class));
            }
        });

        linearLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),RegisterInjectionActivity.class));
            }
        });

        linearLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ReactActivity.class));
            }
        });

        linearLayout6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),ConfirmInjectActivity.class));
            }
        });

        linearLayout7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),HealthFacilitiesActivity.class));
            }
        });

        linearLayout8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),HealthRecordActivity.class));
            }
        });

        linearLayout9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),HealthCodeActivity.class));
            }
        });

    }
    
    public void setData(){
        Session session = new Session(getContext());
        textViewName.setText(session.getName());
    }

    private void mapped(View view) {
        textViewName     = view.findViewById(R.id.textViewNameHome);
        linearLayout1    = view.findViewById(R.id.linearLayoutHome1);
        linearLayout2    = view.findViewById(R.id.linearLayoutHome2);
        linearLayout3    = view.findViewById(R.id.linearLayoutHome3);
        linearLayout4    = view.findViewById(R.id.linearLayoutHome4);
        linearLayout5    = view.findViewById(R.id.linearLayoutHome5);
        linearLayout6    = view.findViewById(R.id.linearLayoutHome6);
        linearLayout7    = view.findViewById(R.id.linearLayoutHome7);
        linearLayout8    = view.findViewById(R.id.linearLayoutHome8);
        linearLayout9    = view.findViewById(R.id.linearLayoutHome9);
    }
}
