package com.example.myapplication.admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.HealthFacilityAdapter;
import com.example.myapplication.admin.fragment.InfoHFAdminFragment;
import com.example.myapplication.fragment.HealthFacilitiesInfoFragment;
import com.example.myapplication.model.HealthFacility;
import com.example.myapplication.model.LoadingDialog;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;
import com.example.myapplication.retrofit.APIUltils;
import com.example.myapplication.retrofit.DataClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthFacilityAdminActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView textView;
    ImageView imageView;
    HealthFacilityAdapter adapter;
    List<HealthFacility> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_facility_admin);
        assign();
    }

    @Override
    protected void onResume() {
        super.onResume();
        event();
    }

    private void event() {
        innitRecyclerView();
    }

    private void assign() {
        recyclerView = findViewById(R.id.recyclerViewListHealthFacilities);
        textView     = findViewById(R.id.textViewNumberOfHF);
        imageView    = findViewById(R.id.imageViewAdd);
    }

    private void innitRecyclerView(){

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(manager);
        assignData();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),AddHFActivity.class));
            }
        });
    }

    public void assignData(){
        LoadingDialog dialog = new LoadingDialog(this);
        dialog.createDialog();
        list.clear();
        DataClient data = APIUltils.getData();
        Call<List<HealthFacility>> callback = data.getListHealthFacility();
        callback.enqueue(new Callback<List<HealthFacility>>() {
            @Override
            public void onResponse(Call<List<HealthFacility>> call, Response<List<HealthFacility>> response) {
                list = response.body();
                textView.setText("Danh sách cơ sở y tế (" + list.size() + ")");
                adapter = new HealthFacilityAdapter(list, new IOnClickItemListener() {
                    @Override
                    public void onClick(OnClick onClick) {
                        HealthFacility facility = (HealthFacility) onClick;
                        replaceFragment(facility);
                    }
                });
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                dialog.dismissDialog();
            }

            @Override
            public void onFailure(Call<List<HealthFacility>> call, Throwable t) {

            }
        });
    }

    public void replaceFragment(HealthFacility facility){
        Bundle bundle = new Bundle();
        bundle.putParcelable("object",facility);
        InfoHFAdminFragment fragment = new InfoHFAdminFragment();
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right_in,R.anim.slide_left_out,0,R.anim.slide_right_out);
        fragmentTransaction.replace(R.id.layoutHealthFacilities,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}