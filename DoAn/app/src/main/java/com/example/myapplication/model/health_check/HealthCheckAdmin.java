package com.example.myapplication.model.health_check;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.my_interface.OnClick;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthCheckAdmin extends OnClick implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("namePatients")
    @Expose
    private String namePatients;

    @SerializedName("nameHealthFacility")
    @Expose
    private String nameHealthFacility;

    @SerializedName("service")
    @Expose
    private String service;

    @SerializedName("price")
    @Expose
    private float price;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("symptom")
    @Expose
    private String symptom;

    @SerializedName("status")
    @Expose
    private String status;

    public HealthCheckAdmin(String id, String namePatients, String nameHealthFacility, String service, float price, String time, String symptom, String status) {
        this.id = id;
        this.namePatients = namePatients;
        this.nameHealthFacility = nameHealthFacility;
        this.service = service;
        this.price = price;
        this.time = time;
        this.symptom = symptom;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamePatients() {
        return namePatients;
    }

    public void setNamePatients(String namePatients) {
        this.namePatients = namePatients;
    }

    public String getNameHealthFacility() {
        return nameHealthFacility;
    }

    public void setNameHealthFacility(String nameHealthFacility) {
        this.nameHealthFacility = nameHealthFacility;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    protected HealthCheckAdmin(Parcel in) {
        id = in.readString();
        namePatients = in.readString();
        nameHealthFacility = in.readString();
        service = in.readString();
        price = in.readFloat();
        time = in.readString();
        symptom = in.readString();
        status = in.readString();
    }

    public static final Creator<HealthCheckAdmin> CREATOR = new Creator<HealthCheckAdmin>() {
        @Override
        public HealthCheckAdmin createFromParcel(Parcel in) {
            return new HealthCheckAdmin(in);
        }

        @Override
        public HealthCheckAdmin[] newArray(int size) {
            return new HealthCheckAdmin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(namePatients);
        dest.writeString(nameHealthFacility);
        dest.writeString(service);
        dest.writeFloat(price);
        dest.writeString(time);
        dest.writeString(symptom);
        dest.writeString(status);
    }
}
