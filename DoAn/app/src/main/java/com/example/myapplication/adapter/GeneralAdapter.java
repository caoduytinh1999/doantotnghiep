package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.ObjectAU;
import com.example.myapplication.model.health_record.HealthRecord;
import com.example.myapplication.my_interface.IOnClickItemListener;
import com.example.myapplication.my_interface.OnClick;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class GeneralAdapter extends RecyclerView.Adapter<GeneralAdapter.ViewHolder>{
    List<HealthRecord> list;
    IOnClickItemListener listener;

    public GeneralAdapter(List<HealthRecord> list, IOnClickItemListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_general_filter,parent,false);
        Animation ani = AnimationUtils.loadAnimation(parent.getContext(),R.anim.animation_recycler);
        view.setAnimation(ani);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        HealthRecord record = list.get(position);
        holder.textView.setText("Lần đo : " + ++position);

        if (record.getSpo2Value() != null){
            holder.textViewSPO2.setText("Nhịp Tim : " + record.getSpo2Value() + " %");
            holder.textViewTimeSP02.setText("Đo lúc : " + record.getSpo2Time().split(" ")[0]);
        }
        else{
            holder.textViewSPO2.setText("SPO2 : ");
            holder.textViewTimeSP02.setText("Đo lúc :");
        }

        if (record.getBloodSugarValue() !=null){
            holder.textViewBloodSugar.setText("Đường Huyết : " + record.getBloodSugarValue() + " mg/DL");
            holder.textViewTimeBloodSugar.setText("Đo lúc : " + record.getBloodSugarTime().split(" ")[0]);
        }
        else{
            holder.textViewBloodSugar.setText("Đường Huyết :");
            holder.textViewTimeBloodSugar.setText("Đo lúc :");
        }

        if (record.getWeightValue() != null){
            holder.textViewWeight.setText("Cân Nặng : " + record.getWeightValue() + " kg");
            holder.textViewTimeWeight.setText("Đo lúc : " + record.getWeightTime().split(" ")[0]);
        }
        else {
            holder.textViewWeight.setText("Cân Nặng : ");
            holder.textViewTimeWeight.setText("Đo lúc : " );
        }

        if (record.getBmiValue() != null){
            holder.textViewBMI.setText("BMI : " + record.getBmiValue() + " kg/m2");
            holder.textViewTimeBMI.setText("Đo lúc : " + record.getBmiTime().split(" ")[0]);
        }
        else{
            holder.textViewBMI.setText("BMI : ");
            holder.textViewTimeBMI.setText("Đo lúc : ");
        }

        if (record.getTemperatureValue() !=null){
            holder.textViewTem.setText("Nhiệt Độ : " + record.getTemperatureValue() + " \u2103");
            holder.textViewTimeTem.setText("Đo lúc : " + record.getTemperatureTime().split(" ")[0]);
        }
        else{
            holder.textViewTem.setText("Nhiệt Độ : ");
            holder.textViewTimeTem.setText("Đo lúc : ");
        }

        if (record.getHeightValue() != null){
            holder.textViewHeight.setText("Chiều Cao : " + record.getHeightValue() + " cm");
            holder.textViewTimeHeight.setText("Đo lúc : " + record.getHeightTime().split(" ")[0]);
        }
        else{
            holder.textViewHeight.setText("Chiều Cao : ");
            holder.textViewTimeHeight.setText("Đo lúc : ");
        }

        if (record.getBloodPressureValue() != null){
            holder.textViewBloodPressure.setText("Huyết Áp : " + record.getBloodPressureValue() + " mmHG");
            holder.textViewTimeBloodPressure.setText("Đo lúc : " + record.getBloodPressureTime().split(" ")[0]);
        }
        else{
            holder.textViewBloodPressure.setText("Huyết Áp : ");
            holder.textViewTimeBloodPressure.setText("Đo lúc : ");
        }

        if (record.getHealthBeatValue() != null){
            holder.textViewHeartBeat.setText("SPO2 : " + record.getHealthBeatValue() + " nhịp/phút");
            holder.textViewTimeHearBeat.setText("Đo lúc : " + record.getHealthBeatTime().split(" ")[0]);
        }
        else{
            holder.textViewHeartBeat.setText("SPO2 : ");
            holder.textViewTimeHearBeat.setText("Đo lúc : ");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView,textViewBloodPressure,textViewTimeBloodPressure,textViewHeartBeat,textViewTimeHearBeat,textViewTem,
                textViewTimeTem,textViewHeight,textViewTimeHeight,textViewWeight,textViewTimeWeight,textViewBMI,textViewTimeBMI,textViewBloodSugar,
        textViewTimeBloodSugar,textViewSPO2,textViewTimeSP02;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            textViewBloodPressure = itemView.findViewById(R.id.textViewBloodPressure);
            textViewTimeBloodPressure = itemView.findViewById(R.id.textViewTimeBloodPressure);
            textViewHeartBeat = itemView.findViewById(R.id.textViewHeartBeat);
            textViewTimeHearBeat = itemView.findViewById(R.id.textViewTimeHeartBeat);
            textViewTem = itemView.findViewById(R.id.textViewTempurature);
            textViewTimeTem = itemView.findViewById(R.id.textViewTimeTempurature);
            textViewHeight = itemView.findViewById(R.id.textViewHeight);
            textViewTimeHeight = itemView.findViewById(R.id.textViewTimeHeight);
            textViewWeight = itemView.findViewById(R.id.textViewWeight);
            textViewTimeWeight = itemView.findViewById(R.id.textViewTimeWeight);
            textViewBMI = itemView.findViewById(R.id.textViewBMI);
            textViewTimeBMI = itemView.findViewById(R.id.textViewTimeBMI);
            textViewBloodSugar = itemView.findViewById(R.id.textViewBloodSugar);
            textViewTimeBloodSugar= itemView.findViewById(R.id.textViewTimeBloodSugar);
            textViewSPO2 = itemView.findViewById(R.id.textViewSPO2);
            textViewTimeSP02 = itemView.findViewById(R.id.textViewTimeSPO2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(new OnClick());
                }
            });
        }
    }
}
